package com.mygdx.catte.Others;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class IClickListener extends ClickListener {
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(pointer >0){
            return  false;
        }
        return super.touchDown(event, x, y, pointer, button);
    }
}
