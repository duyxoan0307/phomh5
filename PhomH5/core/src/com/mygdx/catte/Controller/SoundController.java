package com.mygdx.catte.Controller;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.GameSetting.GameSetting;

public class SoundController {
    public static SoundController Instance;
    public SoundController(){
        Instance = this;
        if(Gdx.app.getType() == Application.ApplicationType.WebGL) {
            initSoundJS();
        }
    }
    public void PlayClipButton(){
        PlaySoundJS("Sounds/buttonClick.mp3");
    }
    public void PlayClipDownCards(){
        PlaySoundJS("Sounds/downcards.mp3");
    }
    public void PlayClipEatCard(){
        PlaySoundJS("Sounds/eatcard.wav");
    }
    public void PlayClipLose(){
        PlaySoundJS("Sounds/lose.wav");
    }
    public void PlayClipPlayCard(){
        PlaySoundJS("Sounds/playcard.wav");
    }
    public void PlayClipWin(){
        PlaySoundJS("Sounds/win.mp3");
    }
    private void initSoundJS(){
        //sound
        loadSound("Sounds/buttonClick.mp3");
        loadSound("Sounds/downcards.mp3");
        loadSound("Sounds/eatcard.wav");
        loadSound("Sounds/lose.wav");
        loadSound("Sounds/playcard.wav");
        loadSound("Sounds/win.mp3");
    }
    void PlaySoundJS(String name){
        if (!GameSetting.Instance.isSound)
            return;
        if(Gdx.app.getType() == Application.ApplicationType.WebGL) {
            playSound(name);
        }
    }
    public static native void loadSound(String name) /*-{
            $wnd.loadSound(name);
        }-*/;

    public static native void playSound(String name) /*-{
            $wnd.playSound(name);
        }-*/;
}
