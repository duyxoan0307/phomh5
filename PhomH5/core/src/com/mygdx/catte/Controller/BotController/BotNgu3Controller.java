package com.mygdx.catte.Controller.BotController;

import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.PlayerModel;

/**
 * Created by DUY on 7/24/2018.
 */

public class BotNgu3Controller implements BotBasicController {
    public boolean IsShouldEat(CardModel card_to_eat)
{
    GameModel gameModel = GameController.Instance.gameModel;
    PlayerModel player = gameModel.GetCurrentPlayer();
    return gameModel.KiemTraCoNenAn (player,card_to_eat);
}
    Array<CardModel> list_hop_le;
    public CardModel ChooseCardToOut(Array<CardModel> list_hop_le)
    {

        GameModel gameModel = GameController.Instance.gameModel;
        PlayerModel player = gameModel.GetCurrentPlayer();

        this.list_hop_le=list_hop_le;

        CardModel card_to_out;
        Array<CardModel> list_0 = gameModel.ListOut (player, 0,list_hop_le);
        card_to_out = GetCardRac (list_0,player,gameModel);
        if (card_to_out != null)
            return card_to_out;

        list_0.clear ();
        list_0 = gameModel.ListOut (player, 1,list_hop_le);
        card_to_out = GetCardCa (list_0, player, gameModel);
        if (card_to_out != null)
            return card_to_out;
        //list_0.clear ();

        list_0.clear ();
        list_0 = gameModel.ListOut (player, 3,list_hop_le);
        return GetRandomCard (list_0,1);
    }

    public CardModel GetRandomCard(Array<CardModel> list,int min)
    {
        return GameController.Instance.gameModel.UuTienDanhQuanLonNhat (list,min);
    }

    public CardModel GetCardRac(Array<CardModel> list, PlayerModel player,GameModel gameModel)
    {
        Array<CardModel> list_danh_cho_doi_phuong_an = gameModel.DanhChoDoiPhuongAn (list,player); //-(7) danh cho doi phuong an
        if (list_danh_cho_doi_phuong_an.size > 0)
            return GetRandomCard (list_danh_cho_doi_phuong_an,5);

        return null;
    }

    public CardModel GetCardCa(Array<CardModel> list, PlayerModel player, GameModel gameModel)
    {
        Array<CardModel> list_danh_cho_doi_phuong_an = gameModel.DanhChoDoiPhuongAn (list,player); //-(7) danh cho doi phuong an
        if (list_danh_cho_doi_phuong_an.size > 0)
            return GetRandomCard (list_danh_cho_doi_phuong_an,5);
        CardModel uu_tien_quan_lon_nhat = gameModel.UuTienDanhQuanLonNhat (list,1); //-(6) danh quan lon nhat
        if (uu_tien_quan_lon_nhat != null)
            return uu_tien_quan_lon_nhat;
        return null;
    }
    public void ChooseCardsToGive()
    {

    }
    public void ChooseCardsToDown()
    {

    }
}
