package com.mygdx.catte.Controller.BotController;

import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.PhomModel;
import com.mygdx.catte.Model.PlayerModel;


/**
 * Created by DUY on 6/13/2018.
 */

public interface BotBasicController {
    boolean IsShouldEat(CardModel card);
    CardModel ChooseCardToOut(Array<CardModel> list_hop_le);
    void ChooseCardsToDown();
    void ChooseCardsToGive();
}
