package com.mygdx.catte.Controller.BotController;

import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.PhomModel;
import com.mygdx.catte.Model.PlayerModel;

/**
 * Created by DUY on 7/24/2018.
 */

public class Bot9Controller implements BotBasicController {
    public boolean IsShouldEat(CardModel card_to_eat)
    {
        return true;
    }
    Array<CardModel> list_hop_le;

    public CardModel ChooseCardToOut(Array<CardModel> list_hop_le)
    {

        GameModel gameModel = GameController.Instance.gameModel;
        PlayerModel player = gameModel.GetCurrentPlayer();

        this.list_hop_le=list_hop_le;

        CardModel card_to_out;
        Array<CardModel> list_0 = gameModel.ListOut (player, 0,list_hop_le);
        card_to_out = GetCardRac (list_0,player,gameModel);
        if (card_to_out != null)
            return card_to_out;

        list_0.clear ();
        list_0 = gameModel.ListOut (player, 1,list_hop_le);
        card_to_out = GetCardCa (list_0, player, gameModel);
        if (card_to_out != null)
            return card_to_out;
        //list_0.clear ();

        list_0.clear ();
        list_0 = gameModel.ListOut (player, 3,list_hop_le);
        return GetRandomCard (list_0,1);
    }

    public CardModel GetRandomCard(Array<CardModel> list,int min)
    {
        return GameController.Instance.gameModel.UuTienDanhQuanLonNhat (list,min);
    }
    public CardModel GetCardRac(Array<CardModel> list, PlayerModel player,GameModel gameModel)
    {
        if (gameModel.CoNenChoDoiPhuongAn (player)) {
            Array<CardModel> list_danh_cho_doi_phuong_an = gameModel.DanhChoDoiPhuongAn (list,player); //-(7) danh cho doi phuong an
            if (list_danh_cho_doi_phuong_an.size > 0)
                return GetRandomCard (list_danh_cho_doi_phuong_an,5);
        }
        Array<CardModel> list_tao_ca_voi_bai_da_danh = gameModel.DanhQuanTaoCaVoiBaiDaDanh (list); //-(8) danh quan tao ca voi bai da danh
        if (list_tao_ca_voi_bai_da_danh.size > 0)
            return GetRandomCard (list_tao_ca_voi_bai_da_danh,4);
        Array<CardModel> list_tao_phom_voi_bai_da_danh = gameModel.DanhQuanTaoPhomVoiBaiDaDanh (list); //-(2) danh quan tao phom voi bai da danh
        if (list_tao_phom_voi_bai_da_danh.size > 0)
            return GetRandomCard (list_tao_phom_voi_bai_da_danh,3);
        Array<CardModel> list_cau_bai = gameModel.CauBai (list); //-(5) cau bai
        if (list_cau_bai.size > 0)
            return GetRandomCard (list_cau_bai,5);
        Array<CardModel> list_quan_ko_an_duoc = gameModel.DanhQuanKhongAnDuoc (player,list,gameModel.NextPlayer()); //-(1) danh quan ko an duoc
        if (list_quan_ko_an_duoc.size > 0)
            return GetRandomCard (list_quan_ko_an_duoc,5);

        return null;
    }

    public CardModel GetCardCa(Array<CardModel> list, PlayerModel player,GameModel gameModel)
    {
        if (gameModel.CoNenChoDoiPhuongAn (player)) {
            Array<CardModel> list_danh_cho_doi_phuong_an = gameModel.DanhChoDoiPhuongAn (list,player); //-(7) danh cho doi phuong an
            if (list_danh_cho_doi_phuong_an.size > 0)
                return GetRandomCard (list_danh_cho_doi_phuong_an,5);
        }
        Array<CardModel> list_tao_ca_voi_bai_da_danh = gameModel.DanhQuanTaoCaVoiBaiDaDanh (list); //-(8) danh quan tao ca voi bai da danh
        if (list_tao_ca_voi_bai_da_danh.size > 0)
            return GetRandomCard (list_tao_ca_voi_bai_da_danh,4);
        Array<CardModel> list_tao_phom_voi_bai_da_danh = gameModel.DanhQuanTaoPhomVoiBaiDaDanh (list); //-(2) danh quan tao phom voi bai da danh
        if (list_tao_phom_voi_bai_da_danh.size > 0)
            return GetRandomCard (list_tao_phom_voi_bai_da_danh,4);
        Array<CardModel> list_cau_bai = gameModel.CauBai (list); //-(5) cau bai
        if (list_cau_bai.size > 0)
            return GetRandomCard (list_cau_bai,4);
        Array<CardModel> list_quan_ko_an_duoc = gameModel.DanhQuanKhongAnDuoc (player,list,gameModel.NextPlayer()); //-(1) danh quan ko an duoc
        if (list_quan_ko_an_duoc.size > 0)
            return GetRandomCard (list_quan_ko_an_duoc,5);
        CardModel uu_tien_quan_lon_nhat = gameModel.UuTienDanhQuanLonNhat (list,1); //-(6) danh quan lon nhat
        if (uu_tien_quan_lon_nhat != null)
            return uu_tien_quan_lon_nhat;
        return null;
    }

    public void ChooseCardsToGive()
    {

    }
    public void ChooseCardsToDown()
    {

    }
}
