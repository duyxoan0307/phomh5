package com.mygdx.catte.Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Model.DataModel;
import com.mygdx.catte.Model.PhomModel;
import com.mygdx.catte.Phom;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.InfoModel;
import com.mygdx.catte.Model.PlayerModel;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.UI.OverlayUI;
import com.mygdx.catte.UI.PlayingUI;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;
import com.mygdx.catte.View.CardView;
import com.mygdx.catte.View.ListCardView;
import com.mygdx.catte.View.PlayerView;

import java.util.Random;


/**
 * Created by DUY on 6/12/2018.
 */

public class GameController extends Group {
    public static GameController Instance;

    public PlayerView[] playerViews;
    Image background;
    Image logo;
    Group deck;
    CardView[] arrCardViews;
    public Image imgArrowGuide;
    public GameModel gameModel;
    public Array<Integer> listPlayerInfo;
    Array<InfoModel> listInfoModel;

    Array<Actor> listQuanLyScore;
    Array<Actor> listQuanLyRank;
    Array<Actor> listQuanLyArrow;

    public static CardModel currentChooseCard;
    public static Array<CardModel> listChooseCard;
    int gameCount =0;

    public GameController() {
        Instance = this;
        new PreferenceController();
        //init setting
        new GameSetting();
        //addBackground
        AddBackGround();

        gameModel=new GameModel(2,4,false);
        playerViews = new PlayerView[4];
        //create group deck
        CreateDecks();
        //init list player views
        SetListPlayerViews();
        //create imgArrow
        imgArrowGuide=new Image(Assets.GetTexture(Assets.imgArrow));
        //listChooseCard
        listChooseCard=new Array<CardModel>();
        //init list quan ly
        listQuanLyScore=new Array<Actor>();
        listQuanLyRank=new Array<Actor>();
        listQuanLyArrow=new Array<Actor>();
        new DataModel();
        is_Sorting=false;
        CardView.canDownCards=false;
        //Test
        Test();

        locateActors();
    }


    void CreateDecks() {
        deck = new Group();
        this.addActor(deck);
        arrCardViews = new CardView[52];
        CreateArrayCardView();
    }

    void Test() {

    }

    public void NewGame(boolean is_resume) {
        is_show_full_screen=true;
        if (!UIController.Instance.CheckTienCuoc())
        {
            OverlayUI.Show();
            UIController.Instance.optionUI.Show();
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Bạn cần "+Utilities.ShortCutMoney(GameController.GetScoreBet()*10)+" để chơi mức cược này!");
            return;
        }
        //reset
        ResetGame();
        //set score bet
        UIController.Instance.playingUI.SetScoreBet();
        //Set list info
        SetListInfo();
        //reset button
        ResetButton();
        //init Game
        InitGame(is_resume);
        //update info
        UpdateInfo();
    }

    void SetListPlayerViews() {
        //create ListPlayerView
        CreateListPlayerView();

        //SetPositionPlayerView
        SetPositionListPlayerView();
    }

    void SetListInfo() {
        if (listPlayerInfo != null && listPlayerInfo.size == PreferenceController.Instance.preferences.getInteger("NumberPlayer"))
            return;
        //create list player info
        CreateListInfo();
    }

    private void CreateListInfo() {
        listPlayerInfo = GetListPlayerWillPlay();
        listInfoModel = new Array<InfoModel>();
        for (int i = 0; i < listPlayerInfo.size; i++) {
            listInfoModel.add(new InfoModel(listPlayerInfo.get(i)));
        }
    }


    private void AddBackGround() {
        background = new Image(Assets.GetTexture(Assets.backgroundGamePlay));
        background.setSize(Phom.V_WIDTH, Phom.V_HEIGHT);
        background.setPosition(-Phom.V_WIDTH / 2, -Phom.V_HEIGHT / 2);

        logo = new Image(Assets.GetTexture(Assets.textPhom));
        logo.setPosition(0,0,Align.center);

        this.addActor(background);
        this.addActor(logo);
    }


    void CreateListPlayerView() {
        for (int i = 0; i < 4; i++) {
            playerViews[i] = new PlayerView(i);
           // this.addActor(playerViews[i]);
            playerViews[i].toBack();
        }
        background.toBack();
    }

    void SetPositionListPlayerView() {
        for (int i = 0; i < 4; i++) {
            SetPositionPlayerView(playerViews[i]);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].infoView);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listOut);
        }

        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listEat);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listPhom[0]);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listPhom[1]);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listPhom[2]);
        }
        for (int i=0;i<4;i++)
        {
            addActor(playerViews[i].listHold);
        }
    }

    void SetPositionPlayerView(PlayerView playerView) {
        Vector2 phom_0_position;
        switch (playerView.index) {
            case 0:
                playerView.listHold.setPosition(0, -Phom.V_HEIGHT/2+80);
                playerView.listOut.setPosition(440, -Phom.V_HEIGHT/2+240);
                playerView.listEat.setPosition(0, -Phom.V_HEIGHT/2+130);

                phom_0_position = new Vector2(0,-Phom.V_HEIGHT/2+260);
                playerView.listPhom[0].setPosition(phom_0_position.x,phom_0_position.y);
                playerView.listPhom[1].setPosition(phom_0_position.x,phom_0_position.y-35);
                playerView.listPhom[2].setPosition(phom_0_position.x,phom_0_position.y-70);
                break;
            case 1:
                playerView.listHold.setPosition(Phom.V_WIDTH/2-180, 0);
                playerView.listOut.setPosition(playerViews[0].listOut.getX(Align.center),-playerViews[0].listOut.getY(Align.center),Align.center);
                playerView.listEat.setPosition(Phom.V_WIDTH/2-240, 0);

                phom_0_position = new Vector2(playerView.listOut.getX()-10,playerView.listHold.getY());
                playerView.listPhom[0].setPosition(phom_0_position.x,phom_0_position.y+35);
                playerView.listPhom[1].setPosition(phom_0_position.x,phom_0_position.y);
                playerView.listPhom[2].setPosition(phom_0_position.x,phom_0_position.y-35);
                break;
            case 2:
                playerView.listHold.setPosition(0, Phom.V_HEIGHT/2-90);
                playerView.listOut.setPosition(-playerViews[1].listOut.getX(Align.center),playerViews[1].listOut.getY(Align.center),Align.center);
                playerView.listEat.setPosition(60, Phom.V_HEIGHT/2-90);

                phom_0_position = new Vector2(0,Phom.V_HEIGHT/2-200);
                playerView.listPhom[0].setPosition(phom_0_position.x,phom_0_position.y+35);
                playerView.listPhom[1].setPosition(phom_0_position.x,phom_0_position.y);
                playerView.listPhom[2].setPosition(phom_0_position.x,phom_0_position.y-35);
                break;
            case 3:
                playerView.listHold.setPosition(-Phom.V_WIDTH/2+180, 0);
                playerView.listOut.setPosition(playerViews[2].listOut.getX(Align.center),-playerViews[2].listOut.getY(Align.center),Align.center);
                playerView.listEat.setPosition(-Phom.V_WIDTH/2+240, 0);

                phom_0_position = new Vector2(playerView.listOut.getX()+10,playerView.listHold.getY());
                playerView.listPhom[0].setPosition(phom_0_position.x,phom_0_position.y+35);
                playerView.listPhom[1].setPosition(phom_0_position.x,phom_0_position.y);
                playerView.listPhom[2].setPosition(phom_0_position.x,phom_0_position.y-35);
                break;
        }
        playerView.UpdateGroupListCardView();
        playerView.SetPositionInfoView();
    }

    void InitGame(boolean is_resume) {
        gameModel.Clear();
        gameModel.gameState= GameModel.GameState.START;
        if (!is_resume)
        {
            DataModel.Instance.Reset();
            DataModel.Instance.SaveListCardOnTable();
        }
        if (is_resume)
        {
            gameModel.listCardOnTable.clear();
            Utilities.ImportDataCards(PreferenceController.Instance.preferences.getString("ListCardOnTable"),gameModel.listCardOnTable);
        }
        gameModel.RegisterEndDistributeCards(new GameModel.OnEndDistributeCards() {
            @Override
            public void OnEnvent() {
                OnEndDistributeCards();
            }
        });

        for (int i = 0; i < gameModel.listPlayer.size; i++) {
            final PlayerView playerView = playerViews[i];
            final PlayerModel playerModel = gameModel.listPlayer.get(i);
            playerModel.infoModel = listInfoModel.get(i);
            playerModel.RegisterSetUnChildToSort(new PlayerModel.OnSetUnChildToSort() {
                @Override
                public void OnEvent(int index_list) {
                    playerView.SetUnChildToSort(index_list);
                }
            });
            playerModel.RegisterSortList(new PlayerModel.OnSortList() {
                @Override
                public void OnEvent(int index_list) {
                    if (index_list<=2)
                        SortList(playerView, index_list);
                    else
                        playerView.SortPhom(playerModel.phom);
                }
            });
            playerModel.RegisterSetInfo(new PlayerModel.OnSetInfo() {
                @Override
                public void OnEvent() {
                    {
                        if (playerModel.index==0)
                        {
                            UIController.Instance.startUI.SetCoin();
                        }
                        playerView.infoView.UpdateInfo(playerModel.infoModel);
                    }
                }
            });
            playerModel.RegisterEndBocHoacAn(new PlayerModel.OnEndBocHoacAn() {
                @Override
                public void OnEvent() {
                    OnEndBocHoacAn();
                }
            });
            playerModel.RegisterEndDanh(new PlayerModel.OnEndDanh() {
                @Override
                public void OnEvent() {
                    OnEndDanh();
                }
            });
            playerModel.RegisterEndHaBai(new PlayerModel.OnEndHaBai() {
                @Override
                public void OnEvent() {
                    OnEndHaBai();
                }
            });
            playerModel.RegisterEndGuiBai(new PlayerModel.OnEndGuiBai() {
                @Override
                public void OnEvent() {
                    OnEndGuiBai();
                }
            });
            playerModel.RegisterChangeScore(new PlayerModel.OnChangeScore() {
                @Override
                public void OnEvent(final int score) {
                    OnChangeScore(playerView,score);
                }
            });
        }

        for (int i = 0; i < gameModel.listCardOnTable.size; i++) {
            final CardView cardView = arrCardViews[i];
            deck.addActor(cardView);
            cardView.image.setPosition(0.7f * i, 1.2f * i, Align.center);

            final CardModel card_model = gameModel.listCardOnTable.get(i);
            card_model.RegisterOnSetParent(new CardModel.OnSetParent() {
                @Override
                public void OnEvent(int index_player, int index_list) {
                    if (index_list<=2)
                       playerViews[index_player].GetListByIndex(index_list).AddCardView(cardView);
                    else
                        playerViews[index_player].listPhom[index_list-3].AddCardView(cardView);
                }
            });
            card_model.RegisterSetVisible(new CardModel.OnSetVisible() {
                @Override
                public void OnEvent(boolean is_visible) {
                    cardView.SetVisible(is_visible, card_model);
                }
            });
            card_model.RegisterSetFlip(new CardModel.OnSetFlip() {
                @Override
                public void OnEvent(int index) {
                    cardView.SetFlip(card_model, index);
                }
            });
            card_model.RegisterSetFirstDistribute(new CardModel.OnSetFirstDistribute() {
                @Override
                public void OnEvent() {
                    cardView.SetFirstDistribute();
                }
            });
            card_model.RegisterSetShadow(new CardModel.OnSetShadow() {
                @Override
                public void OnEvent(int level) {
                    cardView.SetShadow(level);
                }
            });
            card_model.RegisterChooseCard(new CardModel.OnChooseCard() {
                @Override
                public void OnEvent(boolean is_choose) {
                    cardView.MoveCardUpOnly();
                }
            });
            card_model.RegisterSetCanClick(new CardModel.OnSetCanClick() {
                @Override
                public void OnEnvent(final boolean can_click) {
                    cardView.SetCanClick(can_click);
                }
            });
            card_model.RegisterMoveDown(new CardModel.OnMoveDown() {
                @Override
                public void OnEvent() {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            cardView.SetMoveDown();
                        }
                    },0.5f);
                }
            });
            card_model.RegisterCanBocHoacAn(new CardModel.OnCanBocHoacAn() {
                @Override
                public void OnEvent(int type) {
                    cardView.SetCanBocHoacAn(type);
                    if (type>-1)
                    {
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                if (!gameModel.isPlaying)
                                    return;
                                CreateArrowAt(cardView);
                            }
                        },0.6f);
                    }
                }
            });
            //cardView
            cardView.RegisterChooseCard(new CardView.OnChooseCard() {
                @Override
                public void OnEvent(boolean is_choose,boolean is_down_cards) {
                    if (!is_down_cards)
                    {
                        if (is_choose) {
                            currentChooseCard = card_model;
                            CreateArrowGuide(cardView);
                        } else {
                            currentChooseCard = null;
                            imgArrowGuide.setVisible(false);
                        }
                    }
                    else
                    {
                        if (is_choose) {
                            if (!listChooseCard.contains(card_model,false))
                                listChooseCard.add(card_model);
                        } else {
                            listChooseCard.removeValue(card_model,false);
                        }
                    }
                }
            });
        }

        if (!is_resume)
        {
            //distribute cards
            gameModel.DistributeCardsTest();
        }
        else
        {
            DataModel.Instance.LoadData();
            UIController.Instance.playingUI.btnXepBai.setVisible(true);
            UIController.Instance.playingUI.SetTextSoBai();
            if (gameModel.gameState== GameModel.GameState.START)
                BatDauLuotDauTien();
            else
            {
                switch(gameModel.turnState)
                {
                    case BOCHOACAN:
                        BatDauLuot();
                        break;
                    case DANH:
                        OnEndBocHoacAn();
                        break;
                    case HA:
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                if (gameModel.isPlaying)
                                    OnEndDanh();
                            }
                        },1f);
                        break;
                    case GUI:
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                if (gameModel.isPlaying)
                                    OnEndHaBai();
                            }
                        },1f);
                        break;
                }
            }
        }
    }


    void SortList(PlayerView playerView,int index_list){
        ListCardView listCardView=playerView.GetListByIndex(index_list);
        if (index_list==0)
        {
            if (playerView.index==0)
            {
                listCardView.SortCenter(0.6f);
            }
            else
            {
                listCardView.MoveToParent();
            }
            return;
        }
        if (index_list==1)
        {
            if (playerView.index<=1)
            {
                listCardView.SortRight(0.5f);
            }
            else
            {
                listCardView.SortLeft(0.5f);
            }
            return;
        }
        if (index_list==2)
        {
            if (playerView.index==0)
                return;
            if (playerView.index>=2)
                listCardView.SortLeft(0.5f);
            else
                listCardView.SortRight(0.5f);
            return;
        }
    }
    void OnEndDistributeCards(){
        UIController.Instance.playingUI.SetTextSoBai();
        for (PlayerModel player:gameModel.listPlayer)
        {
            if (player.index==0)
                continue;
            player.SortList(0);
        }
        playerViews[0].listHold.SortCenterDelay(0.6f);

        for (CardView cardView:arrCardViews)
        {
            cardView.image.setOrigin(Align.center);
            cardView.image.addAction(Actions.rotateTo(0,0.2f));
        }

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                gameModel.listPlayer.get(0).SetFlipListHold();
            }
        },1.2f);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                UIController.Instance.playingUI.btnXepBai.setVisible(true);
                BatDauLuotDauTien();
            }
        },3f);

        //save data
        gameModel.gameState= GameModel.GameState.START;
        DataModel.Instance.SaveGameModel();
    }


    void UpdateInfo(){
        for (PlayerModel playerModel:gameModel.listPlayer)
            playerModel.SetInfo();
    }

    Array<Integer> GetListPlayerWillPlay() {
        Array<Integer> list = new Array<Integer>();
        int number_player = PreferenceController.Instance.preferences.getInteger("NumberPlayer");
        int ran;
        Random random = new Random();
        list.add(0);
        while (list.size < number_player) {
            do {
                ran = 1 + random.nextInt(13 - 1);
            } while (list.contains(ran, false));
            list.add(ran);
        }
        return list;
    }
    public void ResetGame(){
        is_end_game = false;
        currentChooseCard=null;
        listChooseCard.clear();
        loaiU=null;
        imgArrowGuide.setVisible(false);
        CardView.canDownCards=false;
        UIController.Instance.playingUI.SetGuideContent("");
        for (CardView cardView:arrCardViews)
        {
            cardView.Reset();
        }
        for (PlayerView playerView:playerViews)
            playerView.Clear();
        ResetButton();
        ResetListQuanLy(listQuanLyScore);
        ResetListQuanLy(listQuanLyRank);
        ResetListQuanLy(listQuanLyArrow);
        UIController.Instance.fireWorkUI.HideFireWork();
        UIController.Instance.playingUI.HideEffect();
    }

    void ResetButton(){
        PlayingUI playingUI = UIController.Instance.playingUI;
        playingUI.btnDanh.setVisible(false);
        playingUI.btnAn.setVisible(false);
        playingUI.btnBoc.setVisible(false);
        playingUI.btnGuiBai.setVisible(false);
        playingUI.btnHaBai.setVisible(false);
        playingUI.btnXepBai.setVisible(false);
        playingUI.btnVanMoi.setVisible(false);
    }

    void CreateArrayCardView(){
        for (int i=0;i<arrCardViews.length;i++)
        {
            final CardView cardView = new CardView();
            deck.addActor(cardView);
            cardView.image.setPosition(deck.getX()-cardView.image.getWidth()/2,deck.getY()-cardView.image.getHeight()/2);
            arrCardViews[i]=cardView;
        }
    }
    void AddListQuanLy(Array<Actor> list, Actor actor){
        list.add(actor);
        this.addActor(actor);
    }
    void ResetListQuanLy(Array<Actor> list){
        for (Actor actor:list
                ) {
            removeActor(actor);
        }
        list.clear();
    }

    void CreateArrowGuide(CardView cardView){
        imgArrowGuide.setVisible(true);
        if (imgArrowGuide.hasParent())
            imgArrowGuide.getParent().removeActor(imgArrowGuide);
        cardView.addActor(imgArrowGuide);
        imgArrowGuide.setPosition(cardView.image.getX(Align.center),cardView.image.getY(Align.center)+200,Align.center);
    }

    enum LoaiU{UTRON,UTHUONG,UKHAN};
    LoaiU loaiU;
    boolean KiemTraUKhan(PlayerModel player){
        Array<CardModel> list_khong_co_ca = ListCardKhongCoCa(player.list_hold);
        if (list_khong_co_ca.size>=9)
        {
            for(CardModel card:player.list_hold)
                card.SetVisible(true);
            playerViews[gameModel.listPlayer.indexOf(player,false)].XoeBai();
            loaiU=LoaiU.UKHAN;
            return true;
        }
        return false;
    }

    boolean KiemTraU(final PlayerModel player){
        if (gameModel.turnState== GameModel.TurnState.BOCHOACAN && gameModel.gameState!= GameModel.GameState.START)
        {
            Array<CardModel> list_hold=new Array<CardModel>(player.list_hold);
            if (!gameModel.KiemTraCoPhom(list_hold,player.cardVuaLay))
                return false;
        }
        if (player.list_hold.size==0)
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (!gameModel.isPlaying)
                        return;
                }
            },1f);
            loaiU=LoaiU.UTHUONG;
            return true;
        }
        if (gameModel.turnState!= GameModel.TurnState.BOCHOACAN && gameModel.gameState!= GameModel.GameState.START)
            return false;
        final PhomModel phom_toi_uu=gameModel.TimPhomNhieuLa(player.list_hold,player.list_eat);
        if (phom_toi_uu==null)
            return false;
        //u tron
        if ((player.list_hold.size - phom_toi_uu.GetAllCards().size) ==0) {
            if (player.phom.GetAllCards().size + phom_toi_uu.GetAllCards().size >= 10) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (!gameModel.isPlaying)
                            return;
                        player.HaBai(phom_toi_uu);
                        for (CardModel card : player.list_hold)
                            card.SetVisible(true);
                    }
                }, 1f);
                loaiU = LoaiU.UTRON;
                return true;
            }
        }

        //u thuong
        if ((player.list_hold.size - phom_toi_uu.GetAllCards().size) <=1) {
            if (player.phom.GetAllCards().size + phom_toi_uu.GetAllCards().size >= 9) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (!gameModel.isPlaying)
                            return;
                        player.HaBai(phom_toi_uu);
                        for (CardModel card : player.list_hold)
                            card.SetVisible(true);
                    }
                }, 1f);
                loaiU = LoaiU.UTHUONG;
                return true;
            }
        }
        return false;
    }
    void BatDauLuotDauTien(){
        for (int i=0;i<4;i++)
        {
            PlayerModel player=gameModel.listPlayer.get((gameModel.host+ i)%(4));
            if (KiemTraUKhan(player) || KiemTraU(player))
            {
                SetEndGame(player);
                return;
            }
        }
        for (CardModel card:gameModel.listPlayer.get(0).list_hold)
            card.SetCanClick(true);
        gameModel.gameState= GameModel.GameState.PLAYING;
        DataModel.Instance.SaveGameState();
        OnEndBocHoacAn();
    }
    void BatDauLuot(){
        gameModel.turnState= GameModel.TurnState.BOCHOACAN;
        DataModel.Instance.SaveTurnState();
        final PlayerModel player=gameModel.GetCurrentPlayer();
        if (IsMainPlayerTurn())
        {
            UIController.Instance.playingUI.btnBoc.setVisible(true);
            UIController.Instance.playingUI.SetGuideContent("Bốc 1 lá bài");
            gameModel.listCardOnTable.peek().SetCanBocHoacAn(0);
            if (CheckCanEat(player))
            {
                UIController.Instance.playingUI.btnAn.setVisible(true);
                UIController.Instance.playingUI.SetGuideContent("Bốc 1 lá bài hoặc ăn");
                gameModel.GetPreviousPlayer().list_out.peek().SetCanBocHoacAn(1);
            }
        }
        else
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (!gameModel.isPlaying)
                        return;
                    if (CheckCanEat(player)) {
                        player.An();
                    }
                    else
                    {
                        player.Boc();
                    }
                }
            },1f);
        }
    }
    void OnEndBocHoacAn(){
        if (KiemTraU(gameModel.GetCurrentPlayer()))
        {
            SetEndGame(gameModel.GetCurrentPlayer());
            return;
        }
        gameModel.turnState= GameModel.TurnState.DANH;
        DataModel.Instance.SaveTurnState();
        final PlayerModel player=gameModel.GetCurrentPlayer();
        if (IsMainPlayerTurn())
        {
            UIController.Instance.playingUI.btnBoc.setVisible(false);
            UIController.Instance.playingUI.btnAn.setVisible(false);
            UIController.Instance.playingUI.btnDanh.setVisible(true);
            UIController.Instance.playingUI.SetGuideContent("Đánh 1 lá bài");
        }
        else
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (gameModel.isPlaying)
                    {
                        player.AIDanh();
                    }
                }
            },1f);
        }
    }
    void OnEndDanh(){
        if (IsMainPlayerTurn())
        {
            currentChooseCard=null;
            imgArrowGuide.setVisible(false);
            UIController.Instance.playingUI.btnDanh.setVisible(false);
        }
        if (IsHaBai())
        {
            SetHaBai();
        }
        else
            KetThucLuot();
    }

    void SetHaBai(){
        gameModel.turnState= GameModel.TurnState.HA;
        DataModel.Instance.SaveTurnState();
        final PlayerModel player=gameModel.GetCurrentPlayer();
        if (IsMainPlayerTurn())
        {
            CardView.canDownCards=true;
            DataModel.Instance.SaveCanDownCards();
            UIController.Instance.playingUI.btnHaBai.setVisible(true);
            UIController.Instance.playingUI.SetGuideContent("Chọn những phỏm muốn hạ");
            PhomModel phom_toi_uu=gameModel.TimPhomToiUu(player.list_hold,player.list_eat);
            if (phom_toi_uu!=null)
            {
                listChooseCard=phom_toi_uu.GetAllCards();
                for(CardModel card:listChooseCard)
                    card.SetChooseCard(true);
            }
        }
        else
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (!gameModel.isPlaying)
                        return;
                    player.AIHaBai();
                }
            },1f);
        }
    }
    boolean IsHaBai(){
        return gameModel.GetCurrentPlayer().list_out.size==4;
    }
    void OnEndHaBai(){
        if(is_end_game){
            return;
        }
        if (gameModel.gameState== GameModel.GameState.OVER)
            return;
        if (IsMainPlayerTurn())
        {
            listChooseCard.clear();
            for (CardModel card : gameModel.GetCurrentPlayer().list_hold)
                card.SetMoveDown();
            CardView.canDownCards=false;
            DataModel.Instance.SaveCanDownCards();
            UIController.Instance.playingUI.btnHaBai.setVisible(false);
        }
        if (GameSetting.Instance.isGuiBai && gameModel.GetCurrentPlayer().phom.list_phom.size>0 &&gameModel.GetCurrentPlayer().list_hold.size>0)
        {
            SetGuiBai();
        }
        else
        {
            KiemTraUSauKhiHaHoacGui();
        }
    }
    void SetGuiBai(){
        gameModel.turnState= GameModel.TurnState.GUI;
        DataModel.Instance.SaveTurnState();
        final PlayerModel player=gameModel.GetCurrentPlayer();
        if (IsMainPlayerTurn())
        {
            CardView.canDownCards=true;
            DataModel.Instance.SaveCanDownCards();
            UIController.Instance.playingUI.btnGuiBai.setVisible(true);
            UIController.Instance.playingUI.SetGuideContent("Chọn những lá muốn gửi");
        }
        else
        {
           Timer.Task task = Timer.schedule(new Timer.Task() {
               @Override
               public void run() {
                   if (!gameModel.isPlaying)
                       return;
                   player.AIGuiBai(player.list_hold);
               }
           },1f);
        }
    }
    void OnEndGuiBai(){
        if (IsMainPlayerTurn()){
            listChooseCard.clear();
            for (CardModel card : gameModel.GetCurrentPlayer().list_hold)
                card.SetMoveDown();
            CardView.canDownCards=false;
            DataModel.Instance.SaveCanDownCards();
            UIController.Instance.playingUI.btnGuiBai.setVisible(false);
        }
        KiemTraUSauKhiHaHoacGui();
    }
    void KiemTraUSauKhiHaHoacGui(){
        if (KiemTraU(gameModel.GetCurrentPlayer()))
        {
            SetEndGame(gameModel.GetCurrentPlayer());
            return;
        }
        KetThucLuot();
    }
    void KetThucLuot(){
        if (CheckEndGame())
        {
            DownAllCards();
            SetEndGame(null);
        }
        else
        {
            if (IsMainPlayerTurn())
                UIController.Instance.playingUI.SetGuideContent("");
            NextTurn();
        }
    }
    void NextTurn(){
        gameModel.currentTurn++;
        gameModel.currentTurn=ConvertTurn(gameModel.currentTurn);
        DataModel.Instance.SaveCurrentTurn();
        BatDauLuot();
    }
    int ConvertTurn(int turn)
    {
        if (turn >= 4)
            return turn % 4;
        if (turn < 0)
            return turn + 4;
        return turn;
    }
    boolean CheckEndGame(){
        return gameModel.listCardOnTable.size==0;
    }

    boolean IsMainPlayerTurn(){
        return gameModel.currentTurn==0;
    }
    boolean CheckCanEat(PlayerModel playerModel)
    {
        CardModel card_to_eat = gameModel.GetPreviousPlayer().list_out.peek();
        if (card_to_eat==null)
            return false;
        Array<CardModel> list_hold = new Array<CardModel>(playerModel.list_hold);
        list_hold.add(card_to_eat);
        Array<CardModel> list_eat = new Array<CardModel>(playerModel.list_eat);
        list_eat.add(card_to_eat);
        if (!gameModel.KiemTraCoPhom(list_hold,card_to_eat))
            return false;
        return gameModel.CheckCanEat(list_hold,list_eat);
    }
    PlayerModel player_win;
    boolean is_end_game = false;
    void SetEndGame(final PlayerModel player){
        is_end_game = true;
        if (player!=null)
        {
            player_win=player;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (!gameModel.isPlaying)
                        return;
                    SetAnTrang(player);
                    gameModel.gameState= GameModel.GameState.OVER;
                    DataModel.Instance.SaveGameState();
                }
            },2f);
        }
        else {
            SetRank();
        }

        SetSoundAndFirework(player);
        UIController.Instance.playingUI.SetGuideContent("");
        ResetButton();
        SetEnableButtonReplay();
        Gdx.app.log("Over","GAME OVER");
        gameCount++;
        Phom.zenSDK.TrackLevelCompleted(gameCount);

    }
    void SetSoundAndFirework(PlayerModel player){
        //fire work
        if (player_win.index==0 || player!=null)
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if (!gameModel.isPlaying)
                        return;
                    UIController.Instance.fireWorkUI.ShowFireWork();
                    SoundController.Instance.PlayClipWin();
                }
            },2f);
        }
        else
        {
            Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                SoundController.Instance.PlayClipLose();
                 }
            },2f);
        }
    }


    boolean is_Sorting;
    public void SortListForMainPlayer(){
        PlayerModel player=gameModel.GetMainPlayer();
        if (gameModel.gameState!= GameModel.GameState.PLAYING || is_Sorting || player.is_sorted_list_hold)
        {
            player.SetViewListHold();
            return;
        }
//        SortListTest();
        is_Sorting=true;
        gameModel.SortPhomOfPlayer(player);
        is_Sorting=false;
        player.SetViewListHold();
       // imgArrowGuide.setVisible(false);
        player.is_sorted_list_hold=true;
    }

//utilities
    Array<CardModel> ListCardKhongCoCa(Array<CardModel> list){
        Array<CardModel> list_result = new Array<CardModel>();
        for (int i=0;i<list.size;i++)
        {
            CardModel card=list.get(i);
            if (!CheckCardCoCa(card,list))
                list_result.add(card);
        }
        return list_result;
    }
    boolean CheckCardCoCa(CardModel card_check,Array<CardModel> list){
        Array<CardModel> list_test = new Array<CardModel>();
        list_test.addAll(list);
        list_test.removeValue(card_check,false);
        for (int i=0;i<list_test.size;i++)
        {
            CardModel card=list_test.get(i);
            if (card.number==card_check.number || (Math.abs(card.number-card_check.number)<=2 && card.type==card_check.type))
                return true;
        }
        return false;
    }
    public boolean CheckCurrentCard(){
        if (currentChooseCard==null)
            return false;
        return gameModel.CheckDanhQuanHopLe(gameModel.GetMainPlayer(),currentChooseCard);
    }
    public void HaBaiForMainPlayer(){
        PlayerModel player = gameModel.GetMainPlayer();
        PhomModel phom_hop_le = gameModel.TimPhomToiUu(listChooseCard,player.list_eat);
        if (phom_hop_le==null && player.list_eat.size>0)
        {
            Gdx.app.log("Thong bao","Phom ko hop le!");
            return;
        }
        gameModel.GetMainPlayer().HaBai(phom_hop_le);
        listChooseCard.clear();
    }
    public void GuiBaiForMainPlayer(){
        gameModel.GetCurrentPlayer().AIGuiBai(listChooseCard);
    }
    void DownAllCards(){
        SoundController.Instance.PlayClipDownCards();
        for (PlayerModel playerModel:gameModel.listPlayer)
        {
            for (CardModel card:playerModel.list_hold)
                card.SetVisible(true);
            for (CardModel card:playerModel.list_out)
                card.SetShadow(1);
            for (CardModel card:playerModel.list_hold)
                card.SetShadow(1);
        }
        for (PlayerView playerView:playerViews)
            playerView.XoeBai();
    }

    void SetEnableButtonReplay(){
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                UIController.Instance.playingUI.btnVanMoi.setVisible(true);
                UIController.Instance.playingUI.btnVanMoi.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1)));
            }
        },4);
    }

    void SetRank()
    {
        // yield return new WaitForSeconds (1f);
        final Array<PlayerModel> list_score = gameModel.ListScore ();
        for (int i = 0; i < 4; i++) {
            if (list_score.get(i).list_hold.size >= 9)
                SetRank(list_score.get(i),5);
            else
                SetRank(list_score.get(i),i);
            final int finalI = i;
            SetScore (list_score.get(finalI), finalI);
        }
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                list_score.get(0).ChangeScore(gameModel.sumScoreWin);
                gameModel.gameState= GameModel.GameState.OVER;
                DataModel.Instance.SaveGameState();
            }
        },1.5f);

        player_win=list_score.get(0);
        if (player_win.index==0)
            SetEffectWin(Assets.GetTexture(Assets.GetHang(0)));
        PreferenceController.Instance.preferences.putInteger("Host",list_score.get(0).index);
        PreferenceController.Instance.preferences.flush();
    }

    public void SetScore(final PlayerModel player, int rank)
    {
        if (rank == 0)
            return;
        final int score;
        if (player.list_hold.size>=9)
            score = 4 * gameModel.scoreBet;
        else
            score=rank * gameModel.scoreBet;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                player.ChangeScore(-score);
            }
        },1.5f);
        gameModel.sumScoreWin += score;
    }
    void SetRank(final PlayerModel player_model, final int rank){
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                SetRankViewForPlayer(player_model,rank,false);
            }
        },1.5f);
    }
    void SetAnTrang(PlayerModel player){
        SetEffectWin(Assets.GetTexture(Assets.GetU(loaiU.ordinal())));
        //tinh tien
        PreferenceController.Instance.preferences.putInteger("Host",player.index);
        PreferenceController.Instance.preferences.flush();
        int tien_cuoc_u =0;
        if (loaiU==LoaiU.UTRON)
            tien_cuoc_u=10;
        else
            tien_cuoc_u=5;
        if (player.so_quan_an_duoc==3) {

            PlayerModel player_den_lang=gameModel.GetPreviousPlayer(player);
            int score_eat;
            if (player_den_lang.list_out.size == 3)
                score_eat = gameModel.scoreBet * 4;
            else
                score_eat = gameModel.scoreBet * 3;
            int score = gameModel.scoreBet * tien_cuoc_u * 3 + score_eat;
            player_den_lang.ChangeScore(-score);
            gameModel.sumScoreWin += score;
            Gdx.app.log("Player "+gameModel.listPlayer.indexOf(player_den_lang,false),"Den lang");
            SetRankView(playerViews[player_den_lang.index], Assets.GetTexture(Assets.imgDenLang));
        }
        else
            for (PlayerModel _player : gameModel.listPlayer) {
            if (_player == player)
                continue;
            _player.ChangeScore(-gameModel.scoreBet*tien_cuoc_u);
            gameModel.sumScoreWin += gameModel.scoreBet * tien_cuoc_u;
        }
        player.ChangeScore(gameModel.sumScoreWin);
        SetRankViewForPlayer(player,0,true);

        for (CardModel card:player.list_out)
            card.SetShadow(1);
        for (CardModel card:player.list_hold)
            card.SetShadow(1);
    }
    void OnChangeScore(PlayerView playerView,int score){
        final Label txtScore;
        if (score>0)
        {
            txtScore = GameUI.NewLabel("",Assets.GetBitmapFont(Assets.goldText));
        }
        else
        {
            txtScore = GameUI.NewLabel("",Assets.GetBitmapFont(Assets.silver_text));
        }
        txtScore.setWidth(0);
        txtScore.setAlignment(Align.center);
        GameUI.ScaleLabel(txtScore,1.5f);
        txtScore.setPosition(playerView.GetListByIndex(0).getX(),playerView.GetListByIndex(0).getY());
        String s="";
        if (score>0)
            s+="+";
        else
            if (score<0)
                s+="-";
        s+=""+ Utilities.ShortCutMoney(Math.abs(score));
        txtScore.setText(s);
        txtScore.addAction(Actions.moveBy(0,40,1f));

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                removeActor(txtScore);
                listQuanLyScore.removeValue(txtScore,false);
            }
        },3f);

        AddListQuanLy(listQuanLyScore,txtScore);
    }
    void SetRankViewForPlayer(PlayerModel player_model,int rank,boolean is_an_trang){
        PlayerView playerView=playerViews[player_model.index];
        if (is_an_trang)
        {
            SetRankView(playerView, Assets.GetTexture(Assets.GetU(loaiU.ordinal())));
            return;
        }
        if (rank==5)
        {
            SetRankView(playerView, Assets.GetTexture(Assets.imgChayBai));
        }
        else
        {
            SetRankView(playerView, Assets.GetTexture(Assets.GetHang(rank)));
        }
    }
    void SetEffectWin(Texture texture){
        UIController.Instance.playingUI.effectActor.UpdateTexture(texture,2);
        UIController.Instance.playingUI.effectActor.UpdateTexture(texture,3);
        UIController.Instance.playingUI.ShowEffect();
    }
    boolean is_show_full_screen;
    void SetRankView(PlayerView playerView,Texture textureKetQua)
    {
        Image imgKetQua = new Image(textureKetQua);
        imgKetQua.setPosition(playerView.infoView.getX(),playerView.infoView.getY()-60,Align.center);
        AddListQuanLy(listQuanLyRank,imgKetQua);
        if (is_show_full_screen)
        {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    Phom.zenSDK.ShowFullscreen();
                }
            },0.5f);
            is_show_full_screen=false;
        }
    }



    void CreateArrowAt(CardView cardView)
    {
        if (!IsMainPlayerTurn())
            return;

        if (gameModel.turnState!= GameModel.TurnState.BOCHOACAN)
            return;
        Image imgArrow = new Image(Assets.GetTexture(Assets.imgArrow));
        imgArrow.setPosition(cardView.image.getX(Align.center),cardView.image.getY(Align.center)+140,Align.center);
        addActor(imgArrow);
        listQuanLyArrow.add(imgArrow);
    }
    public void DisableBocVaAn(){
        ResetListQuanLy(listQuanLyArrow);
        if(gameModel.listCardOnTable.size>0){
            gameModel.listCardOnTable.peek().SetCanBocHoacAn(-1);
        }
        if (gameModel.GetPreviousPlayer().list_out.size>0)
            gameModel.GetPreviousPlayer().list_out.peek().SetCanBocHoacAn(-1);
    }
    public static int GetScoreBet(){
        return UIController.Instance.ListScoreBet().get(PreferenceController.Instance.preferences.getInteger("ScoreBet"));
    }

    public void locateActors(){
        background.setSize(Phom.V_WIDTH,Phom.V_HEIGHT);
        background.setPosition(0,0,Align.center);
        for (int i = 0; i < 4; i++) {
            SetPositionPlayerView(playerViews[i]);
        }
    }
}
