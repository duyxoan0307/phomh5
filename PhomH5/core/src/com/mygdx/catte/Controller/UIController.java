package com.mygdx.catte.Controller;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Model.DataModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.PlayerModel;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.UI.FBLeaderboardUI;
import com.mygdx.catte.UI.FireWorkUI;
import com.mygdx.catte.UI.GuideUI;
import com.mygdx.catte.UI.OptionUI;
import com.mygdx.catte.UI.OverlayUI;
import com.mygdx.catte.UI.PauseUI;
import com.mygdx.catte.UI.PlayingUI;
import com.mygdx.catte.UI.ResumeUI;
import com.mygdx.catte.UI.SettingUI;
import com.mygdx.catte.UI.StartUI;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/12/2018.
 */

public class UIController extends Group {
    public static UIController Instance;
    public PlayingUI playingUI;
    public StartUI startUI;
    public OptionUI optionUI;
    public GuideUI guideUI;
    public SettingUI settingUI;
    public PauseUI pauseUI;
    public ResumeUI resumeUI;
    public OverlayUI overlayUI;
    public FireWorkUI fireWorkUI;
    public FBLeaderboardUI fbLeaderboardUI;


    public UIController(){
        Instance=this;
        CreateUI();
        AddUI();
        UpdateOption();
        UpdateSetting();
        Utilities.LoadZenGame();
    }

    private void CreateUI() {
        overlayUI=new OverlayUI();
        playingUI=new PlayingUI();
        startUI=new StartUI();
        optionUI=new OptionUI();
        guideUI=new GuideUI();
        settingUI=new SettingUI();
        pauseUI=new PauseUI();
        resumeUI=new ResumeUI();
        fireWorkUI=new FireWorkUI();
        fbLeaderboardUI = new FBLeaderboardUI();
    }
    private void AddUI() {
        addActor(overlayUI);
        GameUI.ShowNonAction(startUI);
    }

    public void onClickChangeTypeCard(int index)
    {
        Preferences preferences= PreferenceController.Instance.preferences;
        preferences.putInteger("TypeCard", index);
        preferences.flush();
        UpdateSetting();
    }
    public void onClickChangeSound(){
        Preferences preferences= PreferenceController.Instance.preferences;
        GameSetting.Instance.isSound=!GameSetting.Instance.isSound;
        preferences.putBoolean("IsSound",GameSetting.Instance.isSound);
        preferences.flush();
        UpdateSetting();
    }
    public void onClickChangeShowGuide(){
        Preferences preferences= PreferenceController.Instance.preferences;
        GameSetting.Instance.isShowGuide=!GameSetting.Instance.isShowGuide;
        preferences.putBoolean("IsShowGuide",GameSetting.Instance.isShowGuide);
        preferences.flush();
        UpdateSetting();
    }


    public void onClickChangeScoreBet(int index)
    {
        Preferences preferences=PreferenceController.Instance.preferences;
        if (index>preferences.getInteger("MaxScoreBet"))
        {
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Mức cược đang khóa!");
            return;
        }
        preferences.putInteger("ScoreBet",index);
        preferences.flush();
        UpdateOption ();
        if (!CheckTienCuoc ()) {
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Bạn cần "+Utilities.ShortCutMoney(GameController.GetScoreBet()*10)+" để chơi mức cược này!");
        }
        else
        {
            UIController.Instance.optionUI.txtContent.setVisible(false);
        }
    }
    public void onClickChangeGuiBai(){
        Preferences preferences= PreferenceController.Instance.preferences;
        boolean isGuibai = preferences.getBoolean("IsGuiBai");
        isGuibai=!isGuibai;
        preferences.putBoolean("IsGuiBai",isGuibai);
        preferences.flush();
        UpdateOption();

    }
    public Array<Integer> ListScoreBet()
    {
        Array<Integer> list_score_bet = new Array<Integer> ();
        list_score_bet.add (5000);
        list_score_bet.add (10000);
        list_score_bet.add (20000);
        list_score_bet.add (50000);
        list_score_bet.add (100000);
        list_score_bet.add (200000);
        list_score_bet.add (500000);
        list_score_bet.add (1000000);
        return list_score_bet;
    }
    void UpdateSetting(){
        Preferences preferences=PreferenceController.Instance.preferences;
        int currentTypeCard = preferences.getInteger("TypeCard");
        for (int i=0;i<4;i++)
        {
            for (Button button:settingUI.arrButtonTypeCards)
                button.getColor().a=0.4f;
        }
        settingUI.arrButtonTypeCards[currentTypeCard].getColor().a=1;
        GameSetting.Instance.textureTypeCard= Assets.GetTexture(Assets.GetCardType(currentTypeCard));

        GameSetting.Instance.isSound=preferences.getBoolean("IsSound");
        SetCheckBox(settingUI.btnSound,GameSetting.Instance.isSound);
        GameSetting.Instance.isShowGuide=preferences.getBoolean("IsShowGuide");
        SetCheckBox(settingUI.btnShowGuide,GameSetting.Instance.isShowGuide);
    }
    void UpdateOption()
    {
        optionUI.UpdateButton();
        boolean isGuiBai = PreferenceController.Instance.preferences.getBoolean("IsGuiBai");
        GameSetting.Instance.isGuiBai=isGuiBai;
        if (isGuiBai)
        {
            GameUI.SetTextureForButton(optionUI.btnGuiBai, Assets.GetTexture(Assets.btnActive));
        }
        else
            GameUI.SetTextureForButton(optionUI.btnGuiBai, Assets.GetTexture(Assets.btnUnActive));
    }

    public void CheckBackButton() {
        if (fbLeaderboardUI.hasParent()){
            fbLeaderboardUI.onClickExit();
            return;
        }
        if (guideUI.hasParent()){
            guideUI.onClickExitGuide();
            return;
        }
        if (optionUI.hasParent()){
            return;
        }
        if (pauseUI.hasParent()){
            pauseUI.onClickExitPause();
            return;
        }

        if (resumeUI.hasParent()){
            resumeUI.onClickExitQuit();
            return;
        }
        if (settingUI.hasParent()){
            settingUI.onClickExitSetting();
            return;
        }
        if (playingUI.hasParent()){
            playingUI.onClickPause();
            return;
        }

       // first: BoxGiftUI, DailyGiftUI, DailyLoginUI,DailyQuestUI, GuideUI,InfoUI,MoreAppUI,
        // OptionUI,PauseUI,QuitUI,ResumeUI,RewardCoinUI,SettingUI,VideoRewardUI
       // second: BoxGiftUI,InfoAndResultUI, luckySpinUI,PlayingUI
        //third:startUI
    }
    public void onClickStartMenu()
    {
        if (DataModel.Instance.IsResume()) {
            OverlayUI.Instance.Show();
            GameUI.Show(UIController.Instance.resumeUI);
        } else {
            OverlayUI.Instance.Show();
            UIController.Instance.optionUI.Show();
            GameUI.Hide(UIController.Instance.resumeUI);
        }
    }
    public void onClickResumeLastGame()
    {
        OverlayUI.Hide();
        GameUI.HideNonAction(UIController.Instance.startUI);
        GameUI.Hide(UIController.Instance.optionUI);
        GameUI.Hide(UIController.Instance.resumeUI);
        GameUI.ShowNonAction(UIController.Instance.playingUI);
        GameController.Instance.NewGame (true);
    }
    public void onClickNewGamePanelResume()
    {
        TruTienViThoatGiuaChung ();
        DataModel.Instance.Reset();
        GameUI.Hide(UIController.Instance.resumeUI);
        UIController.Instance.optionUI.Show();
    }
    public void onClickExitPanelResume()
    {
        OverlayUI.Hide();
        GameUI.Hide(UIController.Instance.resumeUI);
    }
    public void onClickStartNewGame()
    {
        Preferences preferences=PreferenceController.Instance.preferences;
        if (preferences.getInteger("ScoreBet")>preferences.getInteger("MaxScoreBet"))
        {
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Mức cược đang khóa!");
            return;
        }
        if (!CheckTienCuoc ()) {
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Bạn cần "+Utilities.ShortCutMoney(GameController.GetScoreBet()*10)+" để chơi mức cược này!");
            return;
        }
        OverlayUI.Hide();
        GameUI.HideNonAction(UIController.Instance.startUI);
        UIController.Instance.optionUI.Hide();
        GameUI.ShowNonAction(UIController.Instance.playingUI);
        GameController.Instance.NewGame (false);
    }
    boolean CheckTienCuoc()
    {
        if(PreferenceController.Instance.preferences.getInteger("ScoreBet")==0)
            return true;
        long current_score = Long.parseLong(PreferenceController.Instance.preferences.getString ("HighScore"));
        int tien_cuoc = GameController.GetScoreBet();
        return current_score >= tien_cuoc * 10;
    }
    void TruTienViThoatGiuaChung()
    {
        if (!DataModel.Instance.IsResume())
            return;
        int score = -10*GameController.GetScoreBet();
        GameModel.Instance.GetMainPlayer().ChangeScoreNotHaveInfo(score);
        UIController.Instance.startUI.SetCoin();
        DataModel.Instance.Reset();
    }
    public void onClickBoc(){

        GameController.Instance.DisableBocVaAn();
        GameController.Instance.gameModel.GetMainPlayer().Boc();
    }
    public void onClickAn(){

        GameController.Instance.DisableBocVaAn();
        GameController.Instance.gameModel.GetMainPlayer().An();
    }
    public void onClickDanh(){

        if (GameController.Instance.CheckCurrentCard())
            GameController.Instance.gameModel.GetMainPlayer().Danh(GameController.currentChooseCard);
    }
    public void onClickHa(){

        GameController.Instance.HaBaiForMainPlayer();
    }
    public void onClickKetThucGuiBai(){
        GameController.Instance.gameModel.GetMainPlayer().SetEndGuiBai();
    }
    public void onClickGuiBai(){

        GameController.Instance.GuiBaiForMainPlayer();
    }
    public void onClickXep(){

        GameController.Instance.SortListForMainPlayer();
    }
    public void SetCheckBox(Button btn, boolean is_checked)
    {
        if (is_checked)
            GameUI.SetTextureForButton(btn, Assets.GetTexture(Assets.btnActive));
        else
            GameUI.SetTextureForButton(btn, Assets.GetTexture(Assets.btnUnActive));
    }
    public void AddCoinReward(int coin){
        Preferences preferences=PreferenceController.Instance.preferences;
        long current_coin = Long.parseLong(preferences.getString("HighScore"));
        current_coin+=coin;
        preferences.putString("HighScore",current_coin+"");
        preferences.flush();
        startUI.UpdateInfoView();
        //LeaderBoardUI.Instance.UpdateLeaderBoard();
    }
    public void AddCoinRewardNotShowUI(int coin){
        Preferences preferences=PreferenceController.Instance.preferences;
        long current_coin = Long.parseLong(preferences.getString("HighScore"));
        current_coin+=coin;
        preferences.putString("HighScore",current_coin+"");
        preferences.flush();
        //rewardCoinUI.ShowReward(coin);
        startUI.UpdateInfoView();
       // LeaderBoardUI.Instance.UpdateLeaderBoard();
    }
    public void locateActors(){
        startUI.locateActors();
        playingUI.locateActors();
        optionUI.locateActors();

        overlayUI.locateActors();
    }
}
