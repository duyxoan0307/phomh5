package com.mygdx.catte.AssetManager;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.mygdx.catte.Model.BoardModel;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.MyLinkedMap;

import java.util.Dictionary;

/**
 * Created by DUY on 8/9/2018.
 */

public class Assets {
    public static AssetManager manager;
    public Assets(){
        manager = new AssetManager();
    }

    //texture
    public static final String backgroundGamePlay="UI/backgroundGamePlay.jpg";
    public static final String backgroundStart="UI/backgroundStart.jpg";
    public static final String boxInfo="UI/boxInfo.png";
    public static final String boxNameMenu="UI/boxNameMenu.png";
    public static final String boxTienCuoc="UI/boxTienCuoc.png";
    public static final String btnActive="UI/btnActive.png";
    public static final String btnAnBai="UI/btnAnBai.png";
    public static final String btnAvatarMenu="UI/btnAvatarMenu.png";
    public static final String btnBatDauMenu="UI/btnBatDauMenu.png";
    public static final String btnBoc="UI/btnBoc.png";
    public static final String btnCaiDat="UI/btnCaiDat.png";
    public static final String btnDanh="UI/btnDanh.png";
    public static final String btnGiveCards="UI/btnGiveCards.png";
    public static final String btnGreen="UI/btnGreen.png";
    public static final String btnGuide="UI/btnGuide.png";
    public static final String btnHaBai="UI/btnHaBai.png";
    public static final String btnMenu="UI/btnMenu.png";
    public static final String btnTopDaiGia="UI/btnTopDaiGia.png";
    public static final String btnUnActive="UI/btnUnActive.png";
    public static final String btnVanMoi="UI/btnVanMoi.png";
    public static final String btnXep="UI/btnXep.png";
    public static final String logo="UI/logo.png";
    public static final String btnTiepTuc="UI/btnTiepTuc.png";
    public static final String btnThoat="UI/btnThoat.png";

    public static final String imgBox="UI/imgBox.png";
    public static final String imgChayBai="UI/imgChayBai.png";
    public static final String imgDenLang="UI/imgDenLang.png";
    public static final String money_overlay="UI/money_overlay.png";
    public static final String overlay="UI/overlay.jpg";
    public static final String model="UI/model.png";
    public static final String btnExit="UI/btnExit.png";
    public static final String imgArrow="UI/imgArrow.png";
    public static final String textPhom="UI/textPhom.png";
    public static final String lock_icon="UI/lock_icon.png";


    //fb
    public static final String fbleaderboard_box="UI/fbleaderboard_box.png";
    public static final String fbleaderboard_bar="UI/fbleaderboard_bar.png";


    public static final String goldText="Fonts/goldText.fnt";
    public static final String silver_text="Fonts/silver_text.fnt";
    public static final String purpleRankpurpleRank="Fonts/purpleRankpurpleRank.fnt";
    public static final String Saira_ExtraBold="Fonts/Saira_ExtraBold.fnt";
    public static final String Saira_Regular="Fonts/Saira_Regular.fnt";


    public static final String GetScoreBet(int index){
        return "UI/scoreBet"+index+".png";
    }
    public static final String GetAvatar(int index){
        if (index>13)
            index=13;
        return "UI/ava_"+index+".png";
    }
    public static final String GetCry(int index){
        return "UI/emoticon_cry_"+index+".png";
    }
    public static final String GetSmile(int index){
        return "UI/emoticon_smile_"+index+".png";
    }
    public static final String GetHang(int index){
        return "UI/imgHang"+index+".png";
    }
    public static final String GetU(int index){
        return "UI/imgU"+index+".png";
    }
    public static final String GetCard(int index){
        CardModel card=BoardModel.Instance.listCard.get(index);
        String card_name="";
        if (card.number>=3 && card.number<=13)
            card_name=(card.number-3)+"_"+CardModel.GetNameTypeCardEnglish(card.type);
        else
            card_name=(card.number+10)+"_"+CardModel.GetNameTypeCardEnglish(card.type);
        return "CardsImage/"+card_name+".png";
    }
    public static final String GetCardType(int index){
        return "CardsImage/back_"+index+".png";
    }
    public static void load(){


        LoadBitmapFont(goldText);
        LoadBitmapFont(silver_text);
        LoadBitmapFont(purpleRankpurpleRank);
        LoadBitmapFont(Saira_ExtraBold);
        LoadBitmapFont(Saira_Regular);

    }
    static MyLinkedMap<String,Texture> listTexture=new MyLinkedMap<String, Texture>();
    public static Texture GetTexture(String name){
        if (listTexture.containsKey(name))
            return listTexture.get(name);
        Texture texture=new Texture(name);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        listTexture.put(name,texture);
        return listTexture.get(name);
    }
    public static void dispose(){
        manager.dispose();
    }

    static void LoadBitmapFont(String link){
        manager.load(link, BitmapFont.class);
    }
    public static BitmapFont GetBitmapFont(String name){
        BitmapFont font = manager.get(name,BitmapFont.class);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return font;
    }
}
