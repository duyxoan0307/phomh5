package com.mygdx.catte.Utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Model.BoardModel;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Preferences.PreferenceController;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by DUY on 6/14/2018.
 */

public class Utilities {
    public static String GetNameOfPlayer(int i)
    {
        switch (i) {
            case 0:
                return "bigworld_02";
            case 1:
                return "Wade Barr";
            case 2:
                return "surprise_v1";
            case 3:
                return "fight_fury";
            case 4:
                return "quatinh";
            case 5:
                return "viking119";
            case 6:
                return "grannous1";
            case 7:
                return "deluxtimes";
            case 8:
                return "shoter";
            case 9:
                return "senkkt";
            case 10:
                return "no.thing00";
            case 11:
                return "biibii";
            case 12:
                return "Ori_Zen";
            default:
        }
        return "null";
    }
    public static String ShortCutMoney(long money){
      /*  DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);
        return "$"+formatter.format(money);
        */
      //xxxxxx
      return "$" + money;
    }
    public static void LoadZenGame()
    {
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                String jsonString = HttpRequest.SendGetRequest("http://api.bonanhem.com/z/com.phomtala");
                JsonReader jsonReader = new JsonReader();
                try
                {
                    JsonValue value = jsonReader.parse(jsonString);
                    JsonValue crossData = value.get("crossData");
                    List<JsonValue> list = new ArrayList<JsonValue>();
                    for (int i=0;i<crossData.size;i++)
                        list.add(crossData.get(i));
                    Collections.shuffle(list);
                    int len = list.size()>8?8:list.size();
                    final String[] link = new String[len];
                    final String[] name = new String[len];
                    final String[] packageName = new String[len];
                    for (int i=0;i<len;i++)
                    {
                        name[i] = list.get(i).getString("name_vi");
                        packageName[i] = list.get(i).getString("packageUrl");
                        link[i] = list.get(i).getString("icon128");
                    }
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture[] drawable = new Texture[link.length];
                            for (int i=0;i<link.length;i++)
                            {
                                drawable[i] = new TextureDownload(link[i],180,180).texture;
                                drawable[i].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            }
                            UIController.Instance.moreAppUI.SetValue(name,drawable,packageName);
                        }
                    });
                }catch (Exception ex)
                {

                }
            }
        }).start();*/
    }
    public static void ImportDataCards(String data, Array<CardModel> list)
    {
        int[] arrI = new int[0];
        if (data.length() <= 2)
            return;
        data = data.substring (1, data.length());
        String[] arrS = data.split(",");
        arrI = new int[arrS.length];
        for (int i = 0; i < arrS.length; i++)
        {
            String string_card = arrS [i];
            String[] data_card = string_card.split ("\\|");
            CardModel card = GetCardModelFromData (data_card[0],data_card[1]);
            list.add (card);
        }
    }
    public static void ImportDataCards(String data, CardModel[] list)
    {
        int[] arrI = new int[0];
        if (data.length() <= 2)
            return;
        data = data.substring (1, data.length());
        String[] arrS = data.split(",");
        arrI = new int[arrS.length];
        for (int i = 0; i < arrS.length; i++)
        {
            String string_card = arrS [i];
            String[] data_card = string_card.split ("\\|");
            CardModel card = GetCardModelFromData (data_card[0],data_card[1]);
            list[i]=card;
        }
    }
    public static CardModel GetCardModelFromData(String number,String type)
    {
        CardModel card_model = new CardModel (Integer.parseInt(number), CardModel.CardType.values()[Integer.parseInt(type)]);
        for (CardModel card : BoardModel.Instance.listCard)
        if (CardModel.CompareCards (card, card_model)==0)
            return card;
        return null;
    }
    public static int[] StringToArrayInt(String s)
    {
        int[] arrI = new int[0];
        if (s.length() <= 2)
            return arrI;
        s = s.substring (1, s.length());
        String[] arrS = s.split(",");
        arrI = new int[arrS.length];
        for (int i = 0; i < arrS.length; i++)
        {
            arrI[i] = Integer.parseInt(arrS[i]);
        }
        return arrI;
    }
    public static void SaveListCardToPreference(Array<CardModel> arr,String prefString){
        String s="";
        for (int i = 0; i < arr.size; i++) {
            s += "," + arr.get(i).number + "|" + (int)arr.get(i).type.ordinal(); // ,5|1 => 5 chuon
        }
        PreferenceController.Instance.preferences.putString(prefString,s);
        PreferenceController.Instance.preferences.flush();
    }
    public static void SaveCardToPreference(CardModel card,String prefString){
        if (card==null)
            return;
        Array<CardModel> arr=new Array<CardModel>();
        arr.add(card);
        String s="";
        for (int i = 0; i < arr.size; i++) {
            s += "," + arr.get(i).number + "|" + (int)arr.get(i).type.ordinal(); // ,5|1 => 5 chuon
        }
        PreferenceController.Instance.preferences.putString(prefString,s);
        PreferenceController.Instance.preferences.flush();
    }
    public static void ImportDataCardModel(String data, CardModel card_model)
    {
        int[] arrI = new int[0];
        if (data.length() <= 2)
            return;
        data = data.substring (1, data.length());
        String[] arrS = data.split(",");
        arrI = new int[arrS.length];
        for (int i = 0; i < arrS.length; i++)
        {
            String string_card = arrS [i];
            String[] data_card = string_card.split ("\\|");
            CardModel card = GetCardModelFromData (data_card[0],data_card[1]);
            if (card!=null)
                card_model=card;
        }
    }
}
