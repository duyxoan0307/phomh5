package com.mygdx.catte.Utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Screen.GameScreen;

/**
 * Created by DUY on 6/13/2018.
 */
public class GameUI extends Group {

    //static
    public static BitmapFont NewFont(String font_name){
        BitmapFont.BitmapFontData fontData = new BitmapFont.BitmapFontData(Gdx.files.internal("Fonts/"+font_name+".fnt"),false);
        TextureRegion fontRegion=new TextureRegion(new Texture("Fonts/"+font_name+".png"));
        BitmapFont font = new BitmapFont(fontData,fontRegion,false);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return font;
    }

    public static Label NewLabel(String content, String font_name)
    {
        BitmapFont font = NewFont(font_name);
        Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.WHITE);
        Label label = new Label(content, labelStyle);
        //   label.setWidth(0);
        label.setAlignment(Align.center);
        return label;
    }

    public static Button NewButton(Texture texture)
    {
        TextureRegion tr = new TextureRegion(texture);
        TextureRegionDrawable td = new TextureRegionDrawable(tr);
        Button.ButtonStyle buttonStyle = new Button.ButtonStyle(td, td, td);

        final Button button = new Button(buttonStyle);
        button.setTransform(true);
        button.setBounds(0,0,texture.getWidth(),texture.getHeight());
        button.addListener(new IClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                button.addAction(NewSequenceAction(Color.GRAY,0.2f,Color.WHITE,0.2f));
            }
        });


        return button;
    }
    public static TextButton NewTextButton(String content, float size, Color cl, Texture textureButton, String font)
    {
        final TextButton textButton;
        TextButton.TextButtonStyle tbs = new TextButton.TextButtonStyle();
        tbs.fontColor=cl;
        tbs.font = NewFont(font);
        tbs.up = new TextureRegionDrawable( new TextureRegion(textureButton) );
        textButton = new TextButton(content, tbs);
        textButton.getLabel().setFontScale(size);
        textButton.getLabel().getStyle().font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        textButton.addListener(new IClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (textButton.getText().toString().equals(""))
                    return;
                textButton.addAction(NewSequenceAction(Color.GRAY,0.2f,Color.WHITE,0.2f));
            }
        });
        textButton.setTransform(true);
        return textButton;
    }
    public static TextButton NewTextButton(String content, float size, Color cl, Texture textureButton, BitmapFont font)
    {
        final TextButton textButton;
        TextButton.TextButtonStyle tbs = new TextButton.TextButtonStyle();
        tbs.fontColor=cl;
        tbs.font = font;
        tbs.up = new TextureRegionDrawable( new TextureRegion(textureButton) );
        textButton = new TextButton(content, tbs);
        textButton.getLabel().setFontScale(size);
        textButton.getLabel().getStyle().font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        textButton.addListener(new IClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (textButton.getText().toString().equals(""))
                    return;
                textButton.addAction(NewSequenceAction(Color.GRAY,0.2f,Color.WHITE,0.2f));
            }
        });
        textButton.setTransform(true);
        return textButton;
    }
    public static Slider NewSlider(int min,int max,float stepSize,boolean verticle,Texture background,Texture knob){
        Slider.SliderStyle ss = new Slider.SliderStyle();
        ss.background = new TextureRegionDrawable(new TextureRegion(background));
        ss.knob = new TextureRegionDrawable(new TextureRegion(knob));
        Slider slider = new Slider(0f, 100f, 1f, false, ss);
        return slider;
    }
    public static void Show(final Group group)
    {
        group.setTouchable(Touchable.enabled);
        group.clearActions();
        group.addAction(Actions.sequence(Actions.moveToAligned(-Phom.V_WIDTH,0,Align.right),Actions.moveToAligned(0,0,Align.center,0.5f,Interpolation.swingOut)));
        group.toFront();

        //add Actor
        UIController.Instance.addActor(group);
    }
    public static void ShowFade(final Group group)
    {
        group.setTouchable(Touchable.enabled);
        group.clearActions();
        group.setPosition(0, 0);

        group.addAction(Actions.fadeIn(0.3f));
        group.toFront();

        //add Actor
        UIController.Instance.addActor(group);
    }
    public static void ShowNonAction(Group group){
        group.setTouchable(Touchable.enabled);
        group.clearActions();
        group.setPosition(0,0);
        UIController.Instance.addActor(group);
        group.toFront();
    }
    public static void HideFade(final Group group)
    {
        group.setTouchable(Touchable.disabled);
        group.clearActions();
        group.clearActions();
        group.setPosition(0, 0);

        group.addAction(Actions.sequence(Actions.fadeOut(0.3f),Actions.run(new Runnable() {
            @Override
            public void run() {
                UIController.Instance.removeActor(group);
            }
        })));
        group.toFront();
    }
    public static void Hide(final Group group)
    {
        group.setTouchable(Touchable.disabled);
        group.clearActions();
        group.addAction(Actions.sequence(Actions.moveToAligned(Phom.V_WIDTH,0,Align.left,0.5f,Interpolation.swingIn),Actions.run(new Runnable() {
            @Override
            public void run() {
                UIController.Instance.removeActor(group);
            }
        })));
    }
    public static void HideNonAction(Group group)
    {
        group.setTouchable(Touchable.disabled);
        group.clearActions();
        UIController.Instance.removeActor(group);
    }
    public static void ScaleLabel(Label label, float font_size){
        label.setFontScale(font_size);
    }
    public static Color NewColor(float r, float g,float b, float a){
        return new Color(r/255f,g/255f,b/255f,a);
    }
    public static Color NewColor (String hex) {
        hex = hex.charAt(0) == '#' ? hex.substring(1) : hex;
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        int a = hex.length() != 8 ? 255 : Integer.valueOf(hex.substring(6, 8), 16);
        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }
    public static SequenceAction NewSequenceAction(Color cl1, float t1 , Color cl2, float t2)
    {
        ColorAction colorGreenAction = new ColorAction();
        colorGreenAction.setDuration(t1);
        colorGreenAction.setEndColor(cl1);

        ColorAction colorWhiteAction = new ColorAction();
        colorWhiteAction.setDuration(t2);
        colorWhiteAction.setEndColor(cl2);

        return new SequenceAction(colorGreenAction,colorWhiteAction);
    }
    public static Texture NewTexture(String file){
        Texture texture=new Texture(Gdx.files.internal(file));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return texture;
    }
    public static void SetPositionActor(Actor actor,float x,float y){
        actor.setPosition(x-actor.getWidth()*actor.getScaleX()/2,y-actor.getHeight()*actor.getScaleY()/2);
    }
    public static void SetTextureForImage(Image image, Texture texture){
        image.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
    }
    public static void SetTextureForButton(Button myButton,Texture texture){
        TextureRegion tr = new TextureRegion(texture);
        TextureRegionDrawable td = new TextureRegionDrawable(tr);
        Button.ButtonStyle buttonStyle = new Button.ButtonStyle(td, td, td);
        myButton.setStyle(buttonStyle);
    }
    public static void SetTextureForTextButton(TextButton myButton,Texture texture){
        TextureRegion tr = new TextureRegion(texture);
        TextureRegionDrawable td = new TextureRegionDrawable(tr);
        TextButton.ButtonStyle buttonStyle=myButton.getStyle();
        buttonStyle.up = new TextureRegionDrawable( new TextureRegion(texture) );
    }
    public static Vector2 GetPositionCenterOfActor(Actor actor){
        return new Vector2(actor.getX()+actor.getWidth()/2*actor.getScaleX(),actor.getY()+actor.getHeight()/2*actor.getScaleY());
    }
    public static void FitLabel(Label text){
        if (text.getWidth()<text.getPrefWidth())
        {
            GameUI.ScaleLabel(text, text.getStyle().font.getData().scaleX * text.getWidth()/text.getPrefWidth());
        }
    }
    public static void ResizeByScale(Actor actor,float scale)
    {
        actor.setSize(actor.getWidth()*scale,actor.getHeight()*scale);
    }

    public static void FitLabel(Label text,float max_size){
        ScaleLabel(text,max_size);
        if (text.getWidth()<text.getPrefWidth())
        {
            GameUI.ScaleLabel(text, text.getScaleX() * text.getWidth()/text.getPrefWidth());
        }
    }
    public static void SetTextAndFit(Label label,String text,float max_size){
        label.setText(text);
        FitLabel(label,max_size);
    }
    public static void ScaleParticleEffect(ParticleEffect pe, float scale)
    {
        for (ParticleEmitter emitter:pe.getEmitters())
            emitter.scaleSize(scale);
    }
    public static Label NewLabel(String content, Label.LabelStyle labelStyle)
    {
        Label label = new Label(content, labelStyle);
        //   label.setWidth(0);
        label.setAlignment(Align.center);
        return label;
    }
    public static Label NewLabel(String content, BitmapFont bitmapFont)
    {
        return NewLabel(content,new Label.LabelStyle(bitmapFont,Color.WHITE));
    }
}