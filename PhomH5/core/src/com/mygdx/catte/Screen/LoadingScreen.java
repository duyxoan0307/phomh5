package com.mygdx.catte.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.UI.LoadingUI;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 5/14/2018.
 */

public class LoadingScreen extends ScreenAdapter{

    SpriteBatch batch;
    Stage stage;
    LoadingUI loadingUI;
    GameScreen gameScreen;

    public LoadingScreen() {
        new Assets();
        batch = new SpriteBatch();
        OrthographicCamera camera = new OrthographicCamera();
        Viewport viewPort = new ExtendViewport(Phom.V_WIDTH, Phom.V_HEIGHT, camera);
        stage = new Stage(viewPort, batch);
        camera.position.set(0, 0, 0);
        Gdx.input.setInputProcessor(stage);
        Gdx.input.setCatchBackKey(true);

        gameScreen=new GameScreen();

        loadingUI=new LoadingUI();
        stage.addActor(loadingUI);

       /* Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                Assets.load();
                is_loaded=true;
            }
        });
        thread.start();
        */
       //xxxxx


        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                Assets.load();
                is_loaded=true;
            }
        });



    }

    void AfterLoading(){
       /* Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        gameScreen.AfterLoading();
                        Phom.Instance.setScreen(gameScreen);
                    }
                });
            }
        });
        thread.start();
        */
        //xxxxx
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {

                Phom.zenSDK.FBInstant_GetDoubleStats("money", new IZen.FBInstant_GetStatsCallback() {
                    @Override
                    public void OnValue(double value, boolean undefined) {
                        if(undefined == true){
                            value = 1000000;
                            Phom.zenSDK.FBInstant_SetDoubleStats("money", value, null);
                        }

                        Preferences preferences= Gdx.app.getPreferences("Phom");
                        preferences.putString("HighScore", "" + (long)value);
                        preferences.flush();


                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {

                                gameScreen.AfterLoading();
                                Phom.Instance.setScreen(gameScreen);
                                dispose();
                            }
                        });


                    }
                });
            }
        });


    }
    boolean is_datasyncing = false;
    boolean is_loaded=false;
    @Override
    public void render(float delta) {
//        Gdx.gl.glClearColor(1, 1, 1, 1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (!is_loaded || !Assets.manager.update())
        {
            float percent=Assets.manager.getProgress();
          //  loadingUI.SetPercent((int)(percent*100));
        }
        else if(is_datasyncing == false)
        {
            is_datasyncing = true;
            AfterLoading();
        }
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
        int w = (int)stage.getViewport().getWorldWidth();
        int h = (int)stage.getViewport().getWorldHeight();
        Phom.V_HEIGHT = h;
        Phom.V_WIDTH = w;
        locateActors();
    }

    @Override
    public void show() {
        super.show();
        Phom.zenSDK.OnShow();
    }

    public void locateActors(){
        loadingUI.locateActors();
    }

    @Override
    public void dispose() {
        batch.dispose();
        loadingUI.dispose();
    }
}