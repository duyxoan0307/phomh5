package com.mygdx.catte.Screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.UI.LoadingUI;

import javax.swing.text.TableView;


/**
 * Created by DUY on 5/14/2018.
 */

public class GameScreen extends ScreenAdapter{

    SpriteBatch batch;
    Stage stage;

    public GameScreen() {
        batch = new SpriteBatch();
        OrthographicCamera camera = new OrthographicCamera();
        Viewport viewPort = new ExtendViewport(1334,750, camera);
        stage = new Stage(viewPort, batch);
        camera.position.set(0, 0, 0);
        InputAdapter webGlfullscreen = new InputAdapter() {
            @Override
            public boolean keyUp (int keycode) {
                if (keycode == Input.Keys.ENTER && Gdx.app.getType() == Application.ApplicationType.WebGL) {
                    if (!Gdx.graphics.isFullscreen())
                        Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayModes()[0]);
                }
                return true;
            }
        };
        Gdx.input.setInputProcessor(new InputMultiplexer(webGlfullscreen, stage));
        Gdx.input.setCatchBackKey(true);

        //test
        //Test();
        new SoundController();
    }
    public void AfterLoading(){
        //game controller
        stage.addActor(new GameController());
        //UIcontroller
        stage.addActor(new UIController());
    }
    void Test() {
    }

    @Override
    public void render(float delta) {
//        Gdx.gl.glClearColor(1, 1, 1, 1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }


    boolean resized = false;
    int lastw = 0;
    int lasth = 0;
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
        int w = (int)stage.getViewport().getWorldWidth();
        int h = (int)stage.getViewport().getWorldHeight();

//        int maxh = (int)(w*3f/4f);
//        if(h > maxh)
//            h = maxh;

        if(lastw != w || lasth !=h)
            resized = false;

        Phom.V_HEIGHT = h;
        Phom.V_WIDTH = w;

        if(resized==false) {
            resized = true;
            locateActors();
        }
    }
    public void locateActors(){
        UIController.Instance.locateActors();
        GameController.Instance.locateActors();
    }
    @Override
    public void dispose() {
        batch.dispose();
        Assets.dispose();
    }
}