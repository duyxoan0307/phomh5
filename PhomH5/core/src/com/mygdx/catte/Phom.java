package com.mygdx.catte;

import com.badlogic.gdx.Game;
import com.mygdx.catte.Screen.GameScreen;
import com.mygdx.catte.Screen.LoadingScreen;

public class Phom extends Game {
	public static IZen zenSDK;
	public static int V_WIDTH = 1334;
	public static int V_HEIGHT = 750;
	public static Phom Instance;
	public Phom(IZen zensdk){
		zenSDK=zensdk;
	}
	@Override
	public void create() {
		Instance=this;
		setScreen(new LoadingScreen());
	}
	public void startGame(){
		AskShortcut();
	}
	public void AskShortcut(){
		zenSDK.FBInstant_CanCreateShortCut(new IZen.FBInstant_CanCreateCallback() {
			@Override
			public void OnCallback(boolean canCreateShortcut) {
				if(canCreateShortcut)
					zenSDK.FBInstant_CreateShortCut();
				else
					zenSDK.FBInstant_CanSubscribeBot();
			}
		});
	}
}
