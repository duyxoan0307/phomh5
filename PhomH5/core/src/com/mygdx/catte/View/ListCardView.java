package com.mygdx.catte.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;

/**
 * Created by DUY on 6/12/2018.
 */

public class ListCardView extends Group {
    public float width;
    public float height;
    public Array<CardView> listCardViews;
    public float scale;
    public int index;
    Group group;
    public ListCardView(float scale, int index){
        this.scale=scale;
        this.index=index;
        CardView cardView=new CardView();
        cardView.SetImage(Assets.GetTexture(Assets.GetCard(0)));
        this.width= Assets.GetTexture(Assets.GetCard(0)).getWidth()*scale;
        this.height= Assets.GetTexture(Assets.GetCard(0)).getHeight()*scale;
        listCardViews=new Array<CardView>();
        //test
        //this.addActor(cardView);
        cardView.image.setSize(width,height);
        cardView.image.setPosition(getX()-width/2,getY()-height/2);
        //SortList();
        group=new Group();
        addActor(group);
    }
    public void AddCardView(CardView cardView){
        listCardViews.add(cardView);
        cardView.SetParent(this);
        group.addActor(cardView);
    }
    public void SortCenterDelay(float distance_X){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;
        for (final CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            final float offset = (current - center) * width * distance_X + is_not_odd* distance_X * width /2;
            cardView.SetPosition(getX()-width/2+offset,getY()-height/2-400);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            cardView.image.addAction(Actions.moveTo(getX()-width/2+offset,getY()-height/2,0.5f, Interpolation.swingOut));
                        }
                    },0.2f*listCardViews.indexOf(cardView,false));
                }
            },0.5f);
        }
    }
    public void SortCenter(float distance_X){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = (current - center) * width * distance_X + is_not_odd* distance_X * width /2;

            float y;
            if (cardView.positionType== CardView.PositionType.UP && cardView.canClick)
            {
                y=cardView.image.getY();
                GameController.Instance.imgArrowGuide.setPosition(getX()-width/2+offset+cardView.image.getWidth()/2,GameController.Instance.imgArrowGuide.getY(Align.center),Align.center);
            }
            else
                y=getY()-height/2;
            cardView.SetPosition(getX()-width/2+offset,y);
        }
    }
    public void SortLeft(float distance_X){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = current*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortRight(float distance_X){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = (current-listCardViews.size+1)*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortCenterPhom(float distance_X,int index_phom){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order+ 20*index);
            float offset = (current - center) * width * distance_X + is_not_odd* distance_X * width /2;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortLeftPhom(float distance_X,int index_phom){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order+ 20*index);
            float offset = current*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortRightPhom(float distance_X,int index_phom){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order+ 20*index);
            float offset = (current-listCardViews.size+1)*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortCenterVertical(float distance_X){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;

        for (int i=0;i<listCardViews.size;i++)
        {
            CardView cardView=listCardViews.get(i);
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.toBack();
            float offset = (current - center) * height * distance_X + is_not_odd* distance_X * height /2;

            cardView.SetPosition(getX()-width/2,getY()-height/2+offset);
        }
    }
    public void MoveToParent(){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);
            cardView.SetPosition(getX()-width/2,getY()-height/2);
        }
    }
    public void SortZIndexListTrung(){
        for (CardView cardView:listCardViews
                ) {
            cardView.toFront();
        }
    }
    public void UpdateGroup(){
        Vector2 position = this.stageToLocalCoordinates(new Vector2(0,0));
        group.setPosition(position.x,position.y);
    }
    public void Clear(){
        group.clearChildren();
        listCardViews.clear();
        //clearChildren();
    }
}
