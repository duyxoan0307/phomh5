package com.mygdx.catte.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Model.InfoModel;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/14/2018.
 */

public class InfoView extends Group{
    Image imgBoxInfo;
    Label txtCoin;
    Label txtName;
    Image imgEmoticon;
    public Image imgAvatar;


    public InfoView(String name, int coin, int index_avatar){
        CreateAndAddInfo(name, coin, index_avatar);
    }
    void CreateAndAddInfo(String name, int coin, int index_avatar){

        imgBoxInfo=new Image(Assets.GetTexture(Assets.boxInfo));
        imgBoxInfo.setPosition(0,0,Align.center);

        txtName = GameUI.NewLabel(name,Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtName.setAlignment(Align.top);
        txtName.setWidth(180);
        txtName.setWrap(true);
        GameUI.ScaleLabel(txtName,0.7f);
        txtName.setPosition(0,-60,Align.top);

        imgAvatar = new Image(Assets.GetTexture(Assets.GetAvatar(index_avatar)));
        imgAvatar.setSize(90,90);
        imgAvatar.setPosition(0.02f,16.5f,Align.center);

        txtCoin=GameUI.NewLabel("",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtCoin.setWidth(110);
        txtCoin.setAlignment(Align.center);
        txtCoin.setColor(Color.BLACK);
        txtCoin.setPosition(0,-44,Align.top);

        SetCoin(coin);

        imgEmoticon=new Image(Assets.GetTexture(Assets.GetSmile(0)));
        imgEmoticon.setPosition(imgAvatar.getX(Align.center),imgAvatar.getY(Align.center),Align.center);

        //addActor
        addActor(imgAvatar);
        addActor(imgBoxInfo);
        addActor(txtName);
        addActor(txtCoin);
        addActor(imgEmoticon);
    }
    public void SetCoin(long coin){
        GameUI.SetTextAndFit(txtCoin,Utilities.ShortCutMoney(coin),1);}
    public void SetName(String name){txtName.setText(name);}
    public void SetAvatar(Texture texture){GameUI.SetTextureForImage(imgAvatar,texture);};
    public void UpdateInfo(InfoModel infoModel){
        SetCoin(infoModel.coin);
        SetName(infoModel.name);
        SetAvatar(infoModel.avatar);
    }
    public void ShowEmoticon(Texture texture){
        imgEmoticon.setVisible(true);
        GameUI.SetTextureForImage(imgEmoticon,texture);
        imgEmoticon.setPosition(imgAvatar.getX(Align.center),imgAvatar.getY(Align.center),Align.center);
        imgEmoticon.clearActions();
        Action moveLoop = Actions.forever(Actions.sequence(Actions.moveBy(0,50,0.5f), Actions.sequence(Actions.moveBy(0,-50,0.5f))));
        imgEmoticon.addAction(moveLoop);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                imgEmoticon.setVisible(false);
            }
        },2f);
    }
    public void Clear(){
        imgEmoticon.setVisible(false);
    }
}
