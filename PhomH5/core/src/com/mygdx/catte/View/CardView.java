package com.mygdx.catte.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.Model.BoardModel;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/12/2018.
 */

public class CardView extends Group {
    public String name;
    boolean canClick;
    public Image image;
    public ListCardView parent;
    public enum PositionType{UP,DOWN};
    public PositionType positionType;
    public boolean isVisible;
    public static boolean canDownCards;
    int can_boc_hoac_an;
    public CardView(){
        image = new Image(Assets.GetTexture(Assets.GetCard(0)));
        image.setOrigin(Align.center);
        this.addActor(image);
        AddListener();

        Reset();
    }
//    public void SetParent(ListCardView listCardView){
//        this.parent=listCardView;
//    }
    public void SetImage(Texture texture){
        TextureRegionDrawable textureRegionDrawable;
        textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(texture));
        image.setDrawable(textureRegionDrawable);
    }
    void AddListener()
    {
        this.image.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                OnMouseDown();
            }
        });
    }
    void OnMouseDown(){
        if (can_boc_hoac_an>-1)
        {
            if (can_boc_hoac_an==0)
                UIController.Instance.onClickBoc();
            else
                UIController.Instance.onClickAn();
            return;
        }

        if (!canClick)
            return;
        if (!canDownCards)
        {
            if (positionType==PositionType.DOWN)
                MoveCardUp();
            else
                MoveCardDown();
        }
        else
        {
            if (positionType==PositionType.DOWN)
                MoveCardUpOnly();
            else
                MoveCardDown();
        }
    }
    public void MoveCardUpOnly(){
        if (actionsMove!=null)
            this.image.removeAction(actionsMove);
        this.image.addAction(Actions.moveTo(this.image.getX(),parent.getY()-parent.height/2+60f,0.2f));
        ChooseCard(true);
        positionType=PositionType.UP;
    }
    public void SetMoveDown(){
        this.image.addAction(Actions.moveTo(this.image.getX(),parent.getY()-parent.height/2,0.2f));
        positionType=PositionType.DOWN;
    }
    void MoveCardUp(){
        for (CardView cardView:parent.listCardViews){
            //cardView.image.clearActions();
            cardView.image.addAction(Actions.moveTo(cardView.image.getX(),parent.getY()-parent.height/2,0.2f));
            cardView.positionType=PositionType.DOWN;
        }
      //  this.image.clearActions();
        this.image.addAction(Actions.moveTo(this.image.getX(),parent.getY()-parent.height/2+60f,0.2f));
        ChooseCard(true);
        positionType=PositionType.UP;
    }
    void MoveCardDown(){
      //  this.image.clearActions();
        this.image.addAction(Actions.moveTo(this.image.getX(),parent.getY()-parent.height/2,0.2f));
        ChooseCard(false);
        positionType=PositionType.DOWN;
    }
    public void SetName(CardModel card){
        if (this==null)
            return;
        this.name=card.number+"_"+card.type.toString();
    }
    public void SetParent(ListCardView listCardView){
        this.parent=listCardView;
    }

    public void SetCanClick(boolean can_click){
        this.canClick=can_click;
    }
    public void SetVisible(boolean is_visible,CardModel card_model){
        if (this==null)
            return;
        final TextureRegionDrawable textureRegionDrawable;
        if (!is_visible)
            textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(GameSetting.Instance.textureTypeCard));
        else
            textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(Assets.GetTexture(Assets.GetCard(BoardModel.Instance.GetIndexOfCardInBoard(card_model)))));
        image.setDrawable(textureRegionDrawable);

//        SetFlip(is_visible,card_model);
    }
    public void SetFlip(final boolean is_visible, final CardModel card_model){
        if (this==null)
            return;
        if (isVisible==is_visible)
            return;
        isVisible=is_visible;
        final TextureRegionDrawable textureRegionDrawable;
        if (!is_visible)
            textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(GameSetting.Instance.textureTypeCard));
        else
            textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(Assets.GetTexture(Assets.GetCard(BoardModel.Instance.GetIndexOfCardInBoard(card_model)))));
        image.addAction(Actions.sequence(Actions.scaleTo(0,1,0.2f),Actions.run(new Runnable() {
            @Override
            public void run() {
                image.addAction(Actions.parallel(Actions.scaleTo(1,1,0.2f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        image.setDrawable(textureRegionDrawable);
                    }
                })));
            }
        })));
    }
    public void SetFlip(final CardModel card_model, int index){
        if (isVisible==true)
            return;
        isVisible=true;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (card_model==null)
                    return;
                image.addAction(Actions.sequence(Actions.scaleTo(0,1,0.2f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        image.addAction(Actions.parallel(Actions.scaleTo(1,1,0.2f),Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                if (!GameController.Instance.gameModel.isPlaying)
                                    return;
                                TextureRegionDrawable textureRegionDrawable = new TextureRegionDrawable(new TextureRegion(Assets.GetTexture(Assets.GetCard(BoardModel.Instance.GetIndexOfCardInBoard(card_model)))));
                                image.setDrawable(textureRegionDrawable);
                            }
                        })));
                    }
                })));
            }
        },index*0.1f);
    }
    public void SetSize(float width,float height){
        //this.setSize(width,height);
        this.image.addAction(Actions.sizeTo(width,height,0.4f));
        this.image.setOrigin(Align.center);
    }
    Action actionsMove;
    public void SetPosition(float x,float y){
        //if (actionsMove!=null)
          //  this.image.removeAction(actionsMove);
        actionsMove=Actions.moveTo(x,y,0.4f);
        this.image.addAction(actionsMove);
    }

    OnChooseCard cbChooseCard;
    public void ChooseCard(boolean is_choose){
        if (cbChooseCard!=null)
            cbChooseCard.OnEvent(is_choose,canDownCards);
    }
    public void RegisterChooseCard(OnChooseCard cb)
    {
        cbChooseCard=cb;
    }
    public interface OnChooseCard{
        public void OnEvent(boolean is_choose, boolean is_down_cards);
    }
    public void SetShadow(int level){
        this.image.setColor(GameSetting.Instance.colorShadow[level]);
    }
    public void SetCanBocHoacAn(int type){
        can_boc_hoac_an=type;
    }
    public void Reset(){
        this.image.clearActions();
        if (GameSetting.Instance.textureTypeCard!=null)
        {
            this.image.setDrawable(new TextureRegionDrawable(new TextureRegion(GameSetting.Instance.textureTypeCard)));
            this.image.setSize(GameSetting.Instance.textureTypeCard.getWidth(),GameSetting.Instance.textureTypeCard.getHeight());
            GameUI.ResizeByScale(this.image,0.7f);
        }
        SetShadow(0);
        this.isVisible=false;
        this.canClick=false;
        this.positionType=PositionType.DOWN;
        can_boc_hoac_an=-1;
    }
    public void SetFirstDistribute(){
        this.toFront();
        Vector2 new_position=new Vector2(parent.getX()+(float)(-20+Math.random()*40),parent.getY()+(float)(-20+Math.random()*40));
        this.image.addAction(Actions.parallel(Actions.rotateTo((float) (0+Math.random()*360),0.5f),Actions.moveToAligned(new_position.x,new_position.y,Align.center,0.5f)));
    }
}
