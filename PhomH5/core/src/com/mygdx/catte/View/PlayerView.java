package com.mygdx.catte.View;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.Model.PhomModel;
import com.mygdx.catte.Phom;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayerView extends Group {
    public ListCardView listHold;
    public ListCardView listOut;
    public ListCardView listEat;

    public ListCardView[] listPhom;
    public String name;

    public int index;

    public InfoView infoView;

    public PlayerView(int index){
        //set index
        this.index=index;
        this.name="Player"+index;

        //create list
        if (this.index!=0)
            listHold=new ListCardView(0.45f,5);
        else
            listHold=new ListCardView(1f,5);
        listOut=new ListCardView(0.55f,1);
        listEat=new ListCardView(listHold.scale,1);

        listPhom=new ListCardView[3];
        for (int i=0;i<3;i++)
        {
            ListCardView list_phom = new ListCardView(0.45f,2+i);
            listPhom[i]=list_phom;
        }
        //create info view
        CreateInfoView();
        //add actor
        AddActor();
    }

    public ListCardView GetListByIndex(int index){
        if (index==0)
            return listHold;
        if (index==1)
            return listOut;
        if (index==2)
            return listEat;
        return null;
    }

    public void SetUnChildToSort(int index_list){
        if (index_list<=2)
            GetListByIndex(index_list).Clear();
        else
            for (ListCardView list:listPhom)
                list.Clear();
    }

    void CreateInfoView(){
        String name = "ToiLaAi";
        int coin = 1000000;
        int index_avatar=this.index;
        infoView=new InfoView(name,coin,index_avatar);
    }
    void AddActor(){
        this.addActor(listOut);
        this.addActor(listEat);
        for (int i=0;i<3;i++)
            this.addActor(listPhom[i]);
        this.addActor(listHold);
        this.addActor(infoView);
    }
    public void SetPositionInfoView(){
        switch (this.index){
            case 0:
                infoView.setPosition(-Phom.V_WIDTH/2+200,-Phom.V_HEIGHT/2+100);
                break;
            case 1:
                infoView.setPosition(Phom.V_WIDTH/2-90,0);
                break;
            case 2:
                infoView.setPosition(-150,Phom.V_HEIGHT/2-90);
                break;
            case 3:
                infoView.setPosition(-Phom.V_WIDTH/2+90,0);
                break;
        }
    }
    void SetInsertPhom(PhomModel phom_model){
        if (phom_model.list_phom.size==1){
            for (CardView cardView:listPhom[0].listCardViews)
                listPhom[1].AddCardView(cardView);
//            listPhom[1].listCardViews.addAll(listPhom[0].listCardViews);
            listPhom[0].Clear();
        }
    }
    public void SortPhom(PhomModel phom_model){
        SetInsertPhom(phom_model);
        for (int i=0;i<3;i++)
        {
            ListCardView list_phom_view = listPhom[i];
            if (index==0||index==2)
                list_phom_view.SortCenter(0.5f);
            else
            if (index==1)
                list_phom_view.SortRight(0.5f);
            else
                list_phom_view.SortLeft(0.5f);
        }
    }
    public void XoeBai(){
        if (index==0)
            return;
        if (index==2)
            listHold.SortCenter(0.5f);
        else
            listHold.SortCenterVertical(0.7f);
    }
    public void Clear(){
        listHold.Clear();
        listEat.Clear();
        listOut.Clear();
        for (ListCardView list:listPhom)
            list.Clear();
        infoView.Clear();
    }
    public void UpdateGroupListCardView(){
        listHold.UpdateGroup();
        listOut.UpdateGroup();
        listEat.UpdateGroup();
        for (int i=0;i<3;i++)
            listPhom[i].UpdateGroup();
    }
}