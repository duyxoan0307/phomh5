package com.mygdx.catte.Model;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.BotController.BotBasicController;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayerModel {

    public static long money_default=100000;
    public InfoModel infoModel;
    public int index;
    GameModel gameModel;
    public Array<CardModel> list_hold;
    public Array<CardModel> list_eat;
    public Array<CardModel> list_out;
    public int so_quan_an_duoc;
    BotBasicController botController;
    public PhomModel phom;

    public CardModel cardVuaLay;
    public boolean is_sorted_list_hold;

    public PlayerModel(GameModel gameModel){
        list_hold = new Array<CardModel> ();
        list_eat = new Array<CardModel> ();
        list_out = new Array<CardModel> ();
        so_quan_an_duoc=0;
        this.phom=new PhomModel();
        this.gameModel=gameModel;
        is_sorted_list_hold=false;

        this.botController=gameModel.GetListBot().random();
    }
    public void SetBotController(BotBasicController botBasicController){
        this.botController=botBasicController;
    }
    public static int count=0;
    public void AddNewCard(CardModel cardModel){
        count++;
        list_hold.add(cardModel);
        SetUnChildToSort(0);
        for (CardModel card:list_hold
             ) {
            card.SetParent(index,0);
        }
        //SortList(0);
    }
    public void SetViewListHold(){
        SetUnChildToSort(0);
        if (index==0)
        {
            for (CardModel card:list_hold)
                card.SetParent(index,0);
            SortList(0);
        }
        else
        {
            for (CardModel card:list_hold)
                if (!list_eat.contains(card,false))
                    card.SetParent(index,0);
            SortList(0);
        }
        DataModel.Instance.SaveListHold(this);
    }
    public void SetViewListOut(){
        SetUnChildToSort(1);
        for (CardModel card:list_out)
            card.SetParent(index,1);
        SortList(1);
        DataModel.Instance.SaveListOut(this);
    }
    public void SetViewListEat(){
        DataModel.Instance.SaveListEat(this);
        if (index==0)
            return;
        SetUnChildToSort(2);
        for (CardModel card:list_eat)
            card.SetParent(index,2);
        SortList(2);
    }
    public void SetViewListPhom(){
        SetUnChildToSort(3);
        for (Array<CardModel> list:phom.list_phom)
        {
            for (CardModel card:list)
                card.SetParent(index,3+phom.list_phom.indexOf(list,false));
        }
        SortList(3);
        DataModel.Instance.SaveListPhom(this);
    }
    public void SetFlipListHold(){
        for (int i=0;i<list_hold.size;i++)
            list_hold.get(i).SetFlip(i);
    }

    //cb
    OnSetUnChildToSort cbSetUnChildToSort;

    void SetUnChildToSort(int index_list){
        if (cbSetUnChildToSort!=null)
            cbSetUnChildToSort.OnEvent(index_list);
    }
    public void RegisterSetUnChildToSort(OnSetUnChildToSort onSetUnChildToSort){
        this.cbSetUnChildToSort=onSetUnChildToSort;
    }

    public interface OnSetUnChildToSort{
        public void OnEvent(int index_list);
    }

    OnSortList onSortList;
    public void SortList(int index_list){
        if (onSortList!=null)
            onSortList.OnEvent(index_list);
    }
    public void RegisterSortList(OnSortList onSortList){
        this.onSortList=onSortList;
    }
    public interface OnSortList{
        public void OnEvent(int index_list);
    }

    OnSetInfo cbSetInfo;
    public void SetInfo(){
        if (cbSetInfo!=null)
            cbSetInfo.OnEvent();
    }
    public void RegisterSetInfo(OnSetInfo cb)
    {
        cbSetInfo=cb;
    }
    public interface OnSetInfo{
        public void OnEvent();
    }

    public void Boc(){
        //dosomething

        CardModel card = gameModel.GetRandomCardFromBoard();
        list_hold.add(card);
        if (index==0)
        {
            card.SetVisible(true);
            card.SetCanClick(true);
        }
        SetViewListHold();
        cardVuaLay=card;
        SetEndBocHoacAn();
        is_sorted_list_hold=false;
        DataModel.Instance.SaveCardVuaLay(this);

        SoundController.Instance.PlayClipPlayCard();
    }
    public void An(){
        //dosomething
        PlayerModel player_previous=gameModel.GetPreviousPlayer();
        CardModel card_to_eat = player_previous.list_out.peek();
        list_hold.add(card_to_eat);
        card_to_eat.SetShadow(1);

        if (index==0)
        {
            card_to_eat.SetCanClick(true);
        }

        list_eat.add(card_to_eat);
        so_quan_an_duoc++;
        PlayerModel player_ha=gameModel.GetPlayerHaHienTai();

        //tru tien
        if (player_previous.list_out.size == 4)
        {
            player_previous.ChangeScore(-gameModel.scoreBet * 4);
            ChangeScore(gameModel.scoreBet * 4);
        }
        else
        {
            player_previous.ChangeScore(-gameModel.scoreBet * (list_eat.size));
            ChangeScore(gameModel.scoreBet * (list_eat.size));
        }
        player_previous.list_out.removeValue(card_to_eat,false);

        if (player_ha!=player_previous)
        {
            if(player_ha.list_out.size>0){

                CardModel card_chuyen_ha = player_ha.list_out.peek();
                player_previous.list_out.add(card_chuyen_ha);
                player_ha.list_out.removeValue(card_chuyen_ha,false);
            }
        }
        gameModel.index_player_ha_dau=gameModel.ConvertTurn(gameModel.index_player_ha_dau+1);
        //update view
        player_previous.SetViewListOut();
        player_ha.SetViewListOut();

        SetViewListHold();
        SetViewListEat();
        cardVuaLay=card_to_eat;
        SetEndBocHoacAn();
        is_sorted_list_hold=false;

        DataModel.Instance.SaveIndexPlayerHaDau();

//conversation
        GameController.Instance.playerViews[index].infoView.ShowEmoticon(Assets.GetTexture(Assets.GetSmile((int) (Math.random()*2))));

        GameController.Instance.playerViews[player_previous.index].infoView.ShowEmoticon((Assets.GetTexture(Assets.GetCry((int) (Math.random()*2)))));
        DataModel.Instance.SaveCardVuaLay(this);

        SoundController.Instance.PlayClipEatCard();
    }


    public interface OnEndBocHoacAn{
        public void OnEvent();
    }
    OnEndBocHoacAn cbEndBocHoacAn;
    void SetEndBocHoacAn(){
        if (cbEndBocHoacAn!=null)
            cbEndBocHoacAn.OnEvent();
    }
    public void RegisterEndBocHoacAn(OnEndBocHoacAn cb){
        cbEndBocHoacAn=cb;
    }

    public void AIDanh(){
        PhomModel phom_toi_uu=gameModel.TimPhomToiUu(list_hold,list_eat);
        Array<CardModel> list_hop_le = new Array<CardModel>(list_hold);
        if (phom_toi_uu!=null)
        {
            for (Array<CardModel> list : phom_toi_uu.list_phom)
                gameModel.DeleteCardByList(list_hop_le,list);
        }
        CardModel card_danh;
        card_danh=botController.ChooseCardToOut(list_hop_le);
        if (card_danh==null)
        {
            card_danh=list_hop_le.random();
        }
        Danh(card_danh);
    }
    public void Danh(CardModel cardModel){
        if (index==0)
            cardModel.SetCanClick(false);
        cardModel.SetVisible(true);
        list_out.add(cardModel);
        list_hold.removeValue(cardModel,false);
        SetViewListHold();
        SetViewListOut();

        SetEndDanh();
        is_sorted_list_hold=false;


        PlayerModel next_player = gameModel.GetNextPlayer(this);

        SoundController.Instance.PlayClipPlayCard();
    }

    public interface OnEndDanh{
        public void OnEvent();
    }
    OnEndDanh cbEndDanh;
    void SetEndDanh(){
        if (cbEndDanh!=null)
            cbEndDanh.OnEvent();
    }
    public void RegisterEndDanh(OnEndDanh cb)
    {
        cbEndDanh=cb;
    }

    public void AIHaBai(){
        PhomModel phom_toi_uu=gameModel.TimPhomToiUu(list_hold,list_eat);
        HaBai(phom_toi_uu);

    }
    public void HaBai(PhomModel phom){
        //dosomething
        if (phom!=null)
        {
            for (Array<CardModel> list:phom.list_phom)
                gameModel.CheckAddPhom(this.phom,list);

            for (Array<CardModel> list:phom.list_phom)
            {
                gameModel.DeleteCardByList(list_hold,list);
                gameModel.DeleteCardByList(list_eat,list);
                for(CardModel card:list)
                    card.SetVisible(true);
            }
            if (index==0)
                for (CardModel card:phom.GetAllCards())
                    card.SetCanClick(false);

            SoundController.Instance.PlayClipDownCards();
        }

        SetViewListHold();
        SetViewListOut();
        SetViewListEat();
        SetViewListPhom();

        SetEndHaBai();
        is_sorted_list_hold=false;
    }

    public interface OnEndHaBai{
        public void OnEvent();
    }
    OnEndHaBai cbEndHaBai;
    public void SetEndHaBai(){
        if (cbEndHaBai!=null)
            cbEndHaBai.OnEvent();
    }
    public void RegisterEndHaBai(OnEndHaBai cb)
    {
        cbEndHaBai=cb;
    }

    public void AIGuiBai(Array<CardModel> list){
        //dosomething
        int first_count=list_hold.size;
        int i=0;
        while (i<list.size)
        {
            CardModel card = list.get(i);
            boolean gui_duoc = GuiBai(card);
            if (gui_duoc)
            {
                list.removeValue(card,false);
                i=0;
            }
            else
                i++;
        }
        SetEndGuiBai();

        if (list_hold.size<first_count)
        {
            SoundController.Instance.PlayClipPlayCard();
        }
    }

    public boolean GuiBai(CardModel cardModel){
        Array<CardModel> arrTmp;
        for (PlayerModel player:gameModel.listPlayer)
        {
            for (Array<CardModel> phom:player.phom.list_phom)
            {
                arrTmp=new Array<CardModel>(phom);
                arrTmp.add(cardModel);
                if (gameModel.CheckPhomCungSo(arrTmp)||gameModel.CheckPhom(arrTmp))
                {
                    phom.add(cardModel);
                    GameModel.SortCardInList(phom,false);
                    this.list_hold.removeValue(cardModel,false);
                    cardModel.SetVisible(true);
                    //update view
                    if (index==0)
                    {
                        cardModel.SetCanClick(false);
                        GameController.currentChooseCard=null;
                    }
                    player.SetViewListPhom();
                    this.SetViewListHold();
                    is_sorted_list_hold=false;
                    return true;
                }
            }
        }
        return false;
    }

    public interface OnEndGuiBai{
        public void OnEvent();
    }
    OnEndGuiBai cbEndGuiBai;
    public void SetEndGuiBai(){
        if (cbEndGuiBai!=null)
            cbEndGuiBai.OnEvent();
    }
    public void RegisterEndGuiBai(OnEndGuiBai cb){
        cbEndGuiBai=cb;
    }

    public void Clear(){
        list_hold.clear();
        list_eat.clear();
        so_quan_an_duoc=0;
        list_out.clear();
        this.phom.Clear();
        cardVuaLay=null;
        is_sorted_list_hold=false;
        if (infoModel!=null)
           infoModel.Clear();
        this.botController=gameModel.GetListBot().random();
    }
    public void ChangeScoreNotHaveInfo(int score){
        Preferences preferences=PreferenceController.Instance.preferences;
        long coin = Long.parseLong(preferences.getString("HighScore"));
        coin+=score;
        preferences.putString("HighScore",coin+"");
        preferences.flush();
        Phom.zenSDK.FBInstant_IncrementDoubleStats("money", score, new IZen.FBInstant_IncrementDoubleStatsCallback() {
            @Override
            public void OnSuccess(double newvalue) {
                if (newvalue<50000) {
                    newvalue = 50000;
                    Phom.zenSDK.FBInstant_SetDoubleStats("money",newvalue, null);
                }

                Phom.zenSDK.ReportScore("", (long) newvalue);
                PreferenceController.Instance.preferences.putString("HighScore", (long)newvalue+"");
                PreferenceController.Instance.preferences.flush();
            }
        });
    }
    public void ChangeScore(int score)
    {
        infoModel.coin+=score;
        if (index==0 && infoModel.coin<50000)
            infoModel.coin=50000;
        Preferences preferences=PreferenceController.Instance.preferences;
        if (index==0) {
            preferences.putString("HighScore", infoModel.coin+"");

            Phom.zenSDK.FBInstant_IncrementDoubleStats("money", score, new IZen.FBInstant_IncrementDoubleStatsCallback() {
                @Override
                public void OnSuccess(double newvalue) {
                    infoModel.coin = (long)newvalue;
                    if (infoModel.coin<50000) {
                        infoModel.coin = 50000;
                        Phom.zenSDK.FBInstant_SetDoubleStats("money", infoModel.coin, null);
                    }

                    Phom.zenSDK.ReportScore("", infoModel.coin);
                    PreferenceController.Instance.preferences.putString("HighScore", (infoModel.coin)+"");
                    PreferenceController.Instance.preferences.flush();
                }
            });
        }
        else
            preferences.putString("ScoreOfPlayer"+infoModel.index,String.valueOf(infoModel.coin));
        preferences.flush();

        SetInfo();
//        Gdx.app.log("Player "+index,score+"");
        if (cbChangeScore!=null)
            cbChangeScore.OnEvent(score);
    }

    public interface OnChangeScore{
        public void OnEvent(int score);
    }
    OnChangeScore cbChangeScore;
    public void RegisterChangeScore(OnChangeScore cb)
    {
        cbChangeScore=cb;
    }
}
