package com.mygdx.catte.Model;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.CharArray;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.Utilities;
import com.mygdx.catte.View.CardView;

/**
 * Created by DUY on 7/18/2018.
 */

public class DataModel {
    public static DataModel Instance;

    public DataModel(){
        Instance=this;
    }
    public void SaveListHold(PlayerModel player){
        Utilities.SaveListCardToPreference(player.list_hold,"ListHold"+player.index);
    }
    public void SaveListOut(PlayerModel player){
        Utilities.SaveListCardToPreference(player.list_out,"ListOut"+player.index);
    }
    public void SaveListEat(PlayerModel player){
        Utilities.SaveListCardToPreference(player.list_eat,"ListEat"+player.index);
    }
    public void SaveCardVuaLay(PlayerModel player){
        Utilities.SaveCardToPreference(player.cardVuaLay,"CardVuaLay"+player.index);
    }
    public void SaveListPhom(PlayerModel player){
        PreferenceController.Instance.preferences.putInteger("CountPhom"+player.index,player.phom.list_phom.size);
        PreferenceController.Instance.preferences.flush();
        for (int i=0;i<player.phom.list_phom.size;i++)
        {
            Utilities.SaveListCardToPreference(player.phom.list_phom.get(i),"ListPhom"+player.index+i);
        }
    }
    public void SaveCanDownCards(){
        PreferenceController.Instance.preferences.putBoolean("CanDownCards", CardView.canDownCards);
        PreferenceController.Instance.preferences.flush();
    }

    public void SaveCurrentTurn(){
        Preferences preferences=PreferenceController.Instance.preferences;
        preferences.putInteger("CurrentTurn",GameModel.Instance.currentTurn);
        preferences.flush();
    }
    public void SaveGameState(){
        Preferences preferences=PreferenceController.Instance.preferences;
        preferences.putInteger("GameState",GameModel.Instance.gameState.ordinal());
        preferences.flush();
    }
    public void SaveIndexPlayerHaDau(){
        Preferences preferences=PreferenceController.Instance.preferences;
        preferences.putInteger("IndexPlayerHaDau",GameModel.Instance.index_player_ha_dau);
        preferences.flush();
    }
    public void SaveListCardOnTable()
    {
        Utilities.SaveListCardToPreference(GameModel.Instance.listCardOnTable,"ListCardOnTable");
    }
    public void SaveGameModel(){
        SaveCurrentTurn();
        SaveGameState();
        SaveIndexPlayerHaDau();
        for (PlayerModel player:GameModel.Instance.listPlayer)
        {
            SaveListHold(player);
            SaveListOut(player);
            SaveListEat(player);
            SaveListPhom(player);
        }
    }
    public void SaveTurnState(){
        Preferences preferences=PreferenceController.Instance.preferences;
        preferences.putInteger("TurnState",GameModel.Instance.turnState.ordinal());
        preferences.flush();
    }
    public static void Reset(){
        ResetData();
    }
    public boolean IsResume(){
        if (!PreferenceController.Instance.preferences.contains("GameState"))
            return false;
        return PreferenceController.Instance.preferences.getInteger("GameState")!= GameModel.GameState.OVER.ordinal();
    }
    public void LoadData(){
        Preferences preferences=PreferenceController.Instance.preferences;
        GameModel gameModel=GameModel.Instance;
        gameModel.currentTurn=preferences.getInteger("CurrentTurn");
        gameModel.gameState=GameModel.GameState.values()[preferences.getInteger("GameState")];
        gameModel.index_player_ha_dau=preferences.getInteger("IndexPlayerHaDau");
        gameModel.turnState=GameModel.TurnState.values()[preferences.getInteger("TurnState")];
        gameModel.isPlaying=true;
        for (int i=0;i<4;i++)
        {
            PlayerModel player=gameModel.listPlayer.get(i);
            Utilities.ImportDataCards(preferences.getString("ListHold"+i),player.list_hold);
            Utilities.ImportDataCards(preferences.getString("ListOut"+i),player.list_out);
            Utilities.ImportDataCards(preferences.getString("ListEat"+i),player.list_eat);
            player.so_quan_an_duoc=player.list_eat.size;
            Utilities.ImportDataCardModel(preferences.getString("CardVuaLay"+player.index),player.cardVuaLay);
            int count_phom = preferences.getInteger("CountPhom"+player.index);
            for (int j=0;j<count_phom;j++)
            {
                Array<CardModel> list = new Array<CardModel>();
                Utilities.ImportDataCards(preferences.getString("ListPhom"+player.index+""+j),list);
                player.phom.AddPhom(list);
            }
        }
        for (int i=0;i<4;i++)
        {
            PlayerModel player=gameModel.listPlayer.get(i);
            gameModel.listCardOnTable.removeAll(player.list_hold,false);
            gameModel.listCardOnTable.removeAll(player.list_eat,false);
            gameModel.listCardOnTable.removeAll(player.list_out,false);
            for (Array<CardModel> arr:player.phom.list_phom)
                gameModel.listCardOnTable.removeAll(arr,false);
        }
        for (int i=0;i<4;i++)
        {
            PlayerModel player=gameModel.listPlayer.get(i);
            player.SetViewListHold();
            player.SetViewListOut();
            player.SetViewListEat();
            player.SetViewListPhom();
        }

        for (CardModel card:gameModel.listPlayer.get(0).list_hold){
            card.SetVisible(true);
            card.SetCanClick(true);
        }
        for (PlayerModel player:gameModel.listPlayer)
        {
            for (CardModel card:player.list_out)
                card.SetVisible(true);
            for (CardModel card:player.list_eat)
            {
                card.SetVisible(true);
                card.SetShadow(1);
            }
            for (Array<CardModel> arr:player.phom.list_phom)
                for (CardModel card:arr)
                    card.SetVisible(true);
        }
        CardView.canDownCards=preferences.getBoolean("CanDownCards");
    }
        static void ResetData(){
        Preferences preferences=PreferenceController.Instance.preferences;
        GameModel gameModel=GameModel.Instance;
        if (preferences.contains("CurrentTurn"))
           preferences.remove("CurrentTurn");
        if (preferences.contains("GameState"))
            preferences.remove("GameState");
        if (preferences.contains("IndexPlayerHaDau"))
            preferences.remove("IndexPlayerHaDau");
        if (preferences.contains("TurnState"))
            preferences.remove("TurnState");
        if (gameModel!=null)
            gameModel.isPlaying=true;
        for (int i=0;i<4;i++)
        {
            if (preferences.contains("ListHold"+i))
                preferences.remove("ListHold"+i);
            if (preferences.contains("ListOut"+i))
                preferences.remove("ListOut"+i);
            if (preferences.contains(("ListEat"+i)))
                preferences.remove("ListEat"+i);
            if (preferences.contains(("CardVuaLay"+i)))
                preferences.remove("CardVuaLay"+i);
            if (preferences.contains("CountPhom"+i))
                preferences.remove("CountPhom"+i);
            for (int j=0;j<3;j++)
            {
                if (preferences.contains("ListPhom"+i+""+j))
                    preferences.remove("ListPhom"+i+""+j);
            }
        }
        if (preferences.contains("CanDownCards"))
            preferences.remove("CanDownCards");
    }
}
