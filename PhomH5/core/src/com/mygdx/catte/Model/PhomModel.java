package com.mygdx.catte.Model;

import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.Phom;

/**
 * Created by DUY on 7/12/2018.
 */


public class PhomModel
{
    public Array<Array<CardModel>> list_phom;
    public PhomModel()
    {
        list_phom = new Array<Array<CardModel>>();
    }
    public void AddPhom(Array<CardModel> list)
    {
        if (list.size <3)
            return;
        list_phom.add(list);
    }
    public void Clear()
    {
        list_phom.clear();
    }
    public static boolean CheckTwoPhomSame(PhomModel phom1, PhomModel phom2)
    {
        if (phom1.list_phom.size != phom2.list_phom.size)
            return false;
        int dem = 0;
        for (Array<CardModel> list1 : phom1.list_phom)
        {
            for (Array<CardModel> list2 : phom2.list_phom)
            {
                if (CheckTwoListSame(list1, list2))
                    dem++;
            }
        }
        if (dem == phom1.list_phom.size)
            return true;
        return false;
    }
    public int GetTongDiem() {
        int sum = 0;
        for (Array<CardModel> list : list_phom)
        {
            for (CardModel card : list)
                sum += card.number;
        }
        return sum;
    }
    public int GetTongBai() {
        int sum = 0;
        for (Array<CardModel> list : list_phom)
            for (CardModel card : list)
                sum++;
        return sum;
    }
    static boolean CheckTwoListSame(Array<CardModel> list_1, Array<CardModel> list_2)
    {
        if (list_1.size != list_2.size)
            return false;
        for (CardModel card : list_1)
            if (!list_2.contains(card,false))
                return false;
        return true;
    }
    public Array<CardModel> GetAllCards(){
        Array<CardModel> list_card = new Array<CardModel>();
        for (Array<CardModel> arr:list_phom)
            list_card.addAll(arr);
        return list_card;
    }
    public void PrintPhom(){
        for (Array<CardModel> arr:list_phom)
            GameModel.PrintListCard(arr);
    }
}