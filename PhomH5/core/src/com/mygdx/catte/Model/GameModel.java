package com.mygdx.catte.Model;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.Controller.BotController.Bot10Controller;
import com.mygdx.catte.Controller.BotController.Bot1Controller;
import com.mygdx.catte.Controller.BotController.Bot2Controller;
import com.mygdx.catte.Controller.BotController.Bot3Controller;
import com.mygdx.catte.Controller.BotController.Bot4Controller;
import com.mygdx.catte.Controller.BotController.Bot5Controller;
import com.mygdx.catte.Controller.BotController.Bot6Controller;
import com.mygdx.catte.Controller.BotController.Bot7Controller;
import com.mygdx.catte.Controller.BotController.Bot8Controller;
import com.mygdx.catte.Controller.BotController.Bot9Controller;
import com.mygdx.catte.Controller.BotController.BotBasicController;
import com.mygdx.catte.Controller.BotController.BotNgu2Controller;
import com.mygdx.catte.Controller.BotController.BotNgu3Controller;
import com.mygdx.catte.Controller.BotController.BotNguController;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.MyLinkedMap;
import com.mygdx.catte.Utilities.Utilities;

import java.util.Comparator;
import java.util.Map;


/**
 * Created by DUY on 6/12/2018.
 */

public class GameModel {
    public static GameModel Instance;
    public  BoardModel boardModel;
    public  Array<PlayerModel> listPlayer;
    public  Array<CardModel> listCardOnTable;
    public int currentTurn;
    public boolean isPlaying=false;
    public enum GameState
    {
        START,
        PLAYING,
        PAUSED,
        OVER
    };
    public enum TurnState{BOCHOACAN,DANH,HA,GUI};
    public TurnState turnState;
    public GameState gameState;
    public int scoreBet;
    public int index_player_ha_dau;
    public int host;

    public GameModel(int score_bet,int number_player,boolean is_resume){
        Instance = this;
        boardModel = new BoardModel();
        listPlayer = new Array<PlayerModel>();

        for (int i = 0; i < 4; i++) {
            PlayerModel player_model = new PlayerModel(this);
            BotBasicController bot;
            listPlayer.add(player_model);
            player_model.index=i;
        }
        listCardOnTable = new Array<CardModel>();
        listCardOnTable.addAll(boardModel.listCard);
        listCardOnTable.shuffle();

        scoreBet = 2;
        gameState = GameState.OVER;
        host = PreferenceController.Instance.preferences.getInteger("Host");
        index_player_ha_dau=host;
        turnState=null;

        //test
        //TestToHop();
        a=new int[11];
    }

    public void DistributeCardsTest(){
 //       host=1;
        isPlaying=true;
        int count=listCardOnTable.size;
        int count_loop=-1;

        final Array<CardModel> arrCardTest= new Array<CardModel>();

//        arrCardTest.clear();
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(1, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(2, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(3, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(7, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(5, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(6, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(1, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(9, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(2, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(11, CardModel.CardType.CO))));


        listCardOnTable.removeAll(arrCardTest,false);
        float time_delay=0.01f;
        while (count>(52 - (listPlayer.size*9+1))) {
            for (int i = 0; i < listPlayer.size; i++)
            {
                if (count<=(52 - (listPlayer.size*9+1)))
                    break;
                count_loop++;
                final int finalI = i;
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (isPlaying)
                        {
                            CardModel card_model_random;
                            PlayerModel player = listPlayer.get((host+ finalI)%(listPlayer.size));
                            if (player.index==0 && arrCardTest.size>player.list_hold.size)
                            {
                                card_model_random=arrCardTest.get(player.list_hold.size);
                            }
                            else
                            {
                                card_model_random = GetRandomCardFromBoard();
                            }
                            player.AddNewCard (card_model_random);
                            card_model_random.SetFirstDistribute();
                        }
                    }
                },time_delay*(count_loop));
                count--;
            }
        }
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (isPlaying) {
                    SetEndDistributeCards();
                }
            }
        },time_delay*(count_loop)+1);

    }
    CardModel GetRandomCardFromBoard()
    {
        if (listCardOnTable.size == 0)
            return null;
        CardModel card_random = listCardOnTable.peek();
        listCardOnTable.removeValue(card_random,false);
        UIController.Instance.playingUI.SetTextSoBai();
        return card_random;
    }
    int ConvertTurn(int turn)
    {
        if (turn >= 4)
            return turn % 4;
        if (turn < 0)
            return turn + 4;
        return turn;
    }
    PlayerModel GetPlayerHaHienTai(){
        return listPlayer.get(index_player_ha_dau);
    }
    public PlayerModel GetCurrentPlayer()
    {
        return listPlayer.get(currentTurn);
    }
    public PlayerModel GetPreviousPlayer(){return listPlayer.get(ConvertTurn(currentTurn - 1));}
    public PlayerModel GetNextPlayer(){return listPlayer.get(ConvertTurn(currentTurn + 1));}
    public PlayerModel GetPreviousPlayer(PlayerModel player){return listPlayer.get(ConvertTurn(player.index - 1));}
    public PlayerModel GetNextPlayer(PlayerModel player){return listPlayer.get(ConvertTurn(player.index + 1));}
    public PlayerModel GetMainPlayer()
    {
        return listPlayer.get(0);
    }
    public static void SwapTwoCardsInList(Array<CardModel> list_card_model,CardModel card_model_1, CardModel card_model_2){
        CardModel card_model_temp = card_model_1;
        int index_card_1 = list_card_model.indexOf(card_model_1,false);
        int index_card_2 = list_card_model.indexOf(card_model_2,false);
        list_card_model.set(index_card_1,card_model_2);
        list_card_model.set(index_card_2,card_model_temp);
    }

    public static void SortCardInList(Array<CardModel> list_card_model,boolean isDESC){
        if (list_card_model == null)
            return;
        if (list_card_model.size == 0)
            return;
        for (int i = 0; i < list_card_model.size - 1; i++) {
            for (int j = i + 1; j < list_card_model.size; j++) {
                CardModel card_model_1 = list_card_model.get(i);
                CardModel card_model_2 = list_card_model.get(j);
                if (isDESC == true) {
                    if (CardModel.CompareCards(card_model_1, card_model_2) < 0)
                        SwapTwoCardsInList(list_card_model, card_model_1, card_model_2);

                } else if (isDESC == false) {
                    if (CardModel.CompareCards(card_model_1, card_model_2) > 0)
                        SwapTwoCardsInList(list_card_model, card_model_1, card_model_2);

                }
            }
        }
    }

    OnEndDistributeCards cbEndDistributeCards;
    void SetEndDistributeCards(){
        if (cbEndDistributeCards!=null)
            cbEndDistributeCards.OnEnvent();
    }
    public void RegisterEndDistributeCards(OnEndDistributeCards cb){
        this.cbEndDistributeCards=cb;
    }
    public interface OnEndDistributeCards{
        public void OnEnvent();
    }

    void DeleteCardByList(Array<CardModel> list_to_delete, Array<CardModel> list_card)
    {
        if (list_to_delete == null || list_card == null)
            return;
        for (int i = 0; i < list_to_delete.size; i++) {
            CardModel card_model = list_to_delete.get(i);
            if (list_card.contains(card_model,false) && list_to_delete.contains(card_model,false)) {
                list_to_delete.removeValue(card_model,false);
                i--;
            }
        }
    }
    public boolean CheckCanEat(Array<CardModel> list_hold, Array<CardModel> list_eated) {
        Array<PhomModel> list_phom = TimTatCaListPhom(list_hold);
        LocPhomHopLeTuListEat(list_phom, list_eated);
        if (list_phom.size > 0)
            return true;
        return
                false;
    }
    public PhomModel PhomHopLe(Array<CardModel> list_hold, Array<CardModel> list_eated){
        Array<PhomModel> list_phom = TimTatCaListPhom(list_hold);
        LocPhomHopLeTuListEat(list_phom, list_eated);
        if (list_phom.size <= 0)
            return null;
        if (list_phom.size == 1)
            return list_phom.get(0);
        PhomModel phom_tmp = list_phom.get(0);
        for (int i = 1; i < list_phom.size; i++)
        {
            PhomModel phom = list_phom.get(i);
            if (phom.GetTongDiem() > phom_tmp.GetTongDiem())
                phom_tmp = phom;
        }
        return phom_tmp;
    }

    Array<PhomModel> TimTatCaListPhom(Array<CardModel> list_kiem_tra)
    {
//        if (turnState!=null)
//            Gdx.app.log("So Lan Player " + GetCurrentPlayer().index + " tim khi " + turnState.toString(),"tim");
        if (list_kiem_tra.size == 3)
            return TimTatCaListPhomTrong3La(list_kiem_tra);
        if (list_kiem_tra.size == 4)
            return TimTatCaListPhomTrong4La(list_kiem_tra);
        if (list_kiem_tra.size == 5)
            return TimTatCaListPhomTrong5La(list_kiem_tra);
        if (list_kiem_tra.size == 6)
            return TimTatCaListPhomTrong6La(list_kiem_tra);
        if (list_kiem_tra.size == 7)
            return TimTatCaListPhomTrong7La(list_kiem_tra);
        if (list_kiem_tra.size == 8)
            return TimTatCaListPhomTrong8La(list_kiem_tra);
        if (list_kiem_tra.size == 9)
            return TimTatCaListPhomTrong9La(list_kiem_tra);
        if (list_kiem_tra.size == 10)
            return TimTatCaListPhomTrong10La(list_kiem_tra);
        return new Array<PhomModel>();
    }
    void LocPhomHopLeTuListEat(Array<PhomModel> list_phom, Array<CardModel> list_eat)
    {
        for (int i = 0; i < list_phom.size; i++)
        {
            if (!CheckPhomHopLeTuListEat(list_phom.get(i), list_eat))
            {
                list_phom.removeIndex(i);
                i--;
            }
        }
    }
    Array<PhomModel> ListPhom3TuListPhom2(Array<CardModel> listOriginal, Array<PhomModel> list_phom_2){
        Array<CardModel> list_original;
        Array<PhomModel> list_phom_3 = new Array<PhomModel>();
        for(PhomModel phom:list_phom_2)
        {
            if (phom.list_phom.size<2)
                continue;
            list_original=new Array<CardModel>(listOriginal);
            DeleteCardByList(list_original,phom.GetAllCards());
            Array<PhomModel> list_phom_con_lai = TimTatCaListPhom(list_original);
            for(PhomModel phom_model:list_phom_con_lai)
            {
                for (Array<CardModel> arr:phom.list_phom)
                    phom_model.AddPhom(arr);
            }
            list_phom_3.addAll(list_phom_con_lai);
        }
        return list_phom_3;
    }
    boolean CheckPhomHopLeTuListEat(PhomModel phom, Array<CardModel> list_eat)
    {
        if (phom.list_phom.size < list_eat.size)
            return false;
        Array<Array<CardModel>> list_list_checked = new Array<Array<CardModel>>();
        Array<CardModel> list_eat_checked = new Array<CardModel>();
        for (int i = 0; i < phom.list_phom.size; i++)
        {
            Array<CardModel> list_card = phom.list_phom.get(i);
            if (list_list_checked.contains(list_card,false))
                continue;
            for (CardModel card : list_eat)
            {
                if (list_eat_checked.contains(card,false))
                    continue;
                if (list_list_checked.contains(list_card,false))
                    continue;
                if (list_card.contains(card,false))
                {
                    list_list_checked.add(list_card);
                    list_eat_checked.add(card);
                }
            }
        }
        if (list_eat_checked.size == list_eat.size)
            return true;
        return false;
    }
    void CheckAddPhom(PhomModel phom, Array<CardModel> list)
    {
        if (CheckPhomCungSo(list) || CheckPhomSanh(list))
            phom.AddPhom(list);
    }
    boolean CheckPhomCungSo(Array<CardModel> list)
    {
        if (list.size != 3 && list.size != 4)
            return false;
        for (int i = 1; i < list.size; i++)
            if (list.get(i).number != list.get(0).number)
                return false;
        return true;
    }
    boolean CheckPhomSanh(Array<CardModel> list)
    {

        Array<CardModel> list_tmp = new Array<CardModel>();
        list_tmp.addAll(list);
        SortCardInList(list_tmp, false);
        for (int i = 0; i < list_tmp.size - 1; i++)
            if (list_tmp.get(i).number + 1 != list_tmp.get(i + 1).number || list_tmp.get(i).type != list_tmp.get(i + 1).type)
                return false;
        return true;
    }
    public boolean CheckPhom(Array<CardModel> list){
        return CheckPhomCungSo(list)||CheckPhomSanh(list);
    }
    boolean AddNewPhom(Array<PhomModel> list_phom, PhomModel phom,int min_phom)
    {
        if (phom.list_phom.size == 0)
            return false;
        if (CheckListPhomContainPhom(list_phom, phom))
            return false;
        if (phom.list_phom.size < min_phom)
            return false;
        PhomModel phom_new = new PhomModel();
        for (Array<CardModel> list : phom.list_phom)
        {
            Array<CardModel> _phom = new Array<CardModel>();
            _phom.addAll(list);
            SortCardInList(_phom, false);
            phom_new.AddPhom(_phom);
        }
        list_phom.add(phom_new);
        return true;
    }
    boolean CheckListPhomContainPhom(Array<PhomModel> list_phom, PhomModel phom)
    {
        for (PhomModel phom_tmp : list_phom)
        {
            if (PhomModel.CheckTwoPhomSame(phom, phom_tmp))
                return true;
        }
        return false;
    }
    Array<CardModel> SortPhom3(Array<CardModel> list_card_model)
    {
        Array<CardModel> list_sorted = new Array<CardModel>();

        SortCardInList(list_card_model, true);

        if (list_card_model == null)
            return new Array<CardModel>();
        if (list_card_model.size == 0)
            return new Array<CardModel>();

        MyLinkedMap<Integer, Integer> list_count_each_number = new MyLinkedMap<Integer, Integer>();
        list_count_each_number.put(list_card_model.get(0).number, 1);
        int index = list_card_model.get(0).number;
        for (int i = 1; i < list_card_model.size; i++) {

            if (list_card_model.get(i).number != index) {
                list_count_each_number.put(list_card_model.get(i).number, 1);
                index = list_card_model.get(i).number;
            } else
            {
                list_count_each_number.put(index,list_count_each_number.get(index)+1);
            }
        }
        for (int i = 0; i < list_count_each_number.size(); i++) {
            Map.Entry item = list_count_each_number.getEntry(i);
            if ((Integer)item.getValue() < 3) {
                list_count_each_number.remove(item.getKey());
                i--;
            }
        }

        while (list_count_each_number.size() > 0)
        {
            PushToHeadByNumber(list_card_model, list_count_each_number.getEntry(0).getKey(), list_sorted);
            list_count_each_number.remove(list_count_each_number.getEntry(0).getKey());
        }
        return list_sorted;
    }
    void PushToHeadByNumber(Array<CardModel> list_card_model, int number, Array<CardModel> list_sorted)
    {
        for (int i = 0; i < list_card_model.size; i++) {
            CardModel card_model = list_card_model.get(i);
            if (list_sorted.contains(card_model,false))
                continue;
            if (card_model.number == number) {
                list_sorted.add(card_model);
            }
        }
    }
    Array<CardModel> SortPhomSanh(Array<CardModel> list_card_model)
    {
        SortCardInList(list_card_model, true);

        Array<CardModel> list_sorted = new Array<CardModel>();
        for (int i = 0; i < list_card_model.size; i++) {
            CardModel card_model = list_card_model.get(i);
            if (list_sorted.contains(card_model,false))
                continue;
            Array<CardModel> list_sanh = CountSanhByCard(list_card_model, card_model, list_sorted);
            if (list_sanh.size >= 3) {
                SortCardInList(list_sanh, false);
                list_sorted.addAll(list_sanh);
            }
        }
        return list_sorted;
    }
    Array<CardModel> CountSanhByCard(Array<CardModel> list_card_model, CardModel card_model, Array<CardModel> list_sorted)
    {
        Array<CardModel> list_sanh = new Array<CardModel>();
        list_sanh.add(card_model);

        CardModel card_temp = card_model;

        for (int i = 0; i < list_card_model.size; i++) {
            if (card_model == list_card_model.get(i))
                continue;
            if (list_card_model.get(i).number == card_temp.number)
                continue;
            if (CardModel.CompareCards(list_card_model.get(i), card_temp) > 0)
                continue;
            if (list_sorted.contains(list_card_model.get(i),false))
                continue;
            if (card_temp.number - list_card_model.get(i).number != 1)
                return list_sanh;
            if (card_temp.number - list_card_model.get(i).number == 1 && CardModel.IsSameType(list_card_model.get(i), card_temp)) {
                list_sanh.add(list_card_model.get(i));
                card_temp = list_card_model.get(i);
            }
        }
        return list_sanh;
    }

    public void SortPhomOfPlayer(PlayerModel player)
    {
        //Array<Array<CardModel>> list_result = GetListPhomAllFromListEat (player.list_hold,player.list_eat);
        PhomModel phom_toi_uu = TimPhomToiUu(player.list_hold, player.list_eat);
        Array<CardModel> list = new Array<CardModel>();
        //list.addAll (list_result[0]);
        //list.addAll (list_result [1]);

        if (phom_toi_uu != null)
        {
            for (Array<CardModel> phom : phom_toi_uu.list_phom)
                list.addAll(phom);
        }

        Array<CardModel> list_tmp = new Array<CardModel>();
        list_tmp.addAll(player.list_hold);
        DeleteCardByList(list_tmp, list);


        Array<CardModel> list_sort_ca_3 = SortCa3(list_tmp);
        SortCardInList(list_sort_ca_3, false);
        DeleteCardByList(list_tmp, list_sort_ca_3);

        Array<CardModel> list_sort_ca_sanh = SortCaSanh(list_tmp);
        //SortCardInList(list_sort_ca_sanh, false);
        DeleteCardByList(list_tmp, list_sort_ca_sanh);

        if (list_sort_ca_3.size != 0)
            list.addAll(list_sort_ca_3);
        if (list_sort_ca_sanh.size != 0)
            list.addAll(list_sort_ca_sanh);
        if (list_tmp.size != 0)
        {
            SortCardInList(list_tmp,false);
            list.addAll(list_tmp);
        }
        player.list_hold.clear();
        player.list_hold.addAll(list);
    }
    public PhomModel TimPhomToiUu(Array<CardModel> list_hold, Array<CardModel> list_eated)
    {
        Array<PhomModel> list_phom = TimTatCaListPhom(list_hold);
        LocPhomHopLeTuListEat(list_phom, list_eated);
        if (list_phom.size == 0)
            return null;
        if (list_phom.size == 1)
            return list_phom.get(0);
        PhomModel phom_tmp = list_phom.get(0);
        for (int i = 1; i < list_phom.size; i++)
        {
            PhomModel phom = list_phom.get(i);
            if (phom.GetTongDiem() > phom_tmp.GetTongDiem())
                phom_tmp = phom;
        }
        return phom_tmp;
    }
    public PhomModel TimPhomNhieuLa(Array<CardModel> list_hold, Array<CardModel> list_eated)
    {
        Array<PhomModel> list_phom = TimTatCaListPhom(list_hold);
        LocPhomHopLeTuListEat(list_phom, list_eated);
        if (list_phom.size == 0)
            return null;
        if (list_phom.size == 1)
            return list_phom.get(0);
        PhomModel phom_tmp = list_phom.get(0);
        for (int i = 1; i < list_phom.size; i++)
        {
            PhomModel phom = list_phom.get(i);
            if (phom.GetTongBai() > phom_tmp.GetTongBai())
                phom_tmp = phom;
        }
        return phom_tmp;
    }
    Array<CardModel> SortCa3(Array<CardModel> list_card_model)
    {
        Array<CardModel> list_sorted = new Array<CardModel>();

        SortCardInList(list_card_model, true);
        //		Debug.Log (list_card_model_2.size);
        MyLinkedMap<Integer,Integer> list_count_each_number = new MyLinkedMap<Integer,Integer>();
        if (list_card_model == null)
            return new Array<CardModel>();
        if (list_card_model.size == 0)
            return new Array<CardModel>();

        list_count_each_number.put(list_card_model.get(0).number, 1);
        int index = list_card_model.get(0).number;
        for (int i = 1; i < list_card_model.size; i++) {
            if (list_sorted.contains(list_card_model.get(i),false))
                continue;
            if (list_card_model.get(i).number != index) {
                list_count_each_number.put(list_card_model.get(i).number, 1);
                index = list_card_model.get(i).number;
            } else
                list_count_each_number.put(index,list_count_each_number.get(index)+1);
        }
        for (int i = 0; i < list_count_each_number.size(); i++) {
            Map.Entry item = list_count_each_number.getEntry(i);
            if ((Integer)item.getValue() < 2) {
                list_count_each_number.remove(item.getKey());
                i--;
            }
        }

        while (list_count_each_number.size() > 0)
        {
            //			Debug.Log (list_count_each_number.size);
            PushToHeadByNumber(list_card_model, list_count_each_number.getEntry(0).getKey(), list_sorted);
            list_count_each_number.remove(list_count_each_number.getEntry(0).getKey());
        }
        return list_sorted;
    }
    Array<CardModel> SortCaSanh(Array<CardModel> list_card_model)
    {
        SortCardInList(list_card_model, true);

        Array<CardModel> list_sorted = new Array<CardModel>();
        for (int i = 0; i < list_card_model.size; i++) {
            CardModel card_model = list_card_model.get(i);
            if (list_sorted.contains(card_model,false))
                continue;
            Array<CardModel> list_ca = CountCaSanhByCard(list_card_model, card_model, list_sorted);
            if (list_ca.size >= 2) {
                SortCardInList(list_ca, false);
                list_sorted.addAll(list_ca);
            }
        }
        return list_sorted;
    }
    Array<CardModel> CountCaSanhByCard(Array<CardModel> list_card_model, CardModel card_model, Array<CardModel> list_sorted)
    {
        Array<CardModel> list_ca = new Array<CardModel>();
        list_ca.add(card_model);

        CardModel card_temp = card_model;

        for (int i = 0; i < list_card_model.size; i++) {
            if (card_model == list_card_model.get(i))
                continue;
            if (list_card_model.get(i).number == card_temp.number)
                continue;
            if (list_sorted.contains(list_card_model.get(i),false))
                continue;
            if (card_temp.number - list_card_model.get(i).number > 2)
                return list_ca;
            if ((card_temp.number - list_card_model.get(i).number == 2 || card_temp.number - list_card_model.get(i).number == 1) && CardModel.IsSameType(list_card_model.get(i), card_temp)) {
                list_ca.add(list_card_model.get(i));
                card_temp = list_card_model.get(i);
            }
        }
        return list_ca;
    }

    int[] a;
    public void TestToHop(){
        a=new int[11];
        Array<CardModel> listOriginal = new Array<CardModel>();
        listOriginal.add(new CardModel(10, CardModel.CardType.CHUON));
        listOriginal.add(new CardModel(10, CardModel.CardType.BICH));
        listOriginal.add(new CardModel(10, CardModel.CardType.RO));
        listOriginal.add(new CardModel(10, CardModel.CardType.CO));
        listOriginal.add(new CardModel(7, CardModel.CardType.CHUON));
        listOriginal.add(new CardModel(8, CardModel.CardType.CHUON));
        listOriginal.add(new CardModel(9, CardModel.CardType.CHUON));
        listOriginal.add(new CardModel(11, CardModel.CardType.CHUON));
       // listOriginal.add(new CardModel(4, CardModel.CardType.RO));

        Array<CardModel> listeat = new Array<CardModel>();
        listeat.add(listOriginal.get(2));
        listeat.add(listOriginal.get(4));

        PhomModel phom_toi_uu = TimPhomToiUu(listOriginal,new Array<CardModel>());
        for (Array<CardModel> arr:phom_toi_uu.list_phom)
            PrintListCard(arr);

        System.out.println(CheckCanEat(listOriginal,listeat));
    }
    Array<Array<CardModel>> toHopNChapK(Array<CardModel> listOriginal, int k){
        //init arrayOriginal
        int n =listOriginal.size;
        Array<CardModel> arrCachXep=new Array<CardModel>();
        Array<Array<CardModel>> listCachXep = new Array<Array<CardModel>>();
        if(k>0 && k <=n ) {
            a[0] = 0 ; // khởi tạo giá trị a[0]
            backtrack(listOriginal,1,n,k,listCachXep,arrCachXep) ;
        }
        else {
        }
        return listCachXep;
    }
    void backtrack(Array<CardModel> listOriginal, int i,int n,int k,Array<Array<CardModel>> listCachXep,Array<CardModel> arrCachXep) { // hàm quay lui
        for(int j = a[i-1]+1 ; j<=n-k+i ; j++ ) { // xét các khả năng của j
            a[i] = j ; // ghi nhận một giá trị của j
            if(i==k) { // nếu cấu hình đã đủ k phần tử
                // in một cấu hình ra ngoài
                printResult(listOriginal,listCachXep,k,arrCachXep) ;
            }
            else {
                backtrack(listOriginal, i+1,n,k,listCachXep,arrCachXep) ; // quay lui
            }
        }
    }
    void printResult(Array<CardModel> listOriginal, Array<Array<CardModel>> listCachXep,int k, Array<CardModel> arrCachXep) { // hàm dùng để in một cấu hình ra ngoài
        for(int i = 1 ; i <=k ; i++) {
            arrCachXep.add(listOriginal.get(a[i]-1));
        }
        listCachXep.add(new Array<CardModel>(arrCachXep));
        arrCachXep.clear();
    }
    public static void PrintListCard(Array<CardModel> list_card_model)
    {
        if (list_card_model == null)
            return;
        System.out.println("--START--");
        for (CardModel card_model : list_card_model)
            System.out.println(card_model.number+" "+card_model.type);
        System.out.println("--END--");
    }

    Array<PhomModel> CachXepABC(Array<CardModel> listOriginal, int n1,int n2,int n3)
    {
        Array<Array<CardModel>> listCachXep01;
        Array<Array<CardModel>> listCachXep02;
        Array<Array<CardModel>> listCachXep03;
        Array<PhomModel> listPhom = new Array<PhomModel>();
        PhomModel phomModel=new PhomModel();


        listCachXep01=toHopNChapK(listOriginal,n1);
        if (listCachXep01.size>0)
        {
            for (Array<CardModel> list_01:listCachXep01) {
               // PrintListCard(list_01);
                Array<CardModel> listOriginal02 = new Array<CardModel>(listOriginal);
                listOriginal02.removeAll(list_01,false);
                listCachXep02=toHopNChapK(listOriginal02,n2);
                if (listCachXep02.size>0)
                {
                    for (Array<CardModel> list_02:listCachXep02)
                    {
                        //PrintListCard(list_02);
                        Array<CardModel> listOriginal03 = new Array<CardModel>(listOriginal02);
                        listOriginal03.removeAll(list_02,false);
                        listCachXep03=toHopNChapK(listOriginal03,n3);
                        if (listCachXep03.size>0)
                        {
                            for (Array<CardModel> list_03:listCachXep03) {
                                //list 03
                              //  PrintListCard(list_03);
                                phomModel.Clear();
                                CheckAddPhom(phomModel,list_01);
                                CheckAddPhom(phomModel,list_02);
                                CheckAddPhom(phomModel,list_03);
                                AddNewPhom(listPhom,phomModel,0);
                            }
                        }
                        else
                        {
                            phomModel.Clear();
                            CheckAddPhom(phomModel,list_01);
                            CheckAddPhom(phomModel,list_02);
                            AddNewPhom(listPhom,phomModel,0);
                        }
                    }
                }
                else
                {
                    listCachXep03=toHopNChapK(listOriginal02,n3);
                    if (listCachXep03.size>0)
                    {
                        for (Array<CardModel> list_03:listCachXep03) {
                            //list 03
                           // PrintListCard(list_03);
                            phomModel.Clear();
                            CheckAddPhom(phomModel,list_01);
                            CheckAddPhom(phomModel,list_03);
                            AddNewPhom(listPhom,phomModel,0);
                        }
                    }
                    else
                    {
                        phomModel.Clear();
                        CheckAddPhom(phomModel,list_01);
                        AddNewPhom(listPhom,phomModel,0);

                    }
                }
            }
        }
        else
        {
            listCachXep02=toHopNChapK(listOriginal,n2);
            if (listCachXep02.size>0)
            {
                for (Array<CardModel> list_02:listCachXep02)
                {
                    if (list_02==null)
                        Gdx.app.log("Null","null");
                   // PrintListCard(list_02);
                    Array<CardModel> listOriginal03 = new Array<CardModel>(listOriginal);
                    listOriginal03.removeAll(list_02,false);
                    listCachXep03=toHopNChapK(listOriginal03,n3);
                    if (listCachXep03.size>0)
                    {
                        for (Array<CardModel> list_03:listCachXep03) {
                            //list 03
                           // PrintListCard(list_03);
                            phomModel.Clear();
                            CheckAddPhom(phomModel,list_02);
                            CheckAddPhom(phomModel,list_03);
                            AddNewPhom(listPhom,phomModel,0);
                        }
                    }
                    else {

                        phomModel.Clear();
                        CheckAddPhom(phomModel,list_02);
                        AddNewPhom(listPhom,phomModel,0);
                    }
                }
            }
            else
            {
                listCachXep03=toHopNChapK(listOriginal,n3);
                if (listCachXep03.size>0)
                {
                    for (Array<CardModel> list_03:listCachXep03) {
                        //list 03
                       // PrintListCard(list_03);
                        phomModel.Clear();
                        CheckAddPhom(phomModel,list_03);
                        AddNewPhom(listPhom,phomModel,0);
                    }
                }
                else {
                }
            }
        }
        return listPhom;
    }

    Array<PhomModel> TimTatCaListPhomTrong3La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        return listPhomTest;
    }
    Array<PhomModel> TimTatCaListPhomTrong4La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size>0)
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,0,4);
            if (listPhomTest.size>0)
                listPhom.addAll(listPhomTest);
        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong5La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size>0)
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,0,4);
            if (listPhomTest.size>0)
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,0,5);
                if (listPhomTest.size>0)
                {
                    listPhom.addAll(listPhomTest);
                }
            }
        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong6La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size == 0)
            return listPhom;
        listPhom.addAll(listPhomTest);
        //0-0-4
        listPhomTest = CachXepABC(listOriginal, 0, 0, 4);
        if (listPhomTest.size > 0) {
            listPhom.addAll(listPhomTest);
            listPhomTest = CachXepABC(listOriginal, 0, 0, 5);
            if (listPhomTest.size > 0) {
                listPhom.addAll(listPhomTest);
                listPhomTest = CachXepABC(listOriginal, 0, 0, 6);
                if (listPhomTest.size > 0) {
                    listPhom.addAll(listPhomTest);
                }
            }
        }
        //0-3-3
        listPhomTest = CachXepABC(listOriginal, 0, 3, 3);
        if (CheckCoPhom2(listPhomTest)&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,3)) {
            listPhom.addAll(listPhomTest);
        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong7La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size == 0)
            return listPhom;
        listPhom.addAll(listPhomTest);
        //0-0-4
        listPhomTest = CachXepABC(listOriginal, 0, 0, 4);
        if (listPhomTest.size > 0) {
            listPhom.addAll(listPhomTest);
            listPhomTest = CachXepABC(listOriginal, 0, 0, 5);
            if (listPhomTest.size > 0) {
                listPhom.addAll(listPhomTest);
                listPhomTest = CachXepABC(listOriginal, 0, 0, 6);
                if (listPhomTest.size > 0) {
                    listPhom.addAll(listPhomTest);listPhomTest = CachXepABC(listOriginal, 0, 0, 7);
                    if (listPhomTest.size > 0) {
                        listPhom.addAll(listPhomTest);
                    }
                }
            }
        }
        //0-3-3
        listPhomTest = CachXepABC(listOriginal, 0, 3, 3);
        if (CheckTrongListCoPhomTheoDang(listPhomTest,0,3,3)) {
            listPhom.addAll(listPhomTest);
            //0-3-4
            listPhomTest = CachXepABC(listOriginal, 0, 3, 4);
            if (listPhomTest.size > 0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,4)) {
                listPhom.addAll(listPhomTest);
            }
        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong8La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size == 0)
            return listPhom;
        listPhom.addAll(listPhomTest);
        //0-0-4
        listPhomTest = CachXepABC(listOriginal, 0, 0, 4);
        if (listPhomTest.size > 0) {
            listPhom.addAll(listPhomTest);
            listPhomTest = CachXepABC(listOriginal, 0, 0, 5);
            if (listPhomTest.size > 0) {
                listPhom.addAll(listPhomTest);
                listPhomTest = CachXepABC(listOriginal, 0, 0, 6);
                if (listPhomTest.size > 0) {
                    listPhom.addAll(listPhomTest);
                    listPhomTest = CachXepABC(listOriginal, 0, 0, 7);
                    if (listPhomTest.size > 0) {
                        listPhom.addAll(listPhomTest);
                        listPhomTest = CachXepABC(listOriginal, 0, 0, 8);
                        if (listPhomTest.size > 0) {
                            listPhom.addAll(listPhomTest);
                        }
                    }
                }
            }
        }
        //0-3-3
        listPhomTest = CachXepABC(listOriginal, 0, 3, 3);
        if (CheckTrongListCoPhomTheoDang(listPhomTest,0,3,3)) {
            listPhom.addAll(listPhomTest);
            //0-3-4
            listPhomTest = CachXepABC(listOriginal, 0, 3, 4);
            if (listPhomTest.size > 0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,4)) {
                listPhom.addAll(listPhomTest);
                listPhomTest = CachXepABC(listOriginal, 0, 3, 5);
                if (listPhomTest.size > 0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,5)) {
                    listPhom.addAll(listPhomTest);
                }
                listPhomTest = CachXepABC(listOriginal, 0, 4, 4);
                if (listPhomTest.size > 0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,4,4)) {
                    listPhom.addAll(listPhomTest);
                }
            }
        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong10La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size == 0)
            return listPhom;
        listPhom.addAll(listPhomTest);
        listPhomTest=CachXepABC(listOriginal,0,0,4);
        if (listPhomTest.size>0)
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,0,5);
            if (listPhomTest.size>0)
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,0,6);
                if (listPhomTest.size>0)
                {
                    listPhom.addAll(listPhomTest);
                    listPhomTest=CachXepABC(listOriginal,0,0,7);
                    if (listPhomTest.size>0)
                    {
                        listPhom.addAll(listPhomTest);
                        listPhomTest=CachXepABC(listOriginal,0,0,8);

                        if (listPhomTest.size>0)
                        {
                            listPhom.addAll(listPhomTest);
                            listPhomTest=CachXepABC(listOriginal,0,0,9);
                            if (listPhomTest.size>0)
                            {
                                listPhom.addAll(listPhomTest);
                                listPhomTest=CachXepABC(listOriginal,0,0,10);
                                if (listPhomTest.size>0) {
                                    listPhom.addAll(listPhomTest);
                                }
                            }
                        }
                    }
                }
            }
        }
        listPhomTest=CachXepABC(listOriginal,0,3,3);
        if (!CheckTrongListCoPhomTheoDang(listPhomTest,0,3,3))
            return listPhom;
        listPhom.addAll(listPhomTest);
        listPhomTest=CachXepABC(listOriginal,0,3,4);
        if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,0,3,4))
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,3,5);
            if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,0,1,5))
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,3,6);
                if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,0,1,6))
                {
                    listPhom.addAll(listPhomTest);
                    listPhomTest=CachXepABC(listOriginal,0,0,7);
                    if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,0,0,7))
                    {
                        listPhom.addAll(listPhomTest);
                    }
                }
            }
            listPhomTest=CachXepABC(listOriginal,0,4,4);
            if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,4,4))
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,4,5);
                if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,4,5))
                {
                    listPhom.addAll(listPhomTest);
                    listPhomTest=CachXepABC(listOriginal,0,4,6);
                    if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,4,6))
                    {
                        listPhom.addAll(listPhomTest);
                    }
                    listPhomTest=CachXepABC(listOriginal,0,5,5);
                    if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,5,5))
                    {
                        listPhom.addAll(listPhomTest);
                    }
                }
            }
        }

        listPhomTest=ListPhom3TuListPhom2(listOriginal,listPhom);
        if (listPhomTest.size>0)
            listPhom.addAll(listPhomTest);
//
//        listPhomTest=CachXepABC(listOriginal,3,3,3);
//        if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,3,3,3))
//        {
//            listPhom.addAll(listPhomTest);
//            listPhomTest=CachXepABC(listOriginal,3,3,4);
//            if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,3,3,4))
//            {
//                listPhom.addAll(listPhomTest);
//            }
//        }
        return listPhom;
    }
    Array<PhomModel> TimTatCaListPhomTrong9La(Array<CardModel> listOriginal) {
        Array<PhomModel> listPhom = new Array<PhomModel>();
        Array<PhomModel> listPhomTest = CachXepABC(listOriginal, 0, 0, 3);
        if (listPhomTest.size == 0)
            return listPhom;
        listPhom.addAll(listPhomTest);
        listPhomTest=CachXepABC(listOriginal,0,0,4);
        if (listPhomTest.size>0)
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,0,5);
            if (listPhomTest.size>0)
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,0,6);
                if (listPhomTest.size>0)
                {
                    listPhom.addAll(listPhomTest);
                    listPhomTest=CachXepABC(listOriginal,0,0,7);
                    if (listPhomTest.size>0)
                    {
                        listPhom.addAll(listPhomTest);
                        listPhomTest=CachXepABC(listOriginal,0,0,8);

                        if (listPhomTest.size>0)
                        {
                            listPhom.addAll(listPhomTest);
                            listPhomTest=CachXepABC(listOriginal,0,0,9);
                            if (listPhomTest.size>0)
                            {
                                listPhom.addAll(listPhomTest);
                            }
                        }
                    }
                }
            }
        }
        listPhomTest=CachXepABC(listOriginal,0,3,3);
        if (!CheckTrongListCoPhomTheoDang(listPhomTest,0,3,3))
            return listPhom;
        listPhom.addAll(listPhomTest);
        listPhomTest=CachXepABC(listOriginal,0,3,4);
        if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,4))
        {
            listPhom.addAll(listPhomTest);
            listPhomTest=CachXepABC(listOriginal,0,3,5);
            if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,5))
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,3,6);
                if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,3,6))
                {
                    listPhom.addAll(listPhomTest);
                }
            }
            listPhomTest=CachXepABC(listOriginal,0,4,4);
            if (listPhomTest.size>0 && CheckTrongListCoPhomTheoDang(listPhomTest,0,4,4))
            {
                listPhom.addAll(listPhomTest);
                listPhomTest=CachXepABC(listOriginal,0,4,5);
                if (listPhomTest.size>0&& CheckTrongListCoPhomTheoDang(listPhomTest,0,4,5))
                {
                    listPhom.addAll(listPhomTest);
                }
            }
        }

        listPhomTest=ListPhom3TuListPhom2(listOriginal,listPhom);
        if (listPhomTest.size>0)
            listPhom.addAll(listPhomTest);
//
//        listPhomTest=CachXepABC(listOriginal,3,3,3);
//        if (!CheckTrongListCoPhomTheoDang(listPhomTest,3,3,3))
//        {
//            listPhom.addAll(listPhomTest);
//        }
        return listPhom;
    }
    boolean CheckCoPhom2(Array<PhomModel> listPhom){
        for(PhomModel phom:listPhom)
            if (phom.list_phom.size>=2)
                return true;
        return false;
    }
    void XepPhomTheoSoLaGiamDan(PhomModel phom){
        for (int i=0;i<phom.list_phom.size-1;i++)
        {
            for (int j=i+1;j<phom.list_phom.size;j++)
            {
                if (phom.list_phom.get(i).size<phom.list_phom.get(j).size)
                {
                    //swap
                    phom.list_phom.swap(i,j);
                }
            }
        }
    }
    boolean CheckPhomCoDang(PhomModel phom, int a,int b,int c)
    {
        Array<Integer> arr = new Array<Integer>();
        arr.add(a);
        arr.add(b);
        arr.add(c);
        arr.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer < t1 ? 1 : integer == t1 ? 0 : -1;
            }
        });

        if (arr.get(2)>=3 && phom.list_phom.size<3)
            return false;
        if (arr.get(1)>=3 && phom.list_phom.size<2)
            return false;
        XepPhomTheoSoLaGiamDan(phom);
        for (int i=0;i<phom.list_phom.size;i++)
        {
            if (phom.list_phom.get(i).size!=arr.get(i))
                return false;
        }
        return true;
    }
    boolean CheckTrongListCoPhomTheoDang(Array<PhomModel> listPhom,int a,int b,int c){
        for(PhomModel phom:listPhom)
        {
            if (CheckPhomCoDang(phom,a,b,c))
                return true;
        }

        return false;
    }
    public boolean CheckDanhQuanHopLe(PlayerModel player, CardModel card)
    {
        if (player.list_eat.size==0)
            return true;
        if (player.list_eat.contains(card,false))
            return false;
        Array<CardModel> list_hold = new Array<CardModel>(player.list_hold);
        list_hold.removeValue(card,false);
        return CheckCanEat(list_hold,player.list_eat);
    }
    public void Clear(){
        for (int i = 0; i < 4; i++) {
            listPlayer.get(i).Clear();
        }
        listCardOnTable.clear();
        listCardOnTable.addAll(boardModel.listCard);
        listCardOnTable.shuffle();

        scoreBet = GameController.GetScoreBet();
        gameState = GameState.OVER;
        host = PreferenceController.Instance.preferences.getInteger("Host");
        index_player_ha_dau=host;
        currentTurn=host;
        sumScoreWin=0;
        turnState=null;
    }
    public boolean KiemTraCoPhom(Array<CardModel> list,CardModel card)
    {
        if (card==null)
            return false;
        if (SoLaBang(list,card.number)>=3)
            return true;
        SortCardInList(list,false);
        int index = list.indexOf(card,false);
        if (TonTaiCard(list,card.number-1,card.type) && TonTaiCard(list,card.number+1,card.type))
            return true;
        if (TonTaiCard(list,card.number-2,card.type) && TonTaiCard(list,card.number-1,card.type))
            return true;
        if (TonTaiCard(list,card.number+1,card.type) && TonTaiCard(list,card.number+2,card.type))
            return true;
        return false;
    }
    int SoLaBang(Array<CardModel> list,int number)
    {
        int dem=0;
        for (CardModel card:list)
            if (card.number==number)
                dem++;
        return dem;
    }
    boolean TonTaiCard(Array<CardModel> list,int number,CardModel.CardType type)
    {
        for (CardModel card:list)
            if (card.number==number && card.type==type)
                return true;
        return false;
    }



    public int sumScoreWin;

    public Array<PlayerModel> ListScore()
    {
        Array<PlayerModel> list_player = new Array<PlayerModel> ();
        list_player.addAll (listPlayer);
        for (int i = 0; i < 3; i++) {
            for (int j = i + 1; j < 4; j++) {
                PlayerModel player_1 = list_player.get(i);
                PlayerModel player_2 = list_player.get(j);
                if (!ComparePlayer(player_1,player_2))
                    SwapPlayer (list_player,player_1,player_2);
            }
        }

        return list_player;
    }

    void SwapPlayer(Array<PlayerModel> list,PlayerModel player1,PlayerModel player2)
    {
        PlayerModel player_model_temp = player1;
        int index_player_1 = list.indexOf(player1,false);
        int index_player_2 = list.indexOf(player2,false);
        list.set(index_player_1,player2);
        list.set(index_player_2,player_model_temp);
    }

    boolean ComparePlayer(PlayerModel player_1,PlayerModel player_2)
    {
        if (player_1.list_hold.size == 9 && player_2.list_hold.size < 9)
            return false;
        if (player_1.list_hold.size < 9 && player_2.list_hold.size == 9)
            return true;
        if (player_1.list_hold.size >= 9 && player_2.list_hold.size >= 9) {
            Array<Integer> list_down = new Array<Integer> ();
            for (int i = 0; i < 4; i++)
                list_down.add (ConvertTurn(index_player_ha_dau+i));
            int index_player_1 = listPlayer.indexOf (player_1,false);
            int index_player_2 = listPlayer.indexOf (player_2,false);
            return list_down.indexOf (index_player_1,false)<list_down.indexOf(index_player_2,false);
        }
        if (player_1.list_hold.size < 9 && player_2.list_hold.size < 9) {
            if (CountScoreOfPlayer (player_1) < CountScoreOfPlayer (player_2))
                return true;
            if (CountScoreOfPlayer (player_1) > CountScoreOfPlayer (player_2))
                return false;
            Array<Integer> list_down = new Array<Integer> ();
            for (int i = 0; i < 4; i++)
                list_down.add (ConvertTurn(index_player_ha_dau+i));
            int index_player_1 = listPlayer.indexOf (player_1,false);
            int index_player_2 = listPlayer.indexOf (player_2,false);
            return list_down.indexOf (index_player_1,false)<list_down.indexOf(index_player_2,false);
        }
        return true;
    }
    int CountScoreOfPlayer(PlayerModel player)
    {
        int score = 0;
        for (int i = 0; i < player.list_hold.size; i++)
            score += player.list_hold .get(i).number;
        if (player.list_hold.size == 9)
            score = 1000;
        return score;
    }
    public boolean KiemTraCoNenAn(){
        return true;
    }
    public Array<CardModel> ListOut(PlayerModel player_model, int level,Array<CardModel> list_hop_le)
    {
        Array<CardModel> list_hold = new Array<CardModel>();
        list_hold.addAll(player_model.list_hold);
        Array<CardModel> list_eat = new Array<CardModel>();
        list_eat.addAll(player_model.list_eat);


        //Array<Array<CardModel>> list_phom = GetListPhomAllFromListEat (list_hold, list_eat);

        //Array<CardModel> list_card_model = ListCanOut (player_model);

        Array<CardModel> list_card_model = new Array<CardModel>();
        list_card_model.addAll(list_hop_le);
        //DeleteCardByList (list_card_model,list_phom[0]);
        //DeleteCardByList (list_card_model,list_phom[1]);

        if (level == 3)
        { //tim tat ca
            return list_card_model;
        }
        SortCardInList(list_card_model, true);

        Array<CardModel> list_sort_phom3 = SortPhom3(list_card_model);
        DeleteCardByList(list_card_model, list_sort_phom3);

        Array<CardModel> list_sort_phom_sanh = SortPhomSanh(list_card_model);
        //  PrintListCard(list_sort_phom_sanh);
        DeleteCardByList(list_card_model, list_sort_phom_sanh);

        Array<CardModel> list_sort_ca_3 = SortCa3(list_card_model);
        // PrintListCard(list_sort_ca_3);
        DeleteCardByList(list_card_model, list_sort_ca_3);

        Array<CardModel> list_sort_ca_sanh = SortCaSanh(list_card_model);
        // PrintListCard(list_sort_ca_sanh);
        DeleteCardByList(list_card_model, list_sort_ca_sanh);

        if (level == 0)
        { //tim rac
        }
        else
        if (level == 1)
        { //tim rac, ca
            list_card_model.addAll(list_sort_ca_sanh);
            list_card_model.addAll(list_sort_ca_3);
        }
        else
        if (level == 2)
        { //tim rac, ca, phom
            list_card_model.addAll(list_sort_ca_sanh);
            list_card_model.addAll(list_sort_ca_3);
            list_card_model.addAll(list_sort_phom_sanh);
            list_card_model.addAll(list_sort_phom3);
        }
        return list_card_model;
    }
    public CardModel UuTienDanhQuanLonNhat(Array<CardModel> list, int min) //Uu tien danh quan lon nhat
    {
        SortCardInList(list, false);
        if (list.size == 0)
            return null;
        CardModel card = list.get(list.size - 1);
        if (card.number < min)
            return null;
        return card;
    }
    public Array<CardModel> DanhQuanKhongAnDuoc(PlayerModel player, Array<CardModel> list_hold, PlayerModel nextPlayer) //Danh quan ko an duoc (nhin bai)
    {
        Array<CardModel> list_result = new Array<CardModel>();

        Array<CardModel> list_hold_of_next_player = new Array<CardModel>();
        list_hold_of_next_player.addAll(nextPlayer.list_hold);
        Array<CardModel> list_eat_of_next_player = new Array<CardModel>();
        list_eat_of_next_player.addAll(nextPlayer.list_eat);

        for (CardModel card : list_hold)
        {

            Array<CardModel> list_test = new Array<CardModel>();
            list_test.addAll(list_hold_of_next_player);
            Array<CardModel> list_eat_test = new Array<CardModel>();
            list_eat_test.addAll(list_eat_of_next_player);
            list_eat_test.add(card);
            if (!CheckCanEat(list_test, list_eat_test))
                list_result.add(card);
        }
        return list_result;
    }
    public Array<CardModel> DanhQuanTaoPhomVoiBaiDaDanh(Array<CardModel> list_card) //Danh quan tao phom voi bai da danh de doi phuong ko an dc
    {
        Array<CardModel> list_result = new Array<CardModel>();

        Array<CardModel> list = new Array<CardModel>();
        for (PlayerModel player : listPlayer)
        list.addAll(player.list_out);
        for (CardModel card : list_card)
        {
            Array<CardModel> list_phom_3 = ListPhom3FromCard(list, card);
            Array<CardModel> list_phom_sanh = ListPhomSanhFromCard(list, card, new Array<CardModel>());
            if (list_phom_3.size != 0 || list_phom_sanh.size != 0)
                list_result.add(card);
        }
        return list_result;
    }
    Array<CardModel> ListPhom3FromCard(Array<CardModel> list_card, CardModel card_model)
    {
        Array<CardModel> list = new Array<CardModel>();
        list.addAll(list_card);
        if (!list.contains(card_model,false))
            list.add(card_model);

        Array<CardModel> list_result = new Array<CardModel>();
        for (CardModel card : list)
        {
            if (CardModel.IsSameNumber(card, card_model))
                list_result.add(card);
        }
        if (list_result.size >= 3)
            return list_result;
        return new Array<CardModel>();
    }

    Array<CardModel> ListPhomSanhFromCard(Array<CardModel> list_card, CardModel card_model, Array<CardModel> list_eat)
    {
        Array<CardModel> list_eat_new = new Array<CardModel>();
        list_eat_new.addAll(list_eat);
        list_eat_new.removeValue(card_model,false);
        if (list_card == null)
            return new Array<CardModel>();
        if (list_card.size == 0)
            return new Array<CardModel>();
        Array<CardModel> list = new Array<CardModel>();
        list.addAll(list_card);
        if (!list.contains(card_model,false))
            list.add(card_model);

        Array<CardModel> list_result = new Array<CardModel>();

        for (int t = 0; t < list.size; t++)
        {
            if (!CardModel.IsSameType(list.get(t), card_model))
            {
                list.removeValue(list.get(t),false);
                t--;
            }
        }
        SortCardInList(list, false);
        int index = list.indexOf(card_model,false);
        int i = index;
        int j = index;
        if (i > 0)
            while (list.get(i).number - list.get(i - 1).number == 1 || list_eat_new.contains(list.get(i - 1),false))
            {
                i--;
                if (i == 0)
                    break;
            }
        if (j < list.size - 1)
            while (list.get(j + 1).number - list.get(j).number == 1 || list_eat_new.contains(list.get(j + 1),false))
            {
                j++;
                if (j == list.size - 1)
                    break;
            }
        if (j - i + 1 >= 3)
        {
            list_result.add(card_model);
            int index_card = list.indexOf(card_model,false);
            if (index_card == i)
            {
                list_result.add(list.get(index_card + 1));
                list_result.add(list.get(index_card + 2));
            }
            else
            if (index_card == j)
            {
                list_result.add(list.get(index_card - 1));
                list_result.add(list.get(index_card - 2));
            }
            else
            {
                list_result.add(list.get(index_card - 1));
                list_result.add(list.get(index_card + 1));
            }
            SortCardInList(list_result, false);
            return list_result;
        }
        return new Array<CardModel>();
    }
    public Array<CardModel> DanhQuanTaoCaVoiBaiDaDanh(Array<CardModel> list_card) //Danh quan tao phom voi bai da danh de doi phuong ko an dc
    {
        Array<CardModel> list_result = new Array<CardModel>();

        Array<CardModel> list = new Array<CardModel>();
        for (PlayerModel player:listPlayer)
            list.addAll(player.list_out);
        for (CardModel card:list_card)
        {
            list.add(card);
            Array<CardModel> list_ca = SortCa3(list);
            list_ca.addAll(SortCaSanh(list));
            if (list_ca.size != 0 && list_ca.contains(card,false))
                list_result.add(card);
            list.removeValue(card,false);
        }
        return list_result;
    }


    public CardModel UuTienDanhQuanK(Array<CardModel> list) //Uu tien danh quan K
    {
        Array<CardModel> list_ca_3 = SortCa3(list);
        Array<CardModel> list_ca_sanh = SortCaSanh(list);
        if (list_ca_3.size == 0 && list_ca_sanh.size == 0)
        {
            for (CardModel card:list)
                if (card.number == 13)
                    return card;
        }
        return null;
    }
    Array<Array<CardModel>> ListCaSanh(Array<CardModel> list)
    {
        Array<Array<CardModel>> list_result = new Array<Array<CardModel>>();

        SortCardInList(list, true);

        Array<CardModel> list_sorted = new Array<CardModel>();
        for (int i = 0; i < list.size; i++)
        {
            CardModel card_model = list.get(i);
            if (list_sorted.contains(card_model,false))
                continue;
            Array<CardModel> list_ca = CountCaSanhByCard(list, card_model, list_sorted);
            if (list_ca.size >= 2)
            {
                SortCardInList(list_ca, false);
                list_result.add(list_ca);
            }
        }
        return list_result;
    }

    Array<Array<CardModel>> ListCa3(Array<CardModel> list)
    {
        Array<Array<CardModel>> list_result = new Array<Array<CardModel>>();
        SortCardInList(list, true);
        list_result.add(new Array<CardModel>());
        int dem_khac = 0;
        for (int i = 0; i < list.size - 1; i++)
        {
            if (!list_result.get(dem_khac).contains(list.get(i),false))
                list_result.get(dem_khac).add(list.get(i));

            if (list.get(i + 1).number - list.get(i).number != 0)
            {
                dem_khac++;
                list_result.add(new Array<CardModel>());
            }
            if (!list_result.get(dem_khac).contains(list.get(i + 1),false))
                list_result.get(dem_khac).add(list.get(i + 1));
        }
        return list_result;
    }

    public Array<CardModel> DanhQuanKhongConPhomTrongTay(Array<CardModel> list_card) //Danh quan ko con co the tao phom
    {
        Array<CardModel> list_hold = new Array<CardModel>();
        list_hold.addAll(list_card);

        Array<CardModel> list_can_out = new Array<CardModel>();
        list_can_out.addAll(list_hold);

        Array<CardModel> list_sort_ca_3 = SortCa3(list_hold);
        DeleteCardByList(list_hold, list_sort_ca_3);

        Array<CardModel> list_sort_ca_sanh = SortCaSanh(list_hold);
        DeleteCardByList(list_hold, list_sort_ca_sanh);

        Array<Array<CardModel>> list_ca_3 = ListCa3(list_sort_ca_3);
        Array<Array<CardModel>> list_ca_sanh = ListCaSanh(list_sort_ca_sanh);

        Array<CardModel> list_result = new Array<CardModel>();
        for (Array<CardModel> list:list_ca_3)
        {
            if (list.size == 0)
                continue;
            if (!KiemTraConCaDeTaoPhom(list))
                list_result.addAll(list);
        }
        for (Array<CardModel> list:list_ca_sanh)
        {
            if (list.size == 0)
                continue;
            if (!KiemTraConCaDeTaoPhom(list))
                list_result.addAll(list);
        }
        return list_result;
    }
    boolean GetTypeCa(Array<CardModel> list)
    {
        return list.get(0).number == list.get(1).number;
        //true: 3 false: sanh
    }
    Array<CardModel.CardType> GetTypeNotExists(Array<CardModel> list)
    {
        Array<CardModel.CardType> list_type = new Array<CardModel.CardType>();
        for (int i = 0; i < 4; i++)
        {
            if (i != list.get(0).type.ordinal() && i != list.get(1).type.ordinal())
                list_type.add((CardModel.CardType.values()[i]));
        }
        return list_type;
    }
    boolean KiemTraConCaDeTaoPhom(Array<CardModel> list)
    {
        SortCardInList(list, false);
        boolean isCa3 = GetTypeCa(list);
        int number_need_1;
        int number_need_2;
        CardModel.CardType type_need_1;
        CardModel.CardType type_need_2;

        Array<CardModel> list_outed = new Array<CardModel>();
        for (PlayerModel player:listPlayer)
            list_outed.addAll(player.list_out);

        if (isCa3)
        {
            number_need_1 = list.get(0).number;
            number_need_2 = list.get(0).number;
            Array<CardModel.CardType> list_type = GetTypeNotExists(list);
            type_need_1 = list_type.get(0);
            type_need_2 = list_type.get(1);

            for (CardModel card:list_outed)
            {
                if (card.number == number_need_1 && (card.type == type_need_1 || card.type == type_need_2))
                    return false;
            }

        }
        else
        {
            if (Math.abs(list.get(0).number - list.get(1).number) != 1)
            {
                number_need_1 = (list.get(0).number + list.get(1).number) / 2;
                number_need_2 = 0;
            }
            else
            {
                number_need_1 = list.get(0).number - 1;
                number_need_2 = list.get(1).number + 1;
            }
            type_need_1 = list.get(0).type;
            type_need_2 = list.get(0).type;
            for (CardModel card:list_outed)
            {
                if ((card.number == number_need_1 || card.number == number_need_2) && (card.type == type_need_1))
                    return false;
            }
        }
        return true;
    }

    public Array<CardModel> DanhChoDoiPhuongAn(Array<CardModel> list_hold, PlayerModel player) //nhin bai, danh cho doi phuong an
    {
        int iPlayer = listPlayer.indexOf(player,false);
        int iNextPlayer = ConvertTurn(iPlayer + 1);
        PlayerModel nextPlayer = listPlayer.get(iNextPlayer);
        Array<CardModel> list_result = new Array<CardModel>();

        Array<CardModel> list_hold_of_next_player = new Array<CardModel>();
        list_hold_of_next_player.addAll(nextPlayer.list_hold);
        Array<CardModel> list_eat_of_next_player = new Array<CardModel>();

        for (CardModel card:list_hold)
        {

            Array<CardModel> list_test = new Array<CardModel>();
            list_test.addAll(list_hold_of_next_player);
            Array<CardModel> list_eat_test = new Array<CardModel>();
            list_eat_test.addAll(list_eat_of_next_player);
            list_eat_test.add(card);
            if (CheckCanEat(list_test, list_eat_test))
                list_result.add(card);
        }
        return list_result;
    }

    public PlayerModel NextPlayer()
    {
        return listPlayer.get(ConvertTurn(currentTurn + 1));
    }
    public boolean KiemTraCoNenAn(PlayerModel player_model, CardModel card_to_eat)
    {
        return true;
    }

    public boolean CoNenChoDoiPhuongAn(PlayerModel player_model)
    {
        return false;
    }
    public Array<CardModel> CauBai(Array<CardModel> list_card)   //Chon card de cau bai
    {
        Array<CardModel> list_hold = new Array<CardModel>();
        list_hold.addAll(list_card);

        Array<CardModel> list_can_out = new Array<CardModel>();
        list_can_out.addAll(list_hold);

        Array<CardModel> list_sort_ca_3 = SortCa3(list_hold);
        DeleteCardByList(list_hold, list_sort_ca_3);

        Array<CardModel> list_sort_ca_sanh = SortCaSanh(list_hold);
        DeleteCardByList(list_hold, list_sort_ca_sanh);

        Array<Array<CardModel>> list_ca_3 = ListCa3(list_sort_ca_3);
        Array<Array<CardModel>> list_ca_sanh = ListCaSanh(list_sort_ca_sanh);

        Array<CardModel> list_result = new Array<CardModel>();

        for (Array<CardModel> list:list_ca_3)
        {
            if (list.size == 0)
                continue;
            CardModel card = GetCardToCauFromCa(list, list_can_out);
            if (card != null)
                list_result.add(card);
        }
        for (Array<CardModel> list:list_ca_sanh)
        {
            if (list.size == 0)
                continue;
            CardModel card = GetCardToCauFromCa(list, list_can_out);
            if (card != null)
                list_result.add(card);
        }
        return list_result;
    }
    CardModel GetCardToCauFromCa(Array<CardModel> list, Array<CardModel> list_hold)
    {
        SortCardInList(list, false);
        boolean isCa3 = GetTypeCa(list);
        int number_need_1;
        int number_need_2;
        CardModel.CardType type_need_1;
        CardModel.CardType type_need_2;
        if (list.get(0).number == 10)
        {
            int i = 0;
        }
        if (isCa3)
        {
            number_need_1 = list.get(0).number;
            number_need_2 = list.get(0).number;
            Array<CardModel.CardType> list_type = GetTypeNotExists(list);
            type_need_1 = list_type.get(0);
            type_need_2 = list_type.get(1);

            for (CardModel card:list_hold)
            {
                if (list.contains(card,false))
                    continue;
                if ((Math.abs(card.number - number_need_1) == 1) && (card.type == type_need_1 || card.type == type_need_2))
                    return card;
            }

        }
        else
        {
            if (Math.abs(list.get(0).number - list.get(1).number) != 1)
            {
                number_need_1 = (list.get(0).number + list.get(1).number) / 2;
                number_need_2 = 0;
            }
            else
            {
                number_need_1 = list.get(0).number - 1;
                number_need_2 = list.get(1).number + 1;
            }
            type_need_1 = list.get(0).type;
            type_need_2 = list.get(0).type;
            for (CardModel card:list_hold)
            {
                if (list.contains(card,false))
                    continue;
                if ((card.number == number_need_1) || (card.number == number_need_2))
                    return card;
            }
        }
        return null;
    }
    public Array<BotBasicController> GetListBot(){
        Array<BotBasicController> listBot = new Array<BotBasicController>();
        listBot.add(new Bot1Controller());
        listBot.add(new Bot3Controller());
        listBot.add(new Bot7Controller());
        listBot.add(new Bot9Controller());
        listBot.add(new Bot10Controller());
        listBot.add(new Bot4Controller());
        listBot.add(new Bot2Controller());
        listBot.add(new Bot5Controller());
        listBot.add(new Bot6Controller());
        listBot.add(new Bot8Controller());
        return listBot;
    }
    public Array<BotBasicController> GetListBotPlayer3(){
        Array<BotBasicController> listBot = new Array<BotBasicController>();
        listBot.add(new Bot1Controller());
        listBot.add(new Bot3Controller());
        listBot.add(new Bot7Controller());
        listBot.add(new Bot9Controller());
        listBot.add(new Bot10Controller());
        listBot.add(new Bot4Controller());
        listBot.add(new Bot2Controller());
        listBot.add(new Bot5Controller());
        listBot.add(new Bot6Controller());
        listBot.add(new Bot8Controller());
        listBot.add(new BotNguController());
        listBot.add(new BotNgu2Controller());
        listBot.add(new BotNgu3Controller());
        return listBot;
    }
}
