package com.mygdx.catte.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;


/**
 * Created by DUY on 6/12/2018.
 */

public class BoardModel {
    public static BoardModel Instance;

    public Array<CardModel> listCard;

    public BoardModel(){
        Instance = this;
        InitBoard ();
    }

    void InitBoard()
    {
        int countNumber = 1;
        int countType = -1;
        listCard = new Array<CardModel> ();
        for (int i = 0; i < 52; i++) {
            countType++;
            if (countType == 4) {
                countType= 0;
                countNumber++;
            }
            listCard.add(new CardModel (countNumber,CardModel.CardType.values()[countType]));
        }
    }

    public void PrintBoard()
    {
        for (int i=0;i<listCard.size;i++){
            Gdx.app.log("PrintBoard",listCard.get(i).number+":"+listCard.get(i).type);
        }
    }

    public int GetIndexOfCardInBoard(CardModel card_model)
    {
        return listCard.indexOf(card_model,false);
    }

    public int GetIndexOfCardOtherBoard(CardModel card_model)
    {
        for (CardModel card:listCard
             ) {
            if (CardModel.CompareCards (card_model, card) == 0)
                return GetIndexOfCardInBoard (card);
        }
        return -1;
    }
}
