package com.mygdx.catte.GameSetting;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/13/2018.
 */

public class GameSetting {
    public static GameSetting Instance;

    public Color colorShadow[];
    public boolean isGuiBai;
    public Texture textureTypeCard;
    public boolean isShowGuide;
    public boolean isSound;

    public GameSetting(){
        Instance=this;
        colorShadow=new Color[3];
        colorShadow[0]=Color.WHITE;
        colorShadow[1]= GameUI.NewColor(143,143,143,255);
        colorShadow[2]=GameUI.NewColor(56,56,56,255);
    }
}
