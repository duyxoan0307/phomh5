package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/15/2018.
 */

public class OverlayUI extends Group {
    public static OverlayUI Instance;
    static Image imgOverlay;
    public OverlayUI(){
        Instance=this;
        imgOverlay=new Image(Assets.GetTexture(Assets.overlay));
        imgOverlay.setSize(Phom.V_WIDTH, Phom.V_HEIGHT);
        addActor(imgOverlay);
        GameUI.SetPositionActor(imgOverlay,0,0);
        Hide();

        imgOverlay.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                UIController.Instance.CheckBackButton();
            }
        });

        locateActors();
    }
    public static void Show(){
        SequenceAction sequence = new SequenceAction();
        sequence.addAction(Actions.alpha(0.8f, 0.3f));
        imgOverlay.addAction(sequence);
        Instance.toFront();
        imgOverlay.setTouchable(Touchable.enabled);
    }
    public static void Hide(){
        float duration=0.3f;
        SequenceAction sequence = new SequenceAction();
        sequence.addAction(Actions.alpha(0f, duration));
        imgOverlay.addAction(sequence);
        imgOverlay.setTouchable(Touchable.disabled);
    }
    public void locateActors(){
        imgOverlay.setSize(Phom.V_WIDTH,Phom.V_HEIGHT);
        imgOverlay.setPosition(0,0, Align.center);
    }
}
