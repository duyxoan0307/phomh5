package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Utilities.GameUI;


/**
 * Created by DUY on 6/15/2018.
 */

public class PauseUI extends Group {
    public PauseUI(){
        CreateUI();
        GameUI.HideNonAction(this);
    }

    private void CreateUI() {
        //imgBox
        Image imgBox = new Image(Assets.GetTexture(Assets.imgBox));
        imgBox.setPosition(0,0,Align.center);

        //btnExit
        Button btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        GameUI.SetPositionActor(btnExit, imgBox.getX() + imgBox.getWidth() - 20, imgBox.getY() + imgBox.getHeight() - 20);

        //txtTitle
        Label txtTitle = GameUI.NewLabel("TẠM DỪNG", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle, 2);
        txtTitle.setPosition(0,imgBox.getY(Align.top)-60,Align.center);

        //txtContent
        Label txtContent = GameUI.NewLabel("Bạn có muốn thoát ra menu không?",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtContent, 2);
        txtContent.setWidth(imgBox.getWidth()-30);
        txtContent.setWrap(true);
        txtContent.setAlignment(Align.top);
        txtContent.setPosition(0,imgBox.getY(Align.top)-150,Align.top);

        //btnYes
        Button btnMenu = GameUI.NewButton(Assets.GetTexture(Assets.btnThoat));
        GameUI.SetPositionActor(btnMenu,-260,-160);

        //btnNo
        Button btnResume = GameUI.NewButton(Assets.GetTexture(Assets.btnTiepTuc));
        GameUI.SetPositionActor(btnResume,260,-160);

        //add actor
        addActor(imgBox);
        addActor(btnExit);
        addActor(txtTitle);
        addActor(txtContent);
        addActor(btnMenu);
        addActor(btnResume);

        //addListener
        btnMenu.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                Timer.instance().clear();
                GameUI.Hide(PauseUI.this);
                GameUI.HideNonAction(UIController.Instance.playingUI);
                OverlayUI.Hide();
                GameModel.Instance.isPlaying=false;
                UIController.Instance.startUI.Show();
            }
        });
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                onClickExitPause();
            }
        });
        btnResume.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                onClickExitPause();
            }
        });
    }
    public void onClickExitPause(){
        OverlayUI.Hide();
        GameUI.Hide(this);
    }
}
