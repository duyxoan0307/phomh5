package com.mygdx.catte.UI;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/14/2018.
 */

public class OptionUI extends Group {
    public Button btnGuiBai;
    public Label txtContent;
    Table table;
    Button btnExit;
    public OptionUI(){
        CreateUI();
        GameUI.HideNonAction(this);
        locateActors();
    }
    void CreateUI(){
        //txtContent
        txtContent = GameUI.NewLabel("Bạn không đủ tiền cược để chơi mức này!",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtContent.setColor(Color.RED);
        GameUI.ScaleLabel(txtContent, 2);
        txtContent.setWidth(Phom.V_WIDTH-30);
        txtContent.setWrap(true);
        txtContent.setAlignment(Align.top);
        txtContent.setPosition(0,350,Align.top);

        //btnQuit
        btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));

        //txtCheDoChoi
        Label txtTitle = GameUI.NewLabel("CHỌN MỨC CƯỢC",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle,2);
        txtTitle.setPosition(0,230,Align.top);

        //txtGuiBai
        Label txtGuiBai=GameUI.NewLabel("Gửi bài",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtGuiBai.setAlignment(Align.left);
        GameUI.ScaleLabel(txtGuiBai,1.5f);
        GameUI.SetPositionActor(txtGuiBai,-120,-130);

        btnGuiBai = GameUI.NewButton(Assets.GetTexture(Assets.btnActive));
        btnGuiBai.setPosition(120,-130,Align.center);

        //btnStart
        Button btnStart = GameUI.NewButton(Assets.GetTexture(Assets.btnVanMoi));

        GameUI.SetPositionActor(btnStart,0,btnGuiBai.getY(Align.bottom)-100);

        //image overlay
        final Image imgOverlayScoreBet=new Image(Assets.GetTexture(Assets.money_overlay));
        imgOverlayScoreBet.setSize(210,210);

        //score bet
        table=new Table();
        table.setY(30);
        table.defaults().width(200).height(200);
        for (int i=0;i<4;i++)
        {
            Group group = new Group();
            Image overlay = new Image(Assets.GetTexture(Assets.money_overlay));

            final Button button = GameUI.NewButton(Assets.GetTexture(Assets.GetScoreBet(i)));
            button.setPosition(overlay.getX(Align.center),overlay.getY(Align.center),Align.center);

            Image imgLock = new Image(Assets.GetTexture(Assets.lock_icon));
            imgLock.setTouchable(Touchable.disabled);
            imgLock.setPosition(overlay.getX(Align.center),overlay.getY(Align.center),Align.center);

            group.addActor(overlay);
            group.addActor(button);
            group.addActor(imgLock);

            table.add(group).pad(20);
            if (i==3)
                table.row();
            final int finalI = i;
            button.addListener(new IClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundController.Instance.PlayClipButton();
                    UIController.Instance.onClickChangeScoreBet(finalI);
                }
            });
        }
        //addActor
        addActor(btnExit);
        addActor(txtTitle);
        addActor(btnStart);
        addActor(txtGuiBai);
        addActor(btnGuiBai);
        addActor(txtContent);
        addActor(table);

        //addListener
        btnStart.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();
                UIController.Instance.onClickStartNewGame();

            }
        });
        btnGuiBai.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();
                UIController.Instance.onClickChangeGuiBai();

            }
        });
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();
                onClickExitOption();

            }
        });
    }
    public void UpdateButton(){
        ResetOverlayAllButton();
        Preferences preferences = PreferenceController.Instance.preferences;
        int score_bet = preferences.getInteger("ScoreBet");
        Group group = (Group)table.getChildren().get(score_bet);
        Image overlay = (Image)group.getChildren().get(0);
        overlay.setVisible(true);
        for (int i=0;i<table.getChildren().size;i++)
        {
            Group _group = (Group)table.getChildren().get(i);
            Image _imgLock =(Image)_group.getChildren().get(2);
            _imgLock.setVisible(i>preferences.getInteger("MaxScoreBet"));
        }
    }
    void ResetOverlayAllButton(){
        for (int i=0;i<4;i++)
        {
            Group group = (Group)table.getChildren().get(i);
            Image overlay = (Image)group.getChildren().get(0);
            overlay.setVisible(false);
        }
    }
    public void onClickExitOption(){
        OverlayUI.Hide();
        Hide();
    }
    public void Show(){
        txtContent.setVisible(false);
        GameUI.ShowFade(this);
    }
    public void Hide(){
        GameUI.HideFade(this);
    }
    public void locateActors(){
        btnExit.setPosition(Phom.V_WIDTH/2-65,Phom.V_HEIGHT/2-65,Align.topRight);
    }
}
