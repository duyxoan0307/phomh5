package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Utilities.GameUI;

import java.util.ArrayList;

/**
 * Created by DUY on 8/9/2018.
 */

public class LoadingUI extends Group{
    Label txtPercent;
    Image imgBackground;
    Image imgLogo;
    Image loadingCircle;
    Texture backgroundTexture;
    Texture logoTexture;
    Texture loadingCircleTexture;
    BitmapFont sairaFont;

    public LoadingUI(){
        backgroundTexture=GameUI.NewTexture("UI/backgroundStart.jpg");
        imgBackground = new Image(backgroundTexture);
        imgBackground.setSize(Phom.V_WIDTH,Phom.V_HEIGHT);
        imgBackground.setPosition(0,0,Align.center);

        logoTexture=GameUI.NewTexture("UI/logo.png");
        imgLogo=new Image(logoTexture);
        imgLogo.setPosition(0,120,Align.center);

        loadingCircleTexture=GameUI.NewTexture("UI/loading_circle.png");
        loadingCircle = new Image(loadingCircleTexture);
        loadingCircle.setOrigin(Align.center);

        sairaFont= GameUI.NewFont("Saira_ExtraBold");
        txtPercent= GameUI.NewLabel("0%",sairaFont);
        txtPercent.setPosition(0,-100, Align.center);


        loadingCircle.setPosition(txtPercent.getX(Align.center),txtPercent.getY(Align.center),Align.center);
        GameUI.ScaleLabel(txtPercent,2);
        Label txtLoading = GameUI.NewLabel("Loading...","Saira_ExtraBold");
        GameUI.ScaleLabel(txtLoading,2);
        txtLoading.setPosition(0,-250,Align.center);

        addActor(imgBackground);
        addActor(imgLogo);
        addActor(txtLoading);
        addActor(loadingCircle);
        //addActor(txtPercent);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        loadingCircle.rotateBy(-1);
    }

    public void SetPercent(int percent){
        txtPercent.setText(percent + "%");
    }
    public void locateActors(){
        imgBackground.setSize(Phom.V_WIDTH,Phom.V_HEIGHT);
        imgBackground.setPosition(0,0, Align.center);
    }
    public void dispose(){
        backgroundTexture.dispose();
        logoTexture.dispose();
        loadingCircleTexture.dispose();
        sairaFont.dispose();
    }
}
