package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Utilities.GameUI;



public class FBLeaderboardUI extends Group {


    ScrollPane scroll;
    Table table;
    LeaderboardItemUI myRank;
    String playerID;

    Array<String> arrIDBanned=new Array<String>();

    void AddIDBanned(){
        arrIDBanned.add("1928774203874429");
    }
    public void ShowLeaderboard(){
        if (myRank!=null)
            myRank.remove();
        table.clearChildren();
        final int scrollWidth = (int)scroll.getWidth();
        table.defaults().expandX().space(4);
        final int[] count_rank = {0};
        Phom.zenSDK.FBInstant_LoadLeaderboard (new IZen.FBInstant_LeaderboardEntryCallback() {
            @Override
            public void OnEntry(int rank, String name, String url, int money,String id) {
                if (id.equals(playerID) || !arrIDBanned.contains(id,false))
                {
                    count_rank[0]++;
                    table.row();
                    table.add(new LeaderboardItemUI(count_rank[0], name, url, money, scrollWidth)).expandX().fillX();
                }
            }
        });
        Phom.zenSDK.FBInstant_LoadMyLeaderboard(new IZen.FBInstant_LeaderboardEntryCallback() {
            @Override
            public void OnEntry(int rank, String name, String url, int money,String id) {
                myRank=new LeaderboardItemUI(rank, name, url, money, scrollWidth);
                myRank.setPosition(-scroll.getWidth()/2,-315);
                myRank.getChildren().get(0).remove();
                addActor(myRank);
            }
        });
    }
    public FBLeaderboardUI(){
        GameUI.HideNonAction(this);
        CreateUI();
        AddIDBanned();
        playerID=Phom.zenSDK.FBInstant_GetPlayerID();
    }

    public void CreateUI(){
       // this.setPosition(Phom.V_WIDTH, 0);
        Image bgImg = new Image(Assets.GetTexture(Assets.fbleaderboard_box));
        bgImg.setPosition( 0, 0, Align.center);
        this.addActor(bgImg);

        Button closeBtn =  GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        closeBtn.setPosition(bgImg.getX(Align.topRight), bgImg.getY(Align.topRight)-closeBtn.getHeight()/2, Align.center);
        this.addActor(closeBtn);

        closeBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                onClickExit();
            }
        });

        table = new Table();
        scroll = new ScrollPane(table);
        scroll.setSize(1104, bgImg.getHeight()-220);
        scroll.setPosition(0, -10, Align.center);
        this.addActor(scroll);
    }
    public void onClickExit(){
        OverlayUI.Hide();
        GameUI.Hide(FBLeaderboardUI.this);
    }

    public class LeaderboardItemUI extends Group{
        public LeaderboardItemUI(int rank, String name, String url, long money, int scrollWidth){

            Image image =  new Image(Assets.GetTexture(Assets.fbleaderboard_bar)); //GameUI.NewImage(GameUI.GetTexture("Sprites/leaderboard_bar.png"), 0,0,0, Align.center);
            addActor(image);
            image.setPosition(0, 0, Align.bottomLeft);
            // image.setHeight(100);

            final int centerBar = (int)image.getHeight()/2;
            Label rankLabel = GameUI.NewLabel(rank+".",Assets.GetBitmapFont(Assets.purpleRankpurpleRank));//GameUI.NewLabel(rank +".", 1, 0, 0, Color.WHITE);
            this.addActor(rankLabel);
            rankLabel.setPosition(50, centerBar, Align.left);

            Label nameLabel = GameUI.NewLabel(""+name,Assets.GetBitmapFont(Assets.Saira_ExtraBold));//GameUI.NewLabel(""+name, 1, 0, 0, Color.WHITE);
            this.addActor(nameLabel);
            nameLabel.setPosition(280, centerBar, Align.left);


            Label tsLabel = GameUI.NewLabel("Kỷ lục:",Assets.GetBitmapFont(Assets.Saira_ExtraBold));// GameUI.NewLabel("Tốt nhất:", 1, 0, 0, Color.WHITE);
            this.addActor(tsLabel);
            tsLabel.setPosition(scrollWidth - 200, centerBar, Align.right);


            Label moneyLabel =GameUI.NewLabel(""+money,Assets.GetBitmapFont(Assets.Saira_ExtraBold));//  GameUI.NewLabel("0", 1, 0, 0, Color.YELLOW);
            moneyLabel.setColor(Color.YELLOW);
            this.addActor(moneyLabel);
            moneyLabel.setPosition(scrollWidth - 50, centerBar, Align.right);

            //String url2 = "https://platform-lookaside.fbsbx.com/platform/instantgames/profile_pic.jpg?igpid=1937343412995491&height=256&width=256&ext=1538814184&hash=AeTz0W1guc98_ikU";

            Phom.zenSDK.LoadUrlTexture(url, new IZen.UrlTextureCallback() {
                @Override
                public void success(Texture texture) {
                    Image img = new Image(texture);
                    img.setWidth(65);
                    img.setHeight(65);
                    img.setPosition(210, centerBar, Align.center);
                    addActor(img);

                }
                @Override
                public void error() {

                }
            });
            this.setHeight(image.getHeight()+2);
        }
    }

}
