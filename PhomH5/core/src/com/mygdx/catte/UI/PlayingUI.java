package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.ParticleActor.ParticleEffectActor;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayingUI extends Group{
    public Button btnAn;
    public Button btnDanh;
    public Button btnXepBai;
    public Button btnBoc;
    public Button btnHaBai;
    public Button btnGuiBai;
    public Label txtSoBai;

    public Button btnVanMoi;
    Button btnMenu;
    Image imgBoxTienCuoc;
    public Label txtMucCuoc;
    public Label txtTienCuoc;
    public Label txtContent;

    public ParticleEffectActor effectActor;
    Group groupTienCuoc;

    public PlayingUI(){
        CreateUI();
        AddUI();
        AddListener();
        CreateEffect();
        locateActors();
    }

    Array<ParticleEffect> arrPar;
    void CreateEffect() {
        final ParticleEffect pe = new ParticleEffect();
        pe.load(Gdx.files.internal("Particle/victory/victory.p"),Gdx.files.internal("Particle/victory"));
        effectActor=new ParticleEffectActor(pe);
        effectActor.setY(40);
        pe.scaleEffect(3f);
    }
    public void ShowEffect(){
        addActor(effectActor);
        effectActor.getEffect().reset(false);
    }
    public void HideEffect(){
        removeActor(effectActor);
    }
    void CreateUI(){
        txtSoBai=GameUI.NewLabel("52",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtSoBai.setColor(Color.BLACK);
        GameUI.ScaleLabel(txtSoBai,1.5f);
        txtSoBai.setPosition(5,0,Align.center);
        txtSoBai.setTouchable(Touchable.disabled);

        btnAn=GameUI.NewButton(Assets.GetTexture(Assets.btnAnBai));
        btnAn.setPosition(-300,-150,Align.center);

        btnDanh=GameUI.NewButton(Assets.GetTexture(Assets.btnDanh));
        btnDanh.setPosition(750,-300,Align.center);

        btnXepBai=GameUI.NewButton(Assets.GetTexture(Assets.btnXep));
        btnXepBai.setPosition(750,-450,Align.center);

        btnHaBai=GameUI.NewButton(Assets.GetTexture(Assets.btnHaBai));
        btnHaBai.setPosition(-300,-50,Align.center);

        btnGuiBai=GameUI.NewButton(Assets.GetTexture(Assets.btnGiveCards));
        btnGuiBai.setPosition(300,-50,Align.center);
        btnBoc=GameUI.NewButton(Assets.GetTexture(Assets.btnBoc));
        btnBoc.setPosition(300,-150,Align.center);

        btnVanMoi=GameUI.NewButton(Assets.GetTexture(Assets.btnVanMoi));
        GameUI.SetPositionActor(btnVanMoi,0,0);

        btnMenu=GameUI.NewButton(Assets.GetTexture(Assets.btnMenu));
        btnMenu.setPosition(845,440,Align.center);

        groupTienCuoc=new Group();
        groupTienCuoc.setPosition(0,0,Align.center);
        //imgBoxScoreBet
        imgBoxTienCuoc = new Image(Assets.GetTexture(Assets.boxTienCuoc));
        imgBoxTienCuoc.setPosition(0,0,Align.topLeft);

        //txtMucCuoc
        txtMucCuoc=GameUI.NewLabel("MỨC CƯỢC: ",Assets.GetBitmapFont(Assets.Saira_Regular));
        GameUI.ScaleLabel(txtMucCuoc,1.5f);
        txtMucCuoc.setAlignment(Align.left);
        txtMucCuoc.setPosition(imgBoxTienCuoc.getX(Align.left)+40,imgBoxTienCuoc.getY(Align.center),Align.left);

        //txtTienCuoc
        txtTienCuoc=GameUI.NewLabel("Tien cuoc: ",Assets.GetBitmapFont(Assets.Saira_Regular));
        GameUI.ScaleLabel(txtTienCuoc,1.5f);
        txtTienCuoc.setColor(Color.GREEN);
        txtTienCuoc.setAlignment(Align.right);
        txtTienCuoc.setWidth(130);
        txtTienCuoc.setPosition(txtMucCuoc.getX(Align.right)+50,txtMucCuoc.getY(Align.center),Align.left);

        groupTienCuoc.addActor(imgBoxTienCuoc);
        groupTienCuoc.addActor(txtMucCuoc);
        groupTienCuoc.addActor(txtTienCuoc);

        txtContent=GameUI.NewLabel("",Assets.GetBitmapFont(Assets.Saira_ExtraBold));

        GameUI.ScaleLabel(txtContent,2f);
        txtContent.setPosition(0,-220,Align.center);
        txtContent.setColor(Color.BLACK);
        Action scaleLoopText=(Actions.sequence(Actions.alpha(0,1f), Actions.alpha(1f,1f)));
        txtContent.addAction(Actions.forever(scaleLoopText));
        //test
        //Test();
    }
    void Test(){
        TextButton btnReset = GameUI.NewTextButton("RESET",1,Color.BLACK, Assets.GetTexture(Assets.btnGreen),Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.SetPositionActor(btnReset,550,480);
        btnReset.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                PreferenceController.Instance.ResetPlayerPreferences();
            }
        });
        addActor(btnReset);
    }
    void AddUI(){
        addActor(txtSoBai);
        addActor(btnXepBai);
        addActor(btnAn);
        addActor(btnDanh);
        addActor(btnHaBai);
        addActor(btnGuiBai);
        addActor(btnBoc);
        addActor(btnVanMoi);
        addActor(btnMenu);
        addActor(groupTienCuoc);
        addActor(txtContent);
    }
    void AddListener()
    {
        btnVanMoi.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                GameController.Instance.NewGame(false);
            }
        });

        btnBoc.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickBoc();
            }
        });
        btnAn.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickAn();
            }
        });
        btnDanh.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickDanh();
            }
        });
        btnGuiBai.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickGuiBai();
            }
        });
        btnHaBai.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickHa();
            }
        });
        btnXepBai.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                UIController.Instance.onClickXep();
            }
        });
        btnMenu.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickPause();
            }
        });
    }
    public void SetScoreBet(){
        int score_bet = GameController.GetScoreBet();
        GameUI.SetTextAndFit(txtTienCuoc,Utilities.ShortCutMoney(score_bet),1.5f);
    }
    public void onClickPause(){
        OverlayUI.Show();
        GameUI.Show(UIController.Instance.pauseUI);
    }
    public void SetGuideContent(String s){
        if (!GameSetting.Instance.isShowGuide)
            return;
        txtContent.setText(s);
    }
    public void SetTextSoBai(){
        int so_bai=GameController.Instance.gameModel.listCardOnTable.size;
        String s="";
        if (so_bai>0)
            s+=""+so_bai;
        txtSoBai.setText(s);
    }
    public void locateActors(){
        groupTienCuoc.setPosition(-Phom.V_WIDTH/2+16,Phom.V_HEIGHT/2-16);
        btnMenu.setPosition(Phom.V_WIDTH/2-20,Phom.V_HEIGHT/2-20,Align.topRight);
        btnXepBai.setPosition(Phom.V_WIDTH/2-120,-Phom.V_HEIGHT/2+70,Align.center);
        btnBoc.setPosition(170,-Phom.V_HEIGHT/2+270,Align.center);
        btnGuiBai.setPosition(btnBoc.getX(Align.center),btnBoc.getY(Align.center),Align.center);
        btnHaBai.setPosition(btnBoc.getX(Align.center),btnBoc.getY(Align.center),Align.center);
        btnAn.setPosition(-btnBoc.getX(Align.center),btnBoc.getY(Align.center),Align.center);
        btnDanh.setPosition(btnXepBai.getX(Align.center),btnXepBai.getY(Align.top)+10,Align.bottom);
        txtContent.setPosition(0,btnBoc.getY(Align.bottom)-15,Align.top);
    }
}
