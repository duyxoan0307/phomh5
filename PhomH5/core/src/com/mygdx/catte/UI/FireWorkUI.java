package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.Utilities.GameUI;

import java.util.Random;

/**
 * Created by DUY on 7/31/2018.
 */

public class FireWorkUI extends Group {
    Array<ParticleEffect> arrParticleEffect;
    public FireWorkUI(){
        arrParticleEffect=new Array<ParticleEffect>();
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        updateParticle(batch);
    }
    void updateParticle(Batch batch){
        batch.setProjectionMatrix(getStage().getCamera().combined);
        for (ParticleEffect pe:arrParticleEffect)
        {
            pe.update(Gdx.graphics.getDeltaTime());
            pe.draw(batch);
        }
    }
    public void ShowFireWork(){
        GameUI.ShowNonAction(this);
        arrParticleEffect.clear(); float timeDelay=0;
        final Array<Timer.Task> tasks=new Array<Timer.Task>();
        for (int i=0;i<15;i++)
        {
            float nextTime = (float) (0.3f + Math.random()*(1-0.3f));
            timeDelay+=nextTime;
            Timer.Task task = Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    if(!hasParent())
                    {
                        for (Timer.Task task:tasks)
                            task.cancel();
                        return;
                    }
                    CreateEffect();
                }
            },timeDelay);
            tasks.add(task);
        }
    }
    void CreateEffect(){
        if (arrParticleEffect.size>15)
            return;
        Random random = new Random();
        float ranY = -400f+random.nextFloat()*800f;
        float ranX = -800f + random.nextFloat()*(1600f);

        ParticleEffect pe = new ParticleEffect();
        pe.load(Gdx.files.internal("Particle/firework.p"),Gdx.files.internal("Particle"));
        //pe.getEmitters().first().setPosition(ranX,ranY);
        for (ParticleEmitter emitter:pe.getEmitters())
            emitter.setPosition(ranX,ranY);
        pe.scaleEffect(3f);
        pe.start();
        arrParticleEffect.add(pe);
    }
    public void HideFireWork(){
        GameUI.HideNonAction(this);
    }
}
