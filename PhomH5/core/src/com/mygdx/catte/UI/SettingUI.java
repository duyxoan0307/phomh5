package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/15/2018.
 */

public class SettingUI extends Group {
    public Button[] arrButtonTypeCards;
    public Button btnSound,btnShowGuide;
    public SettingUI(){
        CreateUI();
        GameUI.HideNonAction(this);
    }
    private void CreateUI() {
        //imgBox
        Image imgBox = new Image(Assets.GetTexture(Assets.imgBox));
        imgBox.setSize(Phom.V_WIDTH-200, Phom.V_HEIGHT-200);
        GameUI.SetPositionActor(imgBox, 0, 0);

        //btnExit
        Button btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        GameUI.SetPositionActor(btnExit, imgBox.getX() + imgBox.getWidth() - 20, imgBox.getY() + imgBox.getHeight() - 20);

        //txtTitle
        Label txtTitle = GameUI.NewLabel("CÀI ĐẶT", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle, 2);
        GameUI.SetPositionActor(txtTitle, 0, imgBox.getY(Align.top)-50);

        //txtAmThanh
        Label txtAmThanh = GameUI.NewLabel("Âm thanh", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtAmThanh.setAlignment(Align.left);
        GameUI.ScaleLabel(txtAmThanh, 1f);
        txtAmThanh.setPosition(-500,100,Align.left);

        //txtGuide
        Label txtGuide = GameUI.NewLabel("Hiện hướng dẫn khi đang chơi", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtGuide.setAlignment(Align.left);
        GameUI.ScaleLabel(txtGuide, 1f);
        txtGuide.setPosition(-500,0,Align.left);


        //txtLoaiBai
        Label txtLoaiBai = GameUI.NewLabel("Loại bài", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtLoaiBai.setAlignment(Align.left);
        GameUI.ScaleLabel(txtLoaiBai, 1f);
        txtLoaiBai.setPosition(-500,-130,Align.left);

        //btnSound
        btnSound = GameUI.NewButton(Assets.GetTexture(Assets.btnActive));
        GameUI.SetPositionActor(btnSound,150,txtAmThanh.getY(Align.center));

        //btnShowGuide
        btnShowGuide = GameUI.NewButton(Assets.GetTexture(Assets.btnActive));
        GameUI.SetPositionActor(btnShowGuide,150,txtGuide.getY(Align.center));

        //btnTypeCard
        arrButtonTypeCards = new Button[4];
        Vector2 first_position = new Vector2(-157,txtLoaiBai.getY(Align.center));
        for (int i=0;i<4;i++)
        {
            Button btnTypeCard = GameUI.NewButton(Assets.GetTexture(Assets.GetCardType(i)));
            GameUI.ResizeByScale(btnTypeCard,0.6f);
            GameUI.SetPositionActor(btnTypeCard,first_position.x+i*170,first_position.y);
            arrButtonTypeCards[i]=btnTypeCard;
        }

        //add actor
        addActor(imgBox);
        addActor(btnExit);
        addActor(txtTitle);
        addActor(txtAmThanh);
        addActor(txtGuide);
        addActor(txtLoaiBai);
        addActor(btnSound);
        addActor(btnShowGuide);
        for (Button button : arrButtonTypeCards)
            addActor(button);

        //addListener
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                onClickExitSetting();
            }
        });
        for (int i=0;i<4;i++)
        {
            Button button = arrButtonTypeCards[i];
            final int finalI = i;
            button.clearListeners();
            button.addListener(new IClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {

                    SoundController.Instance.PlayClipButton();

                    UIController.Instance.onClickChangeTypeCard(finalI);
                }
            });
        }
        btnSound.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                UIController.Instance.onClickChangeSound();
            }
        });
        btnShowGuide.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();

                UIController.Instance.onClickChangeShowGuide();
            }
        });
    }
    public void onClickExitSetting(){
        OverlayUI.Hide();
        GameUI.Hide(this);
    }
}
