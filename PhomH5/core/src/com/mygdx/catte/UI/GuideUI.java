package com.mygdx.catte.UI;



import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Phom;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/15/2018.
 */

public class GuideUI extends Group {
    Image imgBox;
    public GuideUI(){
        CreateUI();
        GameUI.HideNonAction(this);
    }

    private void CreateUI() {
        //imgBox
        imgBox = new Image(Assets.GetTexture(Assets.imgBox));
        imgBox.setSize(Phom.V_WIDTH-200,Phom.V_HEIGHT-200);
        GameUI.SetPositionActor(imgBox,0,0);

        //btnExit
        Button btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        GameUI.SetPositionActor(btnExit,imgBox.getX()+imgBox.getWidth()-20,imgBox.getY()+imgBox.getHeight()-20);

        //txtTitle
        Label txtTitle = GameUI.NewLabel("HƯỚNG DẪN CHƠI PHỎM - TÁ LẢ",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle,1.5f);
        GameUI.SetPositionActor(txtTitle,0,imgBox.getY(Align.top)-50);

        //add actor
        addActor(imgBox);
        addActor(btnExit);
        addActor(txtTitle);

        //create scroll pane
        CreateScrollPane();

        //addListener
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickExitGuide();

            }
        });
    }

    private void CreateScrollPane() {
        ScrollPane scrollpane;
        Table container;

        // table that holds the scroll pane
        container = new Table();
        container.setWidth(imgBox.getWidth()-50);
        container.setHeight(imgBox.getHeight()-150);

        GameUI.SetPositionActor(container,0,-50);


        //textGuide
        String contentGuide="BỘ BÀI - NGƯỜI CHƠI:\n" +
                "Chơi Phỏm hay còn gọi là đánh Tá lả là trò chơi sử dụng bộ bài tú lơ khơ 52 lá.\n" +
                "Các thuật ngữ trong game:\n" +
                "- Phỏm: Là bộ gồm 3 quân bài trở lên có cùng chất và số liên tiếp nhau hoặc bộ 3 quân bài khác trở lên chất nhưng cùng số.\n" +
                "- Bài rác: Những lá bài lẻ không thuộc Phỏm nào.\n" +
                "- Nọc: Các lá bài còn dư sau khi chia cho người chơi.\n" +
                "- Cháy: Kết thúc ván, người chơi không hạ được Phỏm nào thì gọi là Cháy.\n" +
                "- Ù: Khi tất cả 9 lá bài đều có phỏm và không còn bài rác.\n" +
                "- Ù tròn: Khi tất cả 10 lá bài đều có phỏm và không còn bài rác.\n" +
                "- Ù khan: Khi tất cả bài không có phỏm hoặc cạ nào.\n" +
                "- Ăn chốt: Ăn bài người chơi trước ở lượt đánh cuối.\n" +
                "- Đền: Nếu người chơi đánh cho người kế tiếp ăn 3 quân bài dẫn đến ù thì người chơi đó phải đền tất cả tiền cược cho người ù\n" +
                "- Tái: Sau khi hạ bài, nếu trong bàn có người ăn, các quân bài đã đánh ra sẽ được chuyển chỗ và bạn được phép đánh thêm một lượt.\n" +
                "- Gửi: Ở lượt cuối, nếu bài rác của bạn có thể kết nạp vào Phỏm của những người chơi đã hạ bài thì bạn có thể Gửi lá bài rác đó đi. Lá bài được gửi đi sẽ không bị tính điểm khi kết thúc ván.\n" +
                "\n" +
                "CÁCH CHƠI PHỎM:\n" +
                "- Bắt đầu một ván Phỏm Tá lả, người chơi đánh đầu tiên sẽ được chia 10 lá bài, những người còn lại được chia 9 lá. Tất cả các lá bài dư được đặt ở giữa bàn, gọi là Nọc.\n" +
                "- Người chơi đầu tiên sẽ đánh ra 1 lá bài. Người tiếp theo sẽ ăn lá bài đó nếu nó hợp với các lá bài đang có tạo thành Phỏm.\n" +
                "- Nếu người tiếp theo lá bài đó thì sẽ phải bốc 1 lá bài trong Nọc.\n" +
                "- Sau khi ăn bài/ bốc bài, người chơi phải đánh ra 1 lá bài. Cứ tiếp tục như vậy cho đến hết vòng.\n" +
                "- Theo luật chơi Phỏm, ván bài sẽ kết thúc khi trong bàn có 1 người chơi Ù. Nếu trong bàn không có ai Ù, sau 4 lượt ván bài sẽ kết thúc.\n" +
                "- Khi đánh lượt cuối, người chơi phải hạ tất cả Phỏm mình đang có, sau đó gửi các lá bài có thể gửi vào Phỏm của những người chơi đã hạ bài rồi mới đánh quân cuối cùng. Các quân bài còn lại sẽ được giữ lại để tính điểm khi kết thúc ván bài.\n" +
                "\n" +
                "TÍNH CƯỢC:\n" +
                "- Người thắng ăn hết tiền cược của 3 người còn lại\n" +
                "- Mỗi lá bài bị ăn chốt: trừ 2 x (số lần bị ăn) lần tiền cược\n" +
                "- Về nhì: trừ 1 lần tiền cược\n" +
                "- Về ba: trừ 2 lần tiền cược\n" +
                "- Về bét: trừ 3 lần tiền cược\n" +
                "- Cháy: trừ 4 lần tiền cược\n" +
                "- Ù hoặc ù khan: mỗi nhà thua sẽ mất 5 lần tiền cược\n" +
                "- Ù tròn: mỗi nhà thua sẽ mất 10 lần tiền cược\n" +
                "\n" +
                "CÁCH TÍNH ĐIỂM:\n" +
                "- Nếu trong bàn có người chơi Ù thì ván bài sẽ kết thúc.\n" +
                "- Sau 4 lượt chơi, nếu không có ai Ù thì các lá bài rác còn lại sẽ được cộng điểm để tính thắng - thua. Ai ít điểm nhất trong bàn sẽ là người thắng cuộc.\n" +
                "- Số điểm của mỗi lá bài là số trên lá bài đó, riêng các lá A, J, Q, K sẽ được tính điểm lần lượt là 1, 11, 12, 13.\n" +
                "- Trong trường hợp điểm số bằng nhau, người hạ bài sau sẽ bị xử thua.\n" +
                "- Người chơi bị Cháy (không hạ được Phỏm nào) se bị tính là về bét.";
        Label txtGuide = GameUI.NewLabel(contentGuide,Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtGuide.setAlignment(Align.left);
        txtGuide.setWrap(true);
        GameUI.ScaleLabel(txtGuide,1);

        //inner table that is used as a makeshift list.
        Table innerContainer = new Table();
        innerContainer.add(txtGuide).expand().fill();

        // create the scrollpane
        scrollpane = new ScrollPane(innerContainer);

        scrollpane.setScrollingDisabled(true,false);

        //add the scroll pane to the containe
        container.add(scrollpane).fill().expand();
        addActor(container);
    }
    public void onClickExitGuide(){
        OverlayUI.Hide();
        GameUI.Hide(this);
    }
}
