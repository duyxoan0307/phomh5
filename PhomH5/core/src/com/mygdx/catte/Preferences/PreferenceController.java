package com.mygdx.catte.Preferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.mygdx.catte.Model.DataModel;

import java.util.Random;

/**
 * Created by DUY on 6/14/2018.
 */

public class PreferenceController {
    public static PreferenceController Instance;
    public Preferences preferences;
    public PreferenceController(){
        Instance=this;
        preferences= Gdx.app.getPreferences("Phom");
        InitPreference();
        CheckComputerCoin();
        preferences.flush();
    }

    private void InitPreference() {
//        preferences.clear();
//        preferences.flush();
        if (!preferences.contains("Host"))
        {
            ResetPlayerPreferences();
        }
        if (!preferences.contains("MaxScoreBet"))
            preferences.putInteger("MaxScoreBet",0);
        if (!preferences.contains("HighScore"))
        {
            preferences.putString("HighScore",100000+"");
            preferences.flush();
        }
    }
    void CheckComputerCoin(){
        for (int i=1;i<13;i++)
        {
            if (Long.parseLong(preferences.getString("ScoreOfPlayer"+i))>=500000)
                continue;;
            int ran = (int) (10 + Math.random()*(30-10));
            preferences.putString("ScoreOfPlayer"+i,String.valueOf((long)(ran*1000000)));
            preferences.flush();
        }
    }
    public void ResetPlayerPreferences(){
        preferences.clear();
        preferences.flush();
        DataModel.Reset();
        preferences.putInteger("Host",0);
        preferences.putString("HighScore",100000+"");
        preferences.putInteger("ScoreBet",0);
        preferences.putInteger("NumberPlayer",4);
        preferences.putBoolean("IsSound",true);
        preferences.putBoolean("IsShowGuide",true);
        preferences.putInteger("TypeCard",0);
        preferences.putString("PlayerName","Player");
        preferences.putInteger("PlayerAvatar",0);
        for (int i=1;i<13;i++)
        {
            int ran = (int) (10 + Math.random()*(30-10));
            preferences.putString("ScoreOfPlayer"+i,String.valueOf((long)(ran*1000000)));
            preferences.flush();
        }
    }
}
