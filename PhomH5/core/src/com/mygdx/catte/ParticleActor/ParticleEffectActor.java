package com.mygdx.catte.ParticleActor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

/**
 * Created by DUY on 7/31/2018.
 */

public class ParticleEffectActor extends Actor {
    ParticleEffect effect;

    public ParticleEffectActor(ParticleEffect effect) {
        this.effect = effect;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        effect.draw(batch); //define behavior when stage calls Actor.draw()
    }

    public void act(float delta) {
        super.act(delta);
        effect.setPosition(getX(), getY()); //set to whatever x/y you prefer
        effect.update(delta); //update it
       //effect.start(); //need to start the particle spawning
    }
    public void ScaleEffect(float pScale){
        float scaling;
        for (ParticleEmitter e : effect.getEmitters()) {

            scaling = e.getXScale().getHighMax();
            e.getXScale().setHigh(scaling * pScale);
            e.getYScale().setHigh(scaling*pScale);

            scaling = e.getXScale().getLowMax();
            e.getXScale().setLow(scaling * pScale);
            e.getYScale().setLow(scaling * pScale);

            scaling = e.getVelocity().getHighMax();
            e.getVelocity().setHigh(scaling * pScale);

            scaling = e.getVelocity().getLowMax();
            e.getVelocity().setLow(scaling * pScale);

            scaling = e.getSpawnHeight().getHighMax();
            e.getSpawnHeight().setHighMax(scaling * pScale);

            scaling = e.getSpawnHeight().getLowMax();
            e.getSpawnHeight().setLowMax(scaling * pScale);

            scaling = e.getSpawnHeight().getHighMin();
            e.getSpawnHeight().setHighMin(scaling * pScale);

            scaling = e.getSpawnHeight().getLowMin();
            e.getSpawnHeight().setLowMin(scaling * pScale);

            scaling = e.getSpawnWidth().getHighMax();
            e.getSpawnWidth().setHighMax(scaling * pScale);

            scaling = e.getSpawnWidth().getLowMax();
            e.getSpawnWidth().setLowMax(scaling * pScale);

            scaling = e.getSpawnWidth().getHighMin();
            e.getSpawnWidth().setHighMin(scaling * pScale);

            scaling = e.getSpawnWidth().getLowMin();
            e.getSpawnWidth().setLowMin(scaling * pScale);

            scaling = e.getXOffsetValue().getLowMax();
            e.getXOffsetValue().setLowMax(scaling * pScale);

            scaling = e.getXOffsetValue().getLowMin();
            e.getXOffsetValue().setLowMin(scaling * pScale);

            scaling = e.getYOffsetValue().getLowMax();
            e.getYOffsetValue().setLowMax(scaling * pScale);

            scaling = e.getYOffsetValue().getLowMin();
            e.getYOffsetValue().setLowMin(scaling * pScale);
        }
    }
    public void UpdateTexture(Texture texture,int index){
        Array<Sprite> arr = new Array<Sprite>();
        arr.add(new Sprite(texture));
        if (effect.getEmitters().size<=index)
            return;
        effect.getEmitters().get(index).setSprites(arr);
    }
    public ParticleEffect getEffect() {
        return effect;
    }
}
