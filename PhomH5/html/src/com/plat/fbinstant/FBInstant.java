package com.plat.fbinstant;

import com.mygdx.catte.IZen;

public class FBInstant {
    public static native void LoadingProgress(int percent) /*-{
            $wnd.FBInstant.setLoadingProgress(percent);
    }-*/;

    public static native void LoadingFinished(IZen.FBInstant_StartCallback callback) /*-{
            $wnd.FBInstant.startGameAsync().then(function() {
                if (callback!=null)
                    callback.@com.mygdx.catte.IZen.FBInstant_StartCallback::OnEvent()();
                $wnd.startGame();
                $wnd.loadFullscreen();
              });
    }-*/;

    public static native String GetPlatform()/*-{
            return $wnd.FBInstant.getPlatform();
    }-*/;
}
