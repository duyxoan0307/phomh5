package com.plat.fbinstant;


import com.mygdx.catte.IZen;

public class FBInstantBot {
    public static native String canCreateShortcut(IZen.FBInstant_CanCreateCallback callback)/*-{
           $wnd.FBInstant.canCreateShortcutAsync()
             .then(function(canCreateShortcut) {
             $wnd.console.log("canCreateShortcut " + canCreateShortcut);
                callback.@com.mygdx.catte.IZen.FBInstant_CanCreateCallback::OnCallback(Z)(canCreateShortcut);
             });
   }-*/;

    public static native String subscribeBot()/*-{
           $wnd.FBInstant.player.canSubscribeBotAsync()
             .then(function(can_subscribe) {
              $wnd.console.log("canSubscribeBot " + can_subscribe);
              if(can_subscribe){
                   $wnd.FBInstant.player.subscribeBotAsync()
                   .then(function() {
                       $wnd.console.log("subscribebot done");
                   });
               }
             });
   }-*/;



    public static native String createShortcut()/*-{
       $wnd.console.log("createShortcut");
       $wnd.FBInstant.createShortcutAsync()
       .then(function() {
           $wnd.console.log("shortcut create");
       });
   }-*/;
}
