package com.plat.fbinstant;

import com.mygdx.catte.IZen;

public class FBInstantLeaderboard {
    public static native void ReportScore(String leaderboard_name, double score) /*-{
        //$wnd.console.log("ReportScore " + score);
        $wnd.FBInstant
          .getLeaderboardAsync(leaderboard_name)
          .then(function(leaderboard){
            return leaderboard.setScoreAsync(score);
          });


        //$wnd.reportScore(score);
        }-*/;

    public static native void LoadLeaderboard(String leaderboard_name, IZen.FBInstant_LeaderboardEntryCallback callback) /*-{
          $wnd.console.log("LoadLeaderboard start");
          $wnd.FBInstant
          .getLeaderboardAsync(leaderboard_name)
          .then(function(leaderboard){
                leaderboard.getEntriesAsync(100, 0)
                .then(function(entries){
                    $wnd.console.log("LoadLeaderboard entries ok");
                    for (var i = 0; i < entries.length; i++) {
                      //window.onLeaderboardEntries(entries[i].getRank(), entries[i].getPlayer().getName(), entries[i].getPlayer().getPhoto(), entries[i].getScore());
                       // $wnd.console.log("id: " +entries[i].getPlayer().getName()+": " + entries[i].getPlayer().getID());                                                $wnd.console.log("id: " + entries[i].getPlayer().getID());
                        if(callback!=null)
                            callback.@com.mygdx.catte.IZen.FBInstant_LeaderboardEntryCallback::OnEntry(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)(entries[i].getRank(), entries[i].getPlayer().getName(), entries[i].getPlayer().getPhoto(), entries[i].getScore(),entries[i].getPlayer().getID());
                    }
                 });
          });
    }-*/;
    public static native void LoadMyLeaderboard(String leaderboard_name, IZen.FBInstant_LeaderboardEntryCallback callback) /*-{
          $wnd.console.log("Load myLeaderboard start");
          $wnd.FBInstant
          .getLeaderboardAsync(leaderboard_name)
          .then(function(leaderboard){
                leaderboard.getPlayerEntryAsync()
                .then(function(entry){
                    $wnd.console.log("Load myLeaderboard entry ok");
                      //window.onLeaderboardEntries(entry.getRank(), entry.getPlayer().getName(), entry.getPlayer().getPhoto(), entry.getScore());
                        $wnd.console.log("test my leaderboard");

                        if(callback!=null)
                            callback.@com.mygdx.catte.IZen.FBInstant_LeaderboardEntryCallback::OnEntry(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)(entry.getRank(),entry.getPlayer().getName(), entry.getPlayer().getPhoto(), entry.getScore(),entry.getPlayer().getID());

                 });
          });
    }-*/;
}
