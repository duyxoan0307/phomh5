package com.phomh5.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.badlogic.gdx.backends.gwt.preloader.AssetDownloader;
import com.badlogic.gdx.backends.gwt.preloader.Preloader;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Phom;
import com.plat.fbinstant.FBInstant;
import com.plat.fbinstant.FBInstantAds;
import com.plat.fbinstant.FBInstantBot;
import com.plat.fbinstant.FBInstantLeaderboard;
import com.plat.fbinstant.FBInstantPlayer;

public class HtmlLauncher extends GwtApplication {
        int PADDING = 0;
        boolean isHandleResize = false;
        public static Phom game;
        long lastInterstitialTime;
        String globalleaderboardName = "TOPDIEM";
        public static boolean isInited = false;
        @Override
        public GwtApplicationConfiguration getConfig () {
                int height = com.google.gwt.user.client.Window.getClientHeight() - PADDING;
                int width = com.google.gwt.user.client.Window.getClientWidth()- PADDING;

                if(width < height){
                        int h = width;
                        width = height;
                        height = h;
                }

                GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(width, height);
                Window.enableScrolling(false);
                Window.setMargin("0");
                float r = (float)width/(float)height;  //4:3=1.33 16:9=1.76
                if(r < 1.5f){
                        isHandleResize = true;
                        Window.addResizeHandler(new ResizeListener());
                }

                cfg.disableAudio = true;
                cfg.preferFlash = false;
                //cfg.antialiasing = true;



                return cfg;
        }

        @Override
        public ApplicationListener createApplicationListener () {

            IZen zenObj = new IZen() {
                @Override
                public void ShowFullscreen() {

                    if (System.currentTimeMillis() - lastInterstitialTime > 220000) {

                        if (FBInstantAds.IsFullscreenReady()) {
                            FBInstantAds.ShowFullscreen();
                            lastInterstitialTime = System.currentTimeMillis();
                        }
                    }


                }

                @Override
                public void ShowBanner(boolean visible) {

                }

                @Override
                public void TrackLevelStart(int level) {

                }

                @Override
                public void TrackLevelFailed(int level) {

                }

                @Override
                public void TrackLevelCompleted(int level) {

                }

                @Override
                public void ShowLeaderBoard() {
                    //showLeaderboard();
                }

                @Override
                public void Rate() {

                }

                @Override
                public void Like() {

                }

                @Override
                public String GetDefaultLanguage() {
                    return null;
                }

                @Override
                public boolean isVideoRewardReady() {
                    return false;
                }

                @Override
                public void ShowVideoReward(OnVideoRewardClosed callback) {

                }
                @Override
                public void LinkOtherGame(String packageName) {

                }

                @Override
                public void LoadUrlTexture(String url, UrlTextureCallback callback) {
                    loadUrlTexture(url, callback);
                }

                @Override
                public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {
                    FBInstantPlayer.GetDoubleStatsAsync(statname, callback);
                }

                @Override
                public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {
                    FBInstantPlayer.SetDoubleStatsAsync(statname, value, callback);
                }

                @Override
                public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {
                    FBInstantPlayer.IncrementDoubleStatsAsync(statname, value, callback);
                }

                @Override
                public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {
                    FBInstantLeaderboard.LoadLeaderboard(globalleaderboardName, callback);
                }

                @Override
                public void FBInstant_LoadMyLeaderboard(FBInstant_LeaderboardEntryCallback callback) {
                    FBInstantLeaderboard.LoadMyLeaderboard(globalleaderboardName, callback);
                }

                @Override
                public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {
                    String name = FBInstantPlayer.GetName();
                    String photo = FBInstantPlayer.GetPhoto();
                    callback.OnInfo(name, photo);
                }

                @Override
                public String FBInstant_GetPlayerID() {
                    return FBInstantPlayer.GetID();
                }

                @Override
                public void OnShow() {
                    if(isInited==false){
                        SetupCanvas();
                        isInited = true;
                    }
                }

                @Override
                public void FBInstant_CanCreateShortCut(FBInstant_CanCreateCallback callback) {
                    FBInstantBot.canCreateShortcut(callback);
                }

                @Override
                public void FBInstant_CreateShortCut() {
                    FBInstantBot.createShortcut();
                }

                @Override
                public void FBInstant_CanSubscribeBot() {
                    FBInstantBot.subscribeBot();
                }


                @Override
                public void ReportScore(String leaderboardID, long score) {
                    FBInstantLeaderboard.ReportScore(globalleaderboardName, (double)(score));
                }
            };

            game =  new Phom(zenObj);
            return game;
        }
    int canvasWidth;
    int canvasHeight;
    public void SetupCanvas(){
        CanvasElement canvas = getCanvasElement();
        int w = canvas.getWidth();// getInnerWidth();
        int h = canvas.getHeight();//getInnerHeight();
        double dpr  = devicePixelRatio();
        int cw = (int)(w*dpr);
        int ch = (int)(h*dpr);

        if(isHandleResize){
            w = getInnerWidth();
            h = getInnerHeight();
            cw = getFBWidth();
            ch = getFBHeight();
        }

        canvas.getStyle().setWidth(w, Style.Unit.PX);
        canvas.getStyle().setHeight(h, Style.Unit.PX);
        canvas.setWidth(cw);
        canvas.setHeight(ch);

    }

    public void ResizeCanvas(){
        if(jsResize() == false) {
            CanvasElement canvas = getCanvasElement();
            //double dpr = devicePixelRatio();
            int fw = getInnerWidth();
            int fh = getInnerHeight();
            canvas.getStyle().setWidth(fw, Style.Unit.PX);
            canvas.getStyle().setHeight(fh, Style.Unit.PX);
            canvas.setWidth(getFBWidth());
            canvas.setHeight(getFBHeight());
            //consolelog3("resize l:" + getFBWidth() + " " + getFBHeight());
        }

    }

    class ResizeListener implements ResizeHandler {
        @Override
        public void onResize(ResizeEvent event) {
            ResizeCanvas();


        }
    }
    public void loadUrlTexture(final String assetFileUrl, final IZen.UrlTextureCallback callback) {
        final AssetDownloader loader = new AssetDownloader();
        loader.loadImage(assetFileUrl, "image/png", "anonymous",  new AssetDownloader.AssetLoaderListener<ImageElement>() {
            @Override
            public void onProgress(double amount) {
            }

            @Override
            public void onFailure() {
                callback.error();
            }

            @Override
            public void onSuccess(ImageElement result) {
                Pixmap pm = new Pixmap(result);
                Texture returnTexture = new Texture(pm);
                callback.success(returnTexture);
            }
        });

    }


    @Override
    public Preloader.PreloaderCallback getPreloaderCallback() {

        return new Preloader.PreloaderCallback() {
            public void error(String file) {
                System.out.println("error: " + file);
            }

            public void update(Preloader.PreloaderState state) {
                double percent = (double)(100.0F * state.getProgress());
                // meterStyle.setWidth(percent, Style.Unit.PCT);

                FBInstant.LoadingProgress((int)percent);

                if(state.hasEnded()) {
                    FBInstant.LoadingFinished(new IZen.FBInstant_StartCallback() {
                        @Override
                        public void OnEvent() {
                            game.startGame();
                        }
                    });
                }
            }
        };
    }

    private static native double devicePixelRatio() /*-{
            return  $wnd.window.devicePixelRatio;
        }-*/;

    private static native int getInnerWidth() /*-{
            return $wnd.window.innerWidth;
        }-*/;

    private static native int getInnerHeight() /*-{
            return $wnd.window.innerHeight;
        }-*/;

    private static native int getFBWidth() /*-{
            return $wnd.window.innerWidth * $wnd.window.devicePixelRatio;
        }-*/;

    private static native int getFBHeight() /*-{
            return $wnd.window.innerHeight * $wnd.window.devicePixelRatio;
        }-*/;

    private static native void consolelog(String log) /*-{
            $wnd.consolelog(log);
        }-*/;

    private static native boolean jsResize() /*-{
            return $wnd.jsResize();
        }-*/;
}