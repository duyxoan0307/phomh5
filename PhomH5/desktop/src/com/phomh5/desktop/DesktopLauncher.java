package com.phomh5.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.catte.IZen;
import com.mygdx.catte.Phom;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DesktopLauncher {
	static int testval = 100000;
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 660;
		config.height = 380;

		IZen zenObj = new IZen() {
			@Override
			public void ShowFullscreen() {

			}

			@Override
			public void ShowBanner(boolean visible) {

			}

			@Override
			public void TrackLevelStart(int level) {

			}

			@Override
			public void TrackLevelFailed(int level) {

			}

			@Override
			public void TrackLevelCompleted(int level) {

			}

			@Override
			public void ShowLeaderBoard() {

			}

			@Override
			public void ReportScore(String leaderboardID, long score) {

			}

			@Override
			public void Rate() {

			}

			@Override
			public void Like() {

			}

			@Override
			public String GetDefaultLanguage() {
				return null;
			}

			@Override
			public boolean isVideoRewardReady() {
				return false;
			}

			@Override
			public void ShowVideoReward(OnVideoRewardClosed callback) {

			}

			@Override
			public void LinkOtherGame(String packageName) {

			}

			@Override
			public void LoadUrlTexture(String url, final UrlTextureCallback callback) {
				new Thread(new Runnable() {

					@Override
					public void run () {
						try {
							String url2 = "http://img.hb.aicdn.com/a61481e41098c6f1ca1ec5850085476f86cb89283604d-x6leM5_/fw/480";
							byte[] bytes = SendGetRequestToByte(url2);
							if (bytes != null) {
								// load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
								final Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
								final int originalWidth = pixmap.getWidth();
								final int originalHeight = pixmap.getHeight();
								int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
								int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
								final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
								potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
								pixmap.dispose();
								Gdx.app.postRunnable(new Runnable() {
									@Override
									public void run() {
										Texture txt = new Texture(potPixmap);
										callback.success(txt);
									}
								});
							}
						}
						catch (Exception e){

						}
					}
				}).start();
			}

			@Override
			public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {
				if(callback!=null)
					callback.OnValue(testval, false);
			}

			@Override
			public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {
				if(callback!=null)
					callback.OnSuccess(true);
			}

			@Override
			public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {
				if(callback!=null)
					callback.OnSuccess(testval+value);
			}

			@Override
			public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_LoadMyLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {
				String name = "Player";
				String photo = "https://lh3.googleusercontent.com/mMuRBAYPuPjr_JfY-nY0yX_b9xHSIFx9IDE1RrPsEdfec8TgzHzk0CJxOFPqK94ArQo=s180-rw";

				callback.OnInfo(name, photo);
			}

			@Override
			public String FBInstant_GetPlayerID() {
				return null;
			}

			@Override
			public void OnShow() {

			}


		};
		new LwjglApplication(new Phom(zenObj), config);
	}


	public static byte[] SendGetRequestToByte(String urlStr) {
		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent", "Mozilla/5.0");
			conn.connect();
			// Get the response
			InputStream in = conn.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead=in.read(data,0,data.length))!=-1) buffer.write(data,0,nRead);
			buffer.flush();
			return buffer.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
